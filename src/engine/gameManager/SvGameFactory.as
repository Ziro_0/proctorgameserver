/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGameFactory

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-26-2018
  ActionScript:  3.0
  Description:  
  History:
    05-26-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.gameManager {
  import engine.I_SvEngine;
  import engine.common.data.GameIdToTypesMap;
  import engine.common.data.GameTypes;
  import engine.game.SvGame;
  import engine.game.fillInBlank.SvFillInBlankGame;
  import engine.game.letters.SvLettersGame;
  import engine.game.multipleChoice.SvMultipleChoiceGame;
  import engine.game.unscramble.SvUnscrambleGame;

  public class SvGameFactory {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // Create
    //==================================================================
    public static function Create(o_engine:I_SvEngine, s_gameId:String):SvGame {
      var s_gameType:String = GameIdToTypesMap.GetGameTypeFromId(s_gameId);
      var o_game:SvGame;
      
      switch (s_gameType) {
        case GameTypes.FILL_IN_BLANK:
          o_game = new SvFillInBlankGame(o_engine, s_gameId);
          break;
        
        case GameTypes.LETTERS:
          o_game = new SvLettersGame(o_engine, s_gameId);
          break;
        
        case GameTypes.MULTIPLE_CHOICE:
          o_game = new SvMultipleChoiceGame(o_engine, s_gameId);
          break;
        
        case GameTypes.UNSCRAMBLE:
          o_game = new SvUnscrambleGame(o_engine, s_gameId);
          break;
          
        default:
          o_game = null;
      }
      
      return(o_game);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}