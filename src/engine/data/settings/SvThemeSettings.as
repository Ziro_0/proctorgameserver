/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvThemeSettings

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          12-20-2018
  ActionScript:  3.0
  Description:  
  History:
    12-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data.settings {
  import engine.common.data.CustomThemePath;
  import engine.common.data.ThemeComponentNames;
  import flib.utils.FDataBlock;

  public class SvThemeSettings {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const PROP_IS_READ_ONLY:String = "isReadOnly";
    private static const PROP_COMPONENTS:String = "components";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_block:FDataBlock;
    private var ms_id:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvThemeSettings(o_data:Object = null, s_themeId:String = null) {
      mo_block = new FDataBlock(o_data || {});
      ms_id = s_themeId;
      initComponents();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // clone
    //==================================================================
    public function clone():SvThemeSettings {
      var o_targetBlock:FDataBlock = new FDataBlock();
      cloneIsReadOnly(o_targetBlock);
      cloneComponents(o_targetBlock);
      return(new SvThemeSettings(o_targetBlock.data, ms_id));
    }
    
    //==================================================================
    // components (get)
    //==================================================================
    public function get components():Object {
      return(mo_block.getObject(PROP_COMPONENTS));
    }
    
    //==================================================================
    // getData
    //==================================================================
    public function getData():Object {
      return(mo_block.data);
    }
    
    //==================================================================
    // getThemeIdOfComponent
    //==================================================================
    public function getThemeIdOfComponent(s_componentName:String):String {
      return(String(components[s_componentName]));
    }
    
    //==================================================================
    // id (get)
    //==================================================================
    public function get id():String {
      return(ms_id);
    }
    
    //==================================================================
    // isReadOnly (get)
    //==================================================================
    public function get isReadOnly():Boolean {
      return(mo_block.getBoolean(PROP_IS_READ_ONLY));
    }
    
    //==================================================================
    // isReadOnly (set)
    //==================================================================
    public function set isReadOnly(b_value:Boolean):void {
      mo_block.setProperty(PROP_IS_READ_ONLY, b_value);
    }
    
    //==================================================================
    // resetComponents
    //==================================================================
    public function resetComponents(s_themeId:String):void {
      ms_id = s_themeId;
      
      var o_components:Object = { };
      
      for each (var s_component:String in ThemeComponentNames.All()) {
        o_components[s_component] = s_themeId;
      }
      
      mo_block.setProperty(PROP_COMPONENTS, o_components);
    }
    
    //==================================================================
    // resetToReadOnly
    //==================================================================
    public function resetToReadOnly(s_themeId:String):void {
      isReadOnly = true;
      resetComponents(s_themeId);
    }
    
    //==================================================================
    // validateComponents
    //==================================================================
    public function validateComponents():Boolean {
      const THEME_COMPONENT_NAMES:Vector.<String> = ThemeComponentNames.All().concat();
      const DEFAULT_FIRST_READ_ONLY_THEME_ID:String = SvReadOnlyThemeIds.All()[0];
      
      var o_components:Object = components;
      var o_newComponents:Object = { };
      var b_wasUpdated:Boolean = false;
      var s_componentName:String;
      
      //iterate each component name in the themes component names array
      for each (s_componentName in THEME_COMPONENT_NAMES) {
        if (s_componentName in o_components) {
          //copy existing components from the old components block into the new components block
          
          var s_themeComponentId:String = o_components[s_componentName] as String;
          if (SvReadOnlyThemeIds.Exists(s_themeComponentId)) {
            //if the component theme id references a read-only theme, then copy it over
            o_newComponents[s_componentName] = s_themeComponentId;
          } else {
            //the only non read-only component theme id reference allowed is if the component uses
            //  a custom image selected by the server presenter
            if (CustomThemePath.IsCustom(s_themeComponentId)) {
              o_newComponents[s_componentName] = s_themeComponentId;
            } else {
              //the component theme id is only nor read-only nor custom, so restore it to the first
              // default read-only theme id
              o_newComponents[s_componentName] = DEFAULT_FIRST_READ_ONLY_THEME_ID;
              b_wasUpdated = true;
            }
          }
        } else {
          //Components names that exist in the themes component names array but not in the old components
          // block will be added. Default to the first read-only theme ID, if this theme is not
          // read-only.
          o_newComponents[s_componentName] =
            SvReadOnlyThemeIds.Exists(id) ? id : DEFAULT_FIRST_READ_ONLY_THEME_ID;
          b_wasUpdated = true;
        }
      }
      
      //Any components in the old components block whose component names don't exist in the themes
      // component names array will not be copied over (removal). This still qualifies as an update.
      if (!b_wasUpdated) {
        for (s_componentName in o_components) {
          if (THEME_COMPONENT_NAMES.indexOf(s_componentName) == -1) {
            b_wasUpdated = true;
            break;
          }
        }
      }
      
      if (b_wasUpdated) {
        mo_block.setProperty(PROP_COMPONENTS, o_newComponents);
      }
      
      return(b_wasUpdated);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // cloneComponents
    //==================================================================
    private function cloneComponents(o_targetBlock:FDataBlock):void {
      var o_targetComponents:Object = { };
      
      var o_sourceComponents:Object = components;
      for (var s_component:String in o_sourceComponents) {
        o_targetComponents[s_component] = o_sourceComponents[s_component];
      }
      
      o_targetBlock.setProperty(PROP_COMPONENTS, o_targetComponents);
    }
    
    //==================================================================
    // cloneIsReadOnly
    //==================================================================
    private function cloneIsReadOnly(o_targetBlock:FDataBlock):void {
      o_targetBlock.setProperty(PROP_IS_READ_ONLY, isReadOnly);
    }
    
    //==================================================================
    // initComponents
    //==================================================================
    private function initComponents():void {
      if (!components) {
        resetComponents("");
      }
    }
  }
}