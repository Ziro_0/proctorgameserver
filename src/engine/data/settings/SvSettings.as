/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvSettings

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-15-2018
  ActionScript:  3.0
  Description:  
  History:
    05-15-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data.settings {
  import flash.filesystem.File;
  import flash.filesystem.FileMode;
  import flash.filesystem.FileStream;
  
  public class SvSettings {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const FILE_URL:String = "proctorGameServer.json";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_data:SvSettingsData;
    private var mo_error:Error;
    private var mb_fileDidntExist:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvSettings() {
      super();
      mo_data = new SvSettingsData();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // CreateFile
    //==================================================================
    public static function CreateFile():File {
      return(File.applicationStorageDirectory.resolvePath(FILE_URL));
    }
    
    //==================================================================
    // data (get)
    //==================================================================
    public function get data():SvSettingsData {
      return(mo_data);
    }
    
    //==================================================================
    // error (get)
    //==================================================================
    public function get error():Error {
      return(mo_error);
    }
    
    //==================================================================
    // fileDidntExist (get)
    //==================================================================
    public function get fileDidntExist():Boolean {
      return(mb_fileDidntExist);
    }
    
    //==================================================================
    // importGameSettings
    //==================================================================
    public function importGameSettings(o_sourceFile:File):Boolean {
      if (!o_sourceFile) {
        return(false);
      }
      
      var o_sourceSettings:SvSettings = new SvSettings();
      const UPDATE_ON_VALIDATE:Boolean = false;
      if (!o_sourceSettings.read(o_sourceFile, UPDATE_ON_VALIDATE)) {
        return(false);
      }
      
      var o_oldGamesSettings:Object = mo_data.games;
      mo_data.games = o_sourceSettings.data.games;
      if (!write()) {
        mo_data.games = o_oldGamesSettings;
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // read
    //==================================================================
    public function read(o_sourceImportFile:File = null, b_updateOnValidate:Boolean = true):Boolean {
      var o_stream:FileStream = o_sourceImportFile ?
        openStreamForImport(o_sourceImportFile) :
        openStreamForReading();
      
      if (!o_stream) {
        mo_error ||= new Error("SvSettings.read. Error: Unable to open stream.");
        return(false);
      }
      
      var s_jsonData:String = readJsonData(o_stream);
      o_stream.close();
      
      if (!s_jsonData) {
        mo_error = new Error("JSON data is empty.");
        return(false);
      }
      
      return(decodeJsonData(s_jsonData, b_updateOnValidate));
    }
    
    //==================================================================
    // validateData
    //==================================================================
    public function validateData():Boolean {
      var b_wasUpdated:Boolean = validateDataGames();
      b_wasUpdated = validateDataThemes() || b_wasUpdated;
      b_wasUpdated = validateDataActiveTheme() || b_wasUpdated;
      return(b_wasUpdated);
    }
    
    //==================================================================
    // write
    //==================================================================
    public function write():Boolean {
      var o_stream:FileStream = openStreamForWriting();
      if (!o_stream) {
        return(false);
      }
      
      var s_jsonData:String = encodeJsonData();
      var b_ok:Boolean = writeJsonData(o_stream, s_jsonData);
      o_stream.close();
      return(b_ok);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // decodeJsonData
    //==================================================================
    private function decodeJsonData(s_jsonData:String, b_updateOnValidate:Boolean):Boolean {
      var o_data:Object;
      if (s_jsonData) {
        try {
          o_data = JSON.parse(s_jsonData);
        } catch (o_error:Error) {
          mo_error = o_error;
          return(false);
        }
      }
      
      mo_data.importFrom(o_data);
      
      if (b_updateOnValidate) {
        if (validateData()) {
          return(write());
        }
      }
      
      return(true);
    }
    
    //==================================================================
    // encodeJsonData
    //==================================================================
    private function encodeJsonData():String {
      var s_jsonData:String;
      
      try {
        const SPACE:int = 2;
        s_jsonData = JSON.stringify(mo_data, null, SPACE);
      } catch (o_error:Error) {
        mo_error = o_error;
        return(null);
      }
      
      return(s_jsonData);
    }
    
    //==================================================================
    // openStream
    //==================================================================
    private function openStream(s_fileMode:String, b_createFileIfNonExist:Boolean,
    o_sourceFile:File = null):FileStream {
      mo_error = null;
      
      var o_file:File = o_sourceFile || CreateFile();
      var o_stream:FileStream;
      mb_fileDidntExist = !o_file.exists;
      if (mb_fileDidntExist) {
        if (b_createFileIfNonExist) {
          o_stream = openStreamForWriting();
          if (!o_stream) {
            mo_error ||= new Error("SvSettings.openStream. Error: Unable to open stream.");
            return(null);
          }
          o_stream.close();
        }
      }
      
      o_stream = new FileStream();
      try {
        o_stream.open(o_file, s_fileMode);
      } catch (o_error:Error) {
        mo_error = o_error;
        o_stream = null;
      }
      
      return(o_stream);
    }
    
    //==================================================================
    // openStreamForImport
    //==================================================================
    private function openStreamForImport(o_sourceImportFile:File):FileStream {
      const CREATE_FILE_IF_NON_EXIST:Boolean = false;
      return(openStream(FileMode.READ, CREATE_FILE_IF_NON_EXIST, o_sourceImportFile));
    }
    
    //==================================================================
    // openStreamForReading
    //==================================================================
    private function openStreamForReading():FileStream {
      const CREATE_FILE_IF_NON_EXIST:Boolean = true;
      return(openStream(FileMode.READ, CREATE_FILE_IF_NON_EXIST));
    }
    
    //==================================================================
    // openStreamForWriting
    //==================================================================
    private function openStreamForWriting():FileStream {
      const CREATE_FILE_IF_NON_EXIST:Boolean = false;
      return(openStream(FileMode.WRITE, CREATE_FILE_IF_NON_EXIST));
    }
    
    //==================================================================
    // readJsonData
    //==================================================================
    private function readJsonData(o_stream:FileStream):String {
      var s_jsonData:String;
      try {
        s_jsonData = o_stream.readUTFBytes(o_stream.bytesAvailable);
      } catch (o_error:Error) {
        mo_error = o_error;
        return(null);
      }
      
      return(s_jsonData);
    }
    
    //==================================================================
    // validateDataActiveTheme
    //==================================================================
    private function validateDataActiveTheme():Boolean {
      var b_wasUpdated:Boolean;
      if (!mo_data.activeThemeId) {
        mo_data.activeThemeId = SvReadOnlyThemeIds.All()[0];
        b_wasUpdated = true;
      }
      return(b_wasUpdated);
    }
    
    //==================================================================
    // validateDataGames
    //==================================================================
    private function validateDataGames():Boolean {
      var b_wasUpdated:Boolean;
      if (mo_data.games == null) {
        mo_data.games = { };
        b_wasUpdated = true;
      }
      return(b_wasUpdated);
    }
    
    //==================================================================
    // validateDataReadOnlyThemes
    //==================================================================
    private function validateDataReadOnlyThemes():Boolean {
      var o_themesSettings:SvThemesSettings = new SvThemesSettings(mo_data.themes);
      var b_wasUpdated:Boolean = o_themesSettings.validateReadOnlyThemes();
      if (b_wasUpdated) {
        mo_data.themes = o_themesSettings.getData();
      }
      return(b_wasUpdated);
    }
    
    //==================================================================
    // validateDataThemes
    //==================================================================
    private function validateDataThemes():Boolean {
      var b_wasUpdated:Boolean;
      if (mo_data.themes == null) {
        mo_data.themes = SvThemesSettings.CreateReadOnlyThemesData();
        b_wasUpdated = true;
      } else {
        if (validateDataReadOnlyThemes()) {
          b_wasUpdated = true;
        }
        
        if (validateThemesComponents()) {
          b_wasUpdated = true;
        }
      }
      
      return(b_wasUpdated);
    }
    
    //==================================================================
    // validateThemesComponents
    //==================================================================
    private function validateThemesComponents():Boolean {
      var o_themesSettings:SvThemesSettings = new SvThemesSettings(mo_data.themes);
      return(o_themesSettings.validateThemesComponents());
    }
    
    //==================================================================
    // writeJsonData
    //==================================================================
    private function writeJsonData(o_stream:FileStream, s_jsonData:String):Boolean {
      try {
        o_stream.writeUTFBytes(s_jsonData);
      } catch (o_error:Error) {
        mo_error = o_error;
        return(false);
      }
      
      return(true);
    }
  }
}