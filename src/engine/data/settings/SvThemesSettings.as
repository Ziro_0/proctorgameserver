/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvThemesSettings

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          12-20-2018
  ActionScript:  3.0
  Description:  
  History:
    12-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data.settings {
  import flib.utils.FDataBlock;

  public final class SvThemesSettings {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //key: String (theme id)
    //data: SvThemeSettings
    private var mo_block:FDataBlock;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvThemesSettings(o_data:Object = null) {
      super();
      initThemeSettings(o_data);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // add
    //==================================================================
    public function add(s_themeId:String, o_themeSettings:SvThemeSettings):void {
      if (s_themeId && o_themeSettings) {
        mo_block.setProperty(s_themeId, o_themeSettings);
      }
    }
    
    //==================================================================
    // CloneFromData
    //==================================================================
    public static function CloneFromData(o_sourceThemesSettingsData:Object):SvThemesSettings {
      var o_sourceThemesSettings:SvThemesSettings = new SvThemesSettings(o_sourceThemesSettingsData);
      
      var o_targetThemesSettings:SvThemesSettings = new SvThemesSettings();
      for (var s_themeId:String in o_sourceThemesSettingsData) {
        var o_sourceThemeSettings:SvThemeSettings = o_sourceThemesSettings.getThemeSettings(s_themeId);
        o_targetThemesSettings.add(s_themeId, o_sourceThemeSettings.clone());
      }
      
      return(o_targetThemesSettings);
    }
    
    //==================================================================
    // CreateReadOnlyThemesData
    //==================================================================
    public static function CreateReadOnlyThemesData():Object {
      var o_data:Object = {};
      for each (var s_readOnlyThemeId:String in SvReadOnlyThemeIds.All()) {
        o_data[s_readOnlyThemeId] = createReadOnlyThemeData(s_readOnlyThemeId);
      }
      
      return(o_data);
    }
    
    //==================================================================
    // exists
    //==================================================================
    public function exists(s_themeId:String):Boolean {
      return(mo_block.containsProperty(s_themeId));
    }
    
    //==================================================================
    // getData
    //==================================================================
    public function getData():Object {
      var o_data:Object = {};
      var as_themeIds:Array = getThemeIds();
      
      for each (var s_themeIds:String in as_themeIds) {
        var o_themeSettings:SvThemeSettings = getThemeSettings(s_themeIds);
        o_data[s_themeIds] = o_themeSettings.getData();
      }
      
      return(o_data);
    }
    
    //==================================================================
    // getThemeIds
    //==================================================================
    public function getThemeIds():Array {
      return(mo_block.getKeys());
    }
    
    //==================================================================
    // getThemeSettings
    //==================================================================
    public function getThemeSettings(s_themeId:String):SvThemeSettings {
      return(mo_block.getObject(s_themeId) as SvThemeSettings);
    }
    
    //==================================================================
    // remove
    //==================================================================
    public function remove(s_themeId:String):void {
      mo_block.deleteProperty(s_themeId);
    }
    
    //==================================================================
    // validateReadOnlyThemes
    //==================================================================
    public function validateReadOnlyThemes():Boolean {
      var b_wasUpdated:Boolean = validateUnofficialReadOnlyThemeIds();
      b_wasUpdated = addNonExistingReadOnlyThemes() || b_wasUpdated;
      return(b_wasUpdated);
    }
    
    //==================================================================
    // validateThemesComponents
    //==================================================================
    public function validateThemesComponents():Boolean {
      var as_themeIds:Array = getThemeIds();
      var b_wasUpdated:Boolean;
      
      for each (var s_themeId:String in as_themeIds) {
        var o_theme:SvThemeSettings = getThemeSettings(s_themeId);
        b_wasUpdated = o_theme.validateComponents() || b_wasUpdated;
      }
      
      return(b_wasUpdated);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addNonExistingReadOnlyThemes
    //==================================================================
    private function addNonExistingReadOnlyThemes():Boolean {
      var b_wasUpdated:Boolean = false;
      
      for each (var s_readOnlyThemeId:String in SvReadOnlyThemeIds.All()) {
        if (!doesThemeExist(s_readOnlyThemeId)) {
          trace("SvThemesSettings.addNonExistingReadOnlyThemes: " + s_readOnlyThemeId);
          var o_themeSettings:SvThemeSettings =
            new SvThemeSettings(createReadOnlyThemeData(s_readOnlyThemeId), s_readOnlyThemeId);
          mo_block.setProperty(s_readOnlyThemeId, o_themeSettings);
          b_wasUpdated = true;
        }
      }
      
      return(b_wasUpdated);
    }
    
    //==================================================================
    // createReadOnlyThemeData
    //==================================================================
    private static function createReadOnlyThemeData(s_readOnlyThemeId:String):Object {
      var o_themeSettings:SvThemeSettings = new SvThemeSettings();
      o_themeSettings.resetToReadOnly(s_readOnlyThemeId);
      return(o_themeSettings.getData());
    }
    
    //==================================================================
    // doesThemeExist
    //==================================================================
    private function doesThemeExist(s_readOnlyThemeId:String):Boolean {
      return(mo_block.containsProperty(s_readOnlyThemeId));
    }
    
    //==================================================================
    // initThemeSettings
    //==================================================================
    private function initThemeSettings(o_data:Object):void {
      mo_block = new FDataBlock();
      
      for (var s_themeId:String in o_data) {
        var o_themeSettingsData:Object = o_data[s_themeId];
        add(s_themeId, new SvThemeSettings(o_themeSettingsData, s_themeId));
      }
    }
    
    //==================================================================
    // isReadOnlyTheme
    //==================================================================
    private function isReadOnlyTheme(s_themeId:String):Boolean {
      var o_themeSettings:SvThemeSettings = getThemeSettings(s_themeId);
      return(o_themeSettings && o_themeSettings.isReadOnly);
    }
    
    //==================================================================
    // isReadOnlyThemeId
    //==================================================================
    private function isReadOnlyThemeId(s_themeId:String):Boolean {
      return(SvReadOnlyThemeIds.Exists(s_themeId));
    }
    
    //==================================================================
    // makeThemeReadOnly
    //==================================================================
    private function makeThemeReadOnly(s_themeId:String):void {
      trace("SvThemesSettings.makeThemeReadOnly: " + s_themeId);
      var o_themeSettings:SvThemeSettings = getThemeSettings(s_themeId);
      o_themeSettings.resetToReadOnly(s_themeId);
    }
    
    //==================================================================
    // makeThemeNonReadOnly
    //==================================================================
    private function makeThemeNonReadOnly(s_themeId:String):void {
      trace("SvThemesSettings.makeThemeNonReadOnly: " + s_themeId);
      var o_themeSettings:SvThemeSettings = getThemeSettings(s_themeId);
      o_themeSettings.isReadOnly = false;
    }
    
    //==================================================================
    // validateUnofficialReadOnlyThemeIds
    //==================================================================
    private function validateUnofficialReadOnlyThemeIds():Boolean {
      //an "unofficial" read-only theme is one whose:
      //1) theme id is defined in the list of read-only ids, but its read-only settings is false; or
      //2) read-only setting is true, but its theme id is not defined in the list of read-only ids
      
      var as_unofficialThemeIds:Array = [];
      var as_existingThemeIds:Array = getThemeIds();
      for each (var s_themeId:String in as_existingThemeIds) {
        var b_isReadOnlyThemeId:Boolean = isReadOnlyThemeId(s_themeId);
        var b_isReadOnlyTheme:Boolean = isReadOnlyTheme(s_themeId);
        
        var b_condition1:Boolean = b_isReadOnlyThemeId && !b_isReadOnlyTheme;
        if (b_condition1) {
          as_unofficialThemeIds.push(s_themeId);
          makeThemeReadOnly(s_themeId);
          continue;
        }
        
        var b_condition2:Boolean = b_isReadOnlyTheme && !b_isReadOnlyThemeId;
        if (b_condition2) {
          as_unofficialThemeIds.push(s_themeId);
          makeThemeNonReadOnly(s_themeId);
        }
      }
      
      return(as_unofficialThemeIds.length > 0);
    }
  }
}