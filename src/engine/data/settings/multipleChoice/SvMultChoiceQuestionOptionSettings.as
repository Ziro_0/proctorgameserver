/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultChoiceQuestionOptionSettings

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-20-2018
  ActionScript:  3.0
  Description:  
  History:
    06-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data.settings.multipleChoice {
  import flib.utils.FDataBlock;

  public class SvMultChoiceQuestionOptionSettings {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const PROP_TEXT:String = "text";
    public static const PROP_IS_CORRECT:String = "isCorrect";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_block:FDataBlock;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMultChoiceQuestionOptionSettings(o_data:Object) {
      init(o_data);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // data (get)
    //==================================================================
    public function get data():Object {
      return(mo_block.data);
    }
    
    //==================================================================
    // data (set)
    //==================================================================
    public function set data(o_value:Object):void {
      mo_block.data = o_value;
    }
    
    //==================================================================
    // isCorrect (get)
    //==================================================================
    public function get isCorrect():Boolean {
      return(mo_block.getBoolean(PROP_IS_CORRECT));
    }
    
    //==================================================================
    // isCorrect (set)
    //==================================================================
    public function set isCorrect(b_value:Boolean):void {
      mo_block.setProperty(PROP_IS_CORRECT, b_value);
    }
    
    //==================================================================
    // text (get)
    //==================================================================
    public function get text():String {
      return(mo_block.getString(PROP_TEXT, ""));
    }
    
    //==================================================================
    // text (set)
    //==================================================================
    public function set text(s_value:String):void {
      mo_block.setProperty(PROP_TEXT, s_value);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_data:Object):void {
      mo_block = new FDataBlock(o_data);
    }
  }
}