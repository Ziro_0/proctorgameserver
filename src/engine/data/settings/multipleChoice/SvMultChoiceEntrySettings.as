/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultChoiceEntrySettings

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-20-2018
  ActionScript:  3.0
  Description:  
  History:
    06-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data.settings.multipleChoice {
  import engine.data.settings.SvMiniGameBaseEntrySettings;
  import flib.utils.FDataBlock;

  public class SvMultChoiceEntrySettings extends SvMiniGameBaseEntrySettings {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const PROP_QUESTION:String = "question";
    public static const PROP_CHOICES:String = "choices";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMultChoiceEntrySettings(o_data:Object = null) {
      super(o_data);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // choices (get)
    //==================================================================
    public function get choices():Array {
      return(block.getArray(PROP_CHOICES));
    }
    
    //==================================================================
    // choices (set)
    //==================================================================
    public function set choices(ao_values:Array):void {
      block.setProperty(PROP_CHOICES, ao_values);
    }
    
    //==================================================================
    // clone
    //==================================================================
    override public function clone():SvMiniGameBaseEntrySettings {
      var o_clone:SvMultChoiceEntrySettings = new SvMultChoiceEntrySettings();
      o_clone.choices = choices ? cloneChoices(choices) : [ ];
      o_clone.question = question;
      return(o_clone);
    }
    
    //==================================================================
    // question (get)
    //==================================================================
    public function get question():String {
      return(block.getString(PROP_QUESTION, ""));
    }
    
    //==================================================================
    // question (set)
    //==================================================================
    public function set question(s_value:String):void {
      block.setProperty(PROP_QUESTION, s_value);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // cloneChoice
    //==================================================================
    private function cloneChoice(o_sourceChoice:Object):Object {
      var o_targetChoice:Object = { };
      for (var s_key:Object in o_sourceChoice) {
        o_targetChoice[s_key] = o_sourceChoice[s_key];
      }
      return(o_targetChoice);
    }
    
    //==================================================================
    // cloneChoices
    //==================================================================
    private function cloneChoices(ao_sourceChoices:Array):Array {
      var ao_targetChoices:Array = [];
      
      for each (var o_sourceChoice:Object in ao_sourceChoices) {
        var o_targetChoice:Object = cloneChoice(o_sourceChoice);
        ao_targetChoices.push(o_targetChoice);
      }
      
      return(ao_targetChoices);
    }
  }
}