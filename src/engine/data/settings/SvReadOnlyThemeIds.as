/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvReadOnlyThemeIds

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          12-18-2018
  ActionScript:  3.0
  Description:  
  History:
    12-18-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data.settings {

  public final class SvReadOnlyThemeIds {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const BLUE_SHOWERS_BABY_DARK:String = "blue_showers_baby_dark";
    public static const BLUE_SHOWERS_BABY_LIGHT:String = "blue_showers_baby_light";
    public static const BLUE_SHOWERS_BABY_MEDIUM:String = "blue_showers_baby_medium";
    public static const BRIDAL_FLORAL_DARL:String = "bridal_floral_dark";
    public static const BRIDAL_FLORAL_LIGHT:String = "bridal_floral_light";
    public static const BRIDAL_FLORAL_MEDIUM:String = "bridal_floral_medium";
    
    private static const _ALL:Vector.<String> = new <String>[
      BLUE_SHOWERS_BABY_DARK,
      BLUE_SHOWERS_BABY_LIGHT,
      BLUE_SHOWERS_BABY_MEDIUM,
      BRIDAL_FLORAL_DARL,
      BRIDAL_FLORAL_LIGHT,
      BRIDAL_FLORAL_MEDIUM
    ];
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // All
    //==================================================================
    public static function All():Vector.<String> {
      return(_ALL.concat());
    }
    
    //==================================================================
    // Exists
    //==================================================================
    public static function Exists(s_themeId:String):Boolean {
      return(_ALL.indexOf(s_themeId) > -1);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}