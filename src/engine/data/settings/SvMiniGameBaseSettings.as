/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMiniGameBaseSettings

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-18-2018
  ActionScript:  3.0
  Description:  
  History:
    05-18-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.data.settings {
  import engine.common.data.GameModels;
  import flib.utils.FDataBlock;
  
  public class SvMiniGameBaseSettings {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const PROP_TITLE:String = "title";
    private static const PROP_ENTRIES:String = "entries";
    private static const PROP_TIME:String = "time";
    private static const PROP_MODEL:String = "model";
    
    private static const DEFAULT_TIME_SECS:uint = 60;
    private static const DEFAULT_MODEL:String = GameModels.BRIDAL;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_block:FDataBlock;
    private var mb_newDataInitialized:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMiniGameBaseSettings(o_data:Object) {
      init(o_data);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // block (get)
    //==================================================================
    public function get block():FDataBlock {
      return(mo_block);
    }
    
    //==================================================================
    // newDataInitialized (get)
    //==================================================================
    public function get newDataInitialized():Boolean {
      return(mb_newDataInitialized);
    }
    
    //==================================================================
    // entries (get)
    //==================================================================
    /**
     * Returns an array of all the defined entries.
     * @return Array of objects.
     */
    public function get entries():Array {
      return(mo_block.getArray(PROP_ENTRIES));
    }
    
    //==================================================================
    // model (get)
    //==================================================================
    public function get model():String {
      return(mo_block.getString(PROP_MODEL));
    }
    
    //==================================================================
    // model (set)
    //==================================================================
    public function set model(s_value:String):void {
      mo_block.setProperty(PROP_MODEL, s_value);
    }
    
    //==================================================================
    // time (get)
    //==================================================================
    public function get time():uint {
      return(mo_block.getNumber(PROP_TIME));
    }
    
    //==================================================================
    // time (set)
    //==================================================================
    public function set time(u_value:uint):void {
      mo_block.setProperty(PROP_TIME, u_value);
    }
    
    //==================================================================
    // title (get)
    //==================================================================
    public function get title():String {
      return(mo_block.getString(PROP_TITLE));
    }
    
    //==================================================================
    // title (set)
    //==================================================================
    public function set title(s_value:String):void {
      mo_block.setProperty(PROP_TITLE, s_value);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_data:Object):void {
      mb_newDataInitialized = false;
      mo_block = new FDataBlock(o_data);
      initEntries();
      initTime();
      initTitle();
      initModel();
    }
    
    //==================================================================
    // initEntries
    //==================================================================
    private function initEntries():void {
      if (mo_block.getArray(PROP_ENTRIES) == null) {
        mb_newDataInitialized = true;
        mo_block.setProperty(PROP_ENTRIES, []);
      }
    }
    
    //==================================================================
    // initModel
    //==================================================================
    private function initModel():void {
      if (!mo_block.containsProperty(PROP_MODEL)) {
        mb_newDataInitialized = true;
        model = DEFAULT_MODEL;
      }
    }
    
    //==================================================================
    // initTime
    //==================================================================
    private function initTime():void {
      var n_time:Number = time;
      if (isNaN(n_time) || n_time <= 0) {
        mb_newDataInitialized = true;
        time = DEFAULT_TIME_SECS;
      }
    }
    
    //==================================================================
    // initTitle
    //==================================================================
    private function initTitle():void {
      if (!mo_block.containsProperty(PROP_TITLE)) {
        mb_newDataInitialized = true;
        title = "";
      }
    }
  }
}