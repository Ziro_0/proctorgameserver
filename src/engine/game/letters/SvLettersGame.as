/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvLettersGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-13-2018
  ActionScript:  3.0
  Description:  
  History:
    07-13-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.game.letters {

  import engine.I_SvEngine;
  import engine.common.data.PlayerGameProgress;
  import engine.data.settings.letters.SvLettersEntrySettings;
  import engine.game.SvGame;
	
  public class SvLettersGame extends SvGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvLettersGame(o_engine:I_SvEngine, s_gameId:String) {
      super(o_engine, s_gameId);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onGetPointsFromPlayerProgress
    //==================================================================
    override protected function onGetPointsFromPlayerProgress(o_progress:PlayerGameProgress):int {
      var i_points:int = 0;
      
      for (var i_index:int = 0; i_index < settings.entries.length; i_index++) {
        var o_entrySettings:SvLettersEntrySettings =
          new SvLettersEntrySettings(settings.entries[i_index]);
        var as_playerWords:Array = o_progress.responses[i_index] as Array;
        var i_numCorrectWords:int = getNumCorrectWords(as_playerWords, o_entrySettings);
        
        i_points += i_numCorrectWords;
      }
      
      return(i_points);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // isPlayerWordCorrect
    //==================================================================
    private function isPlayerWordCorrect(s_playerWord:String, as_letterAnswers:Array):Boolean {
      if (!s_playerWord) {
        return(false);
      }
      
      for each (var s_letterAnswer:String in as_letterAnswers) {
        if (s_letterAnswer != null) {
          s_letterAnswer = s_letterAnswer.toLowerCase();
        }
        
        if (s_playerWord == s_letterAnswer) {
          return(true);
        }
      }
      
      return(false);
    }
    
    //==================================================================
    // getNumCorrectWords
    //==================================================================
    private function getNumCorrectWords(as_playerWords:Array,
    o_entrySettings:SvLettersEntrySettings):int {
      var ao_entries:Array = o_entrySettings.entries;
      if (!as_playerWords || !ao_entries) {
        return(0);
      }
      
      var i_numCorrectWords:int = 0;
      var i_numEntriesTocCheck:int = Math.min(as_playerWords.length, ao_entries.length);
      for (var i_index:int = 0; i_index < i_numEntriesTocCheck; i_index++) {
        var as_letterAnswers:Array = ao_entries[i_index];
        var s_playerWord:String = as_playerWords[i_index] || "";
        
        if (isPlayerWordCorrect(s_playerWord.toLowerCase(), as_letterAnswers)) {
          i_numCorrectWords++;
        }
      }
      
      return(i_numCorrectWords);
    }
  }
}