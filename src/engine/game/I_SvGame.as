/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvGame

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.game {
  import engine.I_SvEngine;
  import flib.utils.FDataBlock;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_SvGame {
    function getEngine():I_SvEngine;
    function sendStateToClients():void;
    function get state():FDataBlock;
  }
}