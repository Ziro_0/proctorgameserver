/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvFillInBlankGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-26-2018
  ActionScript:  3.0
  Description:  
  History:
    07-26-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.game.fillInBlank {
  import engine.I_SvEngine;
  import engine.common.data.PlayerGameProgress;
  import engine.data.settings.fillInBlank.SvFillInBlankEntrySettings;
  import engine.game.SvGame;
	
  public class SvFillInBlankGame extends SvGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvFillInBlankGame(o_engine:I_SvEngine, s_gameId:String) {
      super(o_engine, s_gameId);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onGetPointsFromPlayerProgress
    //==================================================================
    override protected function onGetPointsFromPlayerProgress(o_progress:PlayerGameProgress):int {
      var i_points:int = 0;
      
      for (var i_index:int = 0; i_index < settings.entries.length; i_index++) {
        var o_entrySettings:SvFillInBlankEntrySettings =
          new SvFillInBlankEntrySettings(settings.entries[i_index]);
        var s_playerWord:String = o_progress.responses[i_index] as String;
        if (isPlayerWordCorrect(s_playerWord, o_entrySettings.answers)) {
          i_points++;
        }
      }
      
      return(i_points);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // isPlayerWordCorrect
    //==================================================================
    private function isPlayerWordCorrect(s_playerWord:String, as_answers:Array):Boolean {
      if (!s_playerWord) {
        return(false);
      }
      
      for each (var s_answer:String in as_answers) {
        if (s_answer != null) {
          s_answer = s_answer.toLowerCase();
        }
        
        if (s_playerWord == s_answer) {
          return(true);
        }
      }
      
      return(false);
    }
  }
}