/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGame

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.game {
  import com.greensock.loading.LoaderMax;
  import engine.I_SvEngine;
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.ServerMessageTypes;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import engine.common.data.messageKeys.RoundTimerUpdateKeys;
  import engine.common.data.messageKeys.RoundWinnerKeys;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.common.utils.messages.Message;
  import engine.data.settings.SvMiniGameBaseSettings;
  import engine.ui.dialogs.game.SvGameBaseDialog;
  import engine.ui.dialogs.games.SvGamesDialog;
  import flash.events.TimerEvent;
  import flib.utils.FDataBlock;
  import flib.utils.timers.FTimer;
  import flib.utils.timers.FTimerSystem;

  public class SvGame implements I_SvGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const DELAY_ONE_SECOND:Number = 1000;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_SvEngine;
    private var mo_settings:SvMiniGameBaseSettings;
    
    private var mo_imagesLoader:LoaderMax;
    
    private var mo_timerSystem:FTimerSystem;
    private var mo_gameTimer:FTimer;
    
    private var ms_gameId:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGame(o_engine:I_SvEngine, s_gameId:String):void {
      super();
      init(o_engine, s_gameId);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getEngine
    //==================================================================
    public function getEngine():I_SvEngine {
      return(mo_engine);
    }
    
    //==================================================================
    // handleClientJoin
    //==================================================================
    public function handleClientJoin(s_clientId:String):void {
    }
    
    //==================================================================
    // handleClientLeft
    //==================================================================
    public function handleClientLeft(s_clientId:String):void {
      removeUserFromDataGrid(s_clientId);
    }
    
    //==================================================================
    // handleClientNext
    //==================================================================
    public function handleClientNext(s_clientId:String, o_response:Object):void {
      var o_playerData:FDataBlock = getEngine().getPlayerState(s_clientId);
      var o_progress:PlayerGameProgress = getPlayerProgress(o_playerData);
      if (!o_progress) {
        getEngine().traceAndLog("SvGame.handleClientNext. Warning: Unable to get progress data from player " +
          "with id: " + s_clientId);
        return;
      }
      
      //update response data
      var u_currentRound:uint = o_progress.round;
      o_progress.responses[u_currentRound] = o_response;
      
      //update round
      var u_maxRounds:uint = settings.entries.length;
      var u_nextRound:uint = u_currentRound + 1;
      if (u_nextRound < u_maxRounds) {
        o_progress.round = u_nextRound;
      } else {
        o_progress.isComplete = true;
      }
      
      //update ui
      var o_view:SvGameBaseDialog = getGameDialog();
      if (o_view) {
        o_view.updateUser(o_playerData);
      }
    }
    
    //==================================================================
    // handleClientWatch
    //==================================================================
    public function handleClientWatch(s_clientId:String):void {
    }
    
    //==================================================================
    // id (get)
    //==================================================================
    public function get id():String {
      return(ms_gameId);
    }
    
    //==================================================================
    // sendMessageToClients
    //==================================================================
    public function sendMessageToClients(o_message:Object):void {
      if (mo_engine) {
        mo_engine.sendMessageToClients(o_message);
      }
    }
    
    //==================================================================
    // sendStateToClients
    //==================================================================
    public function sendStateToClients():void {
      if (mo_engine) {
        mo_engine.sendStateToClients();
      }
    }
    
    //==================================================================
    // settings (get)
    //==================================================================
    public function get settings():SvMiniGameBaseSettings {
      return(mo_settings);
    }
    
    //==================================================================
    // start
    //==================================================================
    public function start():void {
      mo_gameTimer = mo_timerSystem.removeTimer(mo_gameTimer);
      mo_gameTimer = mo_timerSystem.addTimer(DELAY_ONE_SECOND, mo_settings.time, onRoundTimer,
        onRoundTimerComplete);
      onUpdateGameTimer(mo_gameTimer.currentCount, mo_gameTimer.repeatCount);
      notifyGameTimer();
    }
    
    //==================================================================
    // state (get)
    //==================================================================
    public function get state():FDataBlock {
      if (mo_engine) {
        return(mo_engine.state);
      }
      return(null);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      uninitLoader();
      mo_gameTimer = null;
      mo_timerSystem.removeAllTimers();
      
      if (mo_engine) {
        mo_engine = null;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addUserToDataGrid
    //==================================================================
    protected function addUserToDataGrid(s_newClientId:String):void {
      var o_view:SvGameBaseDialog = getGameDialog();
      if (o_view) {
        o_view.addUser(getEngine().getPlayerState(s_newClientId));
      }
    }
    
    //==================================================================
    // getGameDialog
    //==================================================================
    protected function getGameDialog():SvGameBaseDialog {
      var o_gamesDialog:SvGamesDialog = getGamesDialog();
      return(o_gamesDialog ? o_gamesDialog.gameDialog : null);
    }
    
    //==================================================================
    // getPlayerProgress
    //==================================================================
    protected function getPlayerProgress(o_clientIdOrPlayerState:Object):PlayerGameProgress {
      var o_playerData:FDataBlock;
      if (o_clientIdOrPlayerState is FDataBlock) {
        o_playerData = o_clientIdOrPlayerState as FDataBlock;
      } else if (o_clientIdOrPlayerState is String) {
        o_playerData = getEngine().getPlayerState(o_clientIdOrPlayerState as String);
      }
      
      return(o_playerData ?
        o_playerData.getObject(PlayerStateKeys.GAME_PROGRESS) as PlayerGameProgress :
        null);
    }
    
    //==================================================================
    // onGetPointsFromPlayerProgress
    //==================================================================
    protected function onGetPointsFromPlayerProgress(o_progress:PlayerGameProgress):int {
      trace("SvGame.onGetPointsFromPlayerProgress. " +
        "Warning: This base function should be overridden. " + this);
      return(0);
    }
    
    //==================================================================
    // onGetWinningPlayers
    //==================================================================
    protected function onGetWinningPlayers():Array {
      var as_clientIds:Array = getEngine().getActiveClientIds();
      
      //For details of the element format, see comments in RoundWinnerKeys.
      var ao_clientPoints:Array = [];
      
      for each (var s_clientId:String in as_clientIds) {
        var o_progress:PlayerGameProgress = getPlayerProgress(s_clientId);
        ao_clientPoints.push({
          id: s_clientId,
          points: onGetPointsFromPlayerProgress(o_progress)
        });
      }
      
      return(sortClientPoints(ao_clientPoints));
    }
    
    //==================================================================
    // onInit
    //==================================================================
    protected function onInit():void {
    }
    
    //==================================================================
    // onUpdateGameTimer
    //==================================================================
    protected function onUpdateGameTimer(i_currentCount:int, i_repeatCount:int):void {
      var o_dialog:SvGameBaseDialog = getGameDialog();
      if (o_dialog) {
        o_dialog.currentGameTime = i_repeatCount - i_currentCount;
      }
    }
    
    //==================================================================
    // removeUserFromDataGrid
    //==================================================================
    protected function removeUserFromDataGrid(s_clientId:String = null):void {
      var o_view:SvGameBaseDialog = getGameDialog();
      if (o_view) {
        o_view.removeUser(s_clientId);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // determineWinner
    //==================================================================
    private function determineWinner():void {
      var ao_winningPlayers:Array = onGetWinningPlayers() || [];
      
      var o_message:Object = Message.Create(ServerMessageTypes.ROUND_WINNER);
      o_message[RoundWinnerKeys.WINNING_PLAYERS] = ao_winningPlayers;
      sendMessageToClients(o_message);
    }
    
    //==================================================================
    // getGamesDialog
    //==================================================================
    private function getGamesDialog():SvGamesDialog {
      return(getEngine().dialogManager.dialog as SvGamesDialog);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_SvEngine, s_gameId:String):void {
      mo_engine = o_engine;
      ms_gameId = s_gameId;
      mo_timerSystem = new FTimerSystem();
      mo_settings = new SvMiniGameBaseSettings(mo_engine.settingsData.games[ms_gameId]);
      onInit();
    }
    
    //==================================================================
    // notifyGameTimer
    //==================================================================
    private function notifyGameTimer():void {
      var o_message:Object = Message.Create(ServerMessageTypes.ROUND_TIMER_UPDATE);
      o_message[RoundTimerUpdateKeys.ELAPSED] = mo_gameTimer.currentCount;
      o_message[RoundTimerUpdateKeys.MAX] = mo_gameTimer.repeatCount;
      sendMessageToClients(o_message);
    }
    
    //==================================================================
    // onRoundTimerComplete
    //==================================================================
    private function onRoundTimerComplete(o_event:TimerEvent):void {
      notifyGameTimer();
      mo_gameTimer = null;
      determineWinner();
    }
    
    //==================================================================
    // onRoundTimer
    //==================================================================
    private function onRoundTimer(o_event:TimerEvent):void {
      onUpdateGameTimer(mo_gameTimer.currentCount, mo_gameTimer.repeatCount);
      notifyGameTimer();
    }
    
    //==================================================================
    // sortClientPoints
    //==================================================================
    private function sortClientPoints(ao_clientPoints:Array):Array {
      const PROP_POINTS:String = "points";
      ao_clientPoints.sortOn(PROP_POINTS, Array.DESCENDING);
      return(ao_clientPoints);
    }
    
    //==================================================================
    // uninitLoader
    //==================================================================
    private function uninitLoader():void {
      if (!mo_imagesLoader) {
        return;
      }
      
      mo_imagesLoader.cancel();
      mo_imagesLoader.unload();
      mo_imagesLoader = null;
    }
  }
}