/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultipleChoiceGame

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-06-2018
  ActionScript:  3.0
  Description:  
  History:
    07-06-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.game.multipleChoice {
  import engine.I_SvEngine;
  import engine.common.data.PlayerGameProgress;
  import engine.data.settings.multipleChoice.SvMultChoiceEntrySettings;
  import engine.data.settings.multipleChoice.SvMultChoiceQuestionOptionSettings;
  import engine.game.SvGame;
	
  public class SvMultipleChoiceGame extends SvGame {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMultipleChoiceGame(o_engine:I_SvEngine, s_gameId:String) {
      super(o_engine, s_gameId);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onGetPointsFromPlayerProgress
    //==================================================================
    override protected function onGetPointsFromPlayerProgress(o_progress:PlayerGameProgress):int {
      var i_points:int = 0;
      var o_optionSettings:SvMultChoiceQuestionOptionSettings =
        new SvMultChoiceQuestionOptionSettings(null);
      
      //if player did not finish, the progress responses array may contain fewer entries than
      // the max number of entries
      var i_numEntriesToTest:int = Math.min(settings.entries.length, o_progress.responses.length);
      
      for (var i_index:int = 0; i_index < i_numEntriesToTest; i_index++) {
        var o_entrySettings:SvMultChoiceEntrySettings = 
          new SvMultChoiceEntrySettings(settings.entries[i_index]);
        var i_playerSelectedChoiceIndex:int = o_progress.responses[i_index] as int;
        
        for (var i_choiceIndex:int = 0; i_choiceIndex < o_entrySettings.choices.length;
        i_choiceIndex++) {
          o_optionSettings.data = o_entrySettings.choices[i_choiceIndex];
          
          if (o_optionSettings.isCorrect && i_playerSelectedChoiceIndex == i_choiceIndex) {
            i_points++;
            break;
          }
        }
      }
      
      return(i_points);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}