/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvUsersDataGridView

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-19-2017
  ActionScript:	3.0
  Description:	
  History:
    05-19-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.usersDataGrid {
  import engine.common.baseView.BaseView;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.dataGrid.SvBaseDataGridUserItem;
  import fl.controls.DataGrid;
  import fl.controls.dataGridClasses.DataGridColumn;
  import flash.events.Event;
  import flib.utils.FDataBlock;
	
  public class SvUsersDataGridView extends BaseView implements I_SvDataGridView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_dataGrid:DataGrid;
    private var mo_serverState:FDataBlock;
    private var mo_dataItemClass:Class;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvUsersDataGridView(o_spriteOrSpriteClass:Object, o_serverState:FDataBlock,
    ao_dataGridHeadersData:Array, o_dataItemClass:Class, o_callbacks:I_SvDataGridViewCallbacks = null):void {
      super(o_spriteOrSpriteClass, o_callbacks);
			init(o_serverState, o_dataItemClass, ao_dataGridHeadersData);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addUser
    //==================================================================
    public function addUser(o_playerState:FDataBlock):Boolean {
      if (!mo_serverState) {
        return(false);
      }
      
      var o_userData:UserData =
        o_playerState ? o_playerState.getObject(PlayerStateKeys.USER_DATA) as UserData : null;
      if (!o_userData || !o_userData.id) {
        return(false); //sanity checks
      }
      
      var o_itemData:SvBaseDataGridUserItem =
        new mo_dataItemClass(mo_serverState, o_playerState) as SvBaseDataGridUserItem;
      mo_dataGrid.addItem(o_itemData);
      return(true);
    }
    
    //==================================================================
    // isItemSelected (get)
    //==================================================================
    public function get isItemSelected ():Boolean {
      return(selectedItemUserId != null);
    }
    
    //==================================================================
    // removeAllUsers
    //==================================================================
    public function removeAllUsers():void {
      mo_dataGrid.removeAll();
    }
    
    //==================================================================
    // removeUser
    //==================================================================
    public function removeUser(s_id:String):Boolean {
      var o_dataItem:SvBaseDataGridUserItem = getUser(s_id);
      if (!o_dataItem) {
        return(false);
      }
      
      mo_dataGrid.removeItem(o_dataItem);
      return(true);
    }
    
    //==================================================================
    // selectedItemUserId (get)
    //==================================================================
    public function get selectedItemUserId():String {
      var o_item:SvBaseDataGridUserItem = mo_dataGrid.selectedItem as SvBaseDataGridUserItem;
      return(o_item ? o_item.id : null);
    }
    
    //==================================================================
    // update
    //==================================================================
    public function update():void {
      for (var u_index:uint = 0; u_index < mo_dataGrid.length; u_index++) {
        var o_itemData:SvBaseDataGridUserItem = mo_dataGrid.getItemAt(u_index) as SvBaseDataGridUserItem;
        o_itemData.update();
      }
      mo_dataGrid.invalidateList();
    }
    
    //==================================================================
    // updateUser
    //==================================================================
    public function updateUser(o_playerState:FDataBlock):Boolean {
      if (!mo_serverState) {
        return(false);
      }
      
      var o_userData:UserData =
        o_playerState ? o_playerState.getObject(PlayerStateKeys.USER_DATA) as UserData : null;
      if (!o_userData || !o_userData.id) {
        return(false); //sanity checks
      }
      
      var o_itemData:SvBaseDataGridUserItem = getUser(o_userData.id);
      if (!o_itemData) {
        return(false);
      }
      
      o_itemData.update();
      mo_dataGrid.invalidateList();
      return(true);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addExistingUsers
    //==================================================================
    private function addExistingUsers():void {
      var o_playerStates:FDataBlock = mo_serverState.getBlock(ServerStateKeys.PLAYERS);
      var as_clientIds:Array = o_playerStates.getKeys();
      for each (var s_clientId:String in as_clientIds) {
        var o_playerState:FDataBlock = o_playerStates.getBlock(s_clientId);
        addUser(o_playerState);
      }
    }
    
    //==================================================================
    // getUser
    //==================================================================
    private function getUser(s_userId:String):SvBaseDataGridUserItem {
      for (var u_index:uint = 0; u_index < mo_dataGrid.length; u_index++) {
        var o_item:SvBaseDataGridUserItem = mo_dataGrid.getItemAt(u_index) as SvBaseDataGridUserItem;
        if (o_item.id == s_userId) {
          return(o_item);
        }
      }
      
      return(null);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_serverState:FDataBlock, o_dataItemClass:Class, ao_dataGridHeadersData:Array):void {
      mo_serverState = o_serverState;
      mo_dataItemClass = o_dataItemClass || SvBaseDataGridUserItem;
      mo_dataGrid = display as DataGrid;
      mo_dataGrid.addEventListener(Event.CHANGE, onDataGridChange, false, 0, true);
      initColumnHeaders(ao_dataGridHeadersData);
      addExistingUsers();
    }
    
    //==================================================================
    // initColumnHeaders
    //==================================================================
    private function initColumnHeaders(ao_dataGridHeadersData:Array):void {
      for each (var o_cellData:FDataBlock in ao_dataGridHeadersData) {
        var o_column:DataGridColumn = new DataGridColumn();
        if (o_cellData.containsProperty("headerText")) {
          o_column.headerText = o_cellData.getString("headerText");
        } else {
          o_column.visible = false;
        }
        
        o_column.dataField = o_cellData.getString("dataField");
        mo_dataGrid.addColumn(o_column);
      }
    }
    
    //==================================================================
    // onDataGridChange
    //==================================================================
    private function onDataGridChange(o_event:Event):void {
      var o_callbacks:I_SvDataGridViewCallbacks = callbacks as I_SvDataGridViewCallbacks;
      if (o_callbacks) {
        o_callbacks.dataGridViewOnChange(this);
      }
    }
  }
}