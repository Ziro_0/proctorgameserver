/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMainView

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-15-2017
  ActionScript:	3.0
  Description:	
  History:
    05-15-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.mainView {
  import engine.common.baseView.BaseView;
  import engine.common.ui.buttons.Button;
  import engine.common.utils.AppDescriptorData;
  import engine.data.SvDebugData;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.usersDataGrid.SvUserDataItem;
  import engine.ui.usersDataGrid.SvUsersDataGridView;
  import fl.controls.UIScrollBar;
  import flash.events.Event;
  import flash.events.FocusEvent;
  import flash.text.TextField;
  import flib.utils.FDataBlock;

  public class SvMainView extends BaseView implements I_SvDataGridViewCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const INITIAL_IP_ADDRESS:String = SvDebugData.IP_ADDRESS;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_usersDataGridView:SvUsersDataGridView;
    
    private var mo_startButton:Button;
    private var mo_stopButton:Button;
    private var mo_clearLogButton:Button;
    private var mo_gamesButton:Button;
    private var mo_themesButton:Button;
    private var mo_settingsButton:Button;
    private var mo_importButton:Button;
    
    private var mo_txfIpAddress:TextField;
    private var mo_txfPort:TextField;
    private var mo_txfLog:TextField;
    
    private var ms_startText:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMainView(o_serverState:FDataBlock, o_callbacks:I_SvMainViewCallbacks = null):void {
      super(MainViewDisplay, o_callbacks);
			init(o_serverState);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dataGridViewOnChange
    //==================================================================
    public function dataGridViewOnChange(o_dataGridView:I_SvDataGridView):void {
      updateUserControlButtonsEnability();
    }
    
    //==================================================================
    // inputTextsEnabled (set)
    //==================================================================
    public function set inputTextsEnabled(b_value:Boolean):void {
      mo_txfIpAddress.mouseEnabled = mo_txfPort.mouseEnabled = b_value;
      
      if (b_value) {
        mo_txfIpAddress.textColor = mo_txfPort.textColor = 0x000000;
        mo_txfIpAddress.backgroundColor = mo_txfPort.backgroundColor = 0xffffff;
      } else {
        mo_txfIpAddress.textColor = mo_txfPort.textColor = 0x707070;
        mo_txfIpAddress.backgroundColor = mo_txfPort.backgroundColor = 0xbbbbbb;
        
        if (display.stage.focus == mo_txfIpAddress || display.stage.focus == mo_txfPort) {
          display.stage.focus = null;
        }
      }
    }
    
    //==================================================================
    // ipAddress (get)
    //==================================================================
    public function get ipAddress():String {
      return(mo_txfIpAddress.text || "");
    }
    
    //==================================================================
    // ipAddress (set)
    //==================================================================
    public function set ipAddress(s_value:String):void {
      mo_txfIpAddress.text = s_value || "";
    }
    
    //==================================================================
    // logTextField (get)
    //==================================================================
    public function get logTextField():TextField {
      return(mo_txfLog);
    }
    
    //==================================================================
    // port (get)
    //==================================================================
    public function get port():uint {
      return(parseInt(mo_txfPort.text));
    }
    
    //==================================================================
    // port (set)
    //==================================================================
    public function set port(u_value:uint):void {
      mo_txfPort.text = u_value.toString();
    }
    
    //==================================================================
    // startButton (get)
    //==================================================================
    public function get startButton():Button {
      return(mo_startButton);
    }
    
    //==================================================================
    // stopButton (get)
    //==================================================================
    public function get stopButton():Button {
      return(mo_stopButton);
    }
    
    //==================================================================
    // themesButton (get)
    //==================================================================
    public function get themesButton():Button {
      return(mo_themesButton);
    }
    
    //==================================================================
    // usersDataGridView (get)
    //==================================================================
    public function get usersDataGridView():SvUsersDataGridView {
      return(mo_usersDataGridView);
    }
    
    //==================================================================
    // updatePlayersList
    //==================================================================
    public function updatePlayersList():void {
      mo_usersDataGridView.update();
    }
    
    //==================================================================
    // version (set)
    //==================================================================
    public function set version(s_value:String):void {
      getTextField("txf_version", s_value);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // enableStartButton
    //==================================================================
    private function enableStartButton():void {
      mo_startButton.enabled = isIpAddressValid() && isPortValid();
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_serverState:FDataBlock):void {
      mo_usersDataGridView = new SvUsersDataGridView(
        getSprite("dg_user_display"),
        o_serverState,
        SvMainUsersDataGridColumnsData.HEADER_DATA,
        SvUserDataItem,
        this);
      initButtons();
      initLogTextField();
      initIpAddressTextField();
      initPortTextField();
      updateLogScrollBar();
      initHeaderText();
      
      display.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_startButton = initButton("start_button_display", onStartButtonClick);
      mo_stopButton = initButton("stop_button_display", onStopButtonClick, false, false);
      mo_clearLogButton = initButton("clear_log_button_display", onClearLogButtonClick, false);
      mo_gamesButton = initButton("games_button_display", onGamesButtonClick, false);
      mo_themesButton = initButton("themes_button_display", onThemesButtonClick, false);
      mo_settingsButton = initButton("settings_button_display", onSettingsButtonClick);
      mo_importButton = initButton("import_button_display", onImportButtonClick);
      updateUserControlButtonsEnability();
    }
    
    //==================================================================
    // initHeaderText
    //==================================================================
    private function initHeaderText():void {
      getTextField("txtHeader", AppDescriptorData.Name);
    }
    
    //==================================================================
    // initIpAddressTextField
    //==================================================================
    private function initIpAddressTextField():void {
      mo_txfIpAddress = getTextField("txf_ip_address", INITIAL_IP_ADDRESS);
      mo_txfIpAddress.restrict = "0-9.";
      mo_txfIpAddress.addEventListener(Event.CHANGE, onTxfIpAddressChange, false, 0, true);
      mo_txfIpAddress.addEventListener(FocusEvent.FOCUS_IN, onTxfFocusIn, false, 0, true);
      mo_txfIpAddress.addEventListener(FocusEvent.FOCUS_OUT, onTxfIpAddressFocusOut, false, 0, true);
    }
    
    //==================================================================
    // initLogTextField
    //==================================================================
    private function initLogTextField():void {
      mo_txfLog = getTextField("txf_log", "");
      mo_txfLog.addEventListener(Event.CHANGE, onTxfLogChange, false, 0, true);
      
      var o_scrollBar:UIScrollBar = getSprite("scr_log") as UIScrollBar;
      o_scrollBar.move(mo_txfLog.x + mo_txfLog.width, mo_txfLog.y);
      o_scrollBar.height = mo_txfLog.height;
      o_scrollBar.update();
    }
    
    //==================================================================
    // initPortTextField
    //==================================================================
    private function initPortTextField():void {
      mo_txfPort = getTextField("txf_port");
      mo_txfPort.restrict = "0-9";
      mo_txfPort.addEventListener(Event.CHANGE, onTxfPortChange, false, 0, true);
      mo_txfPort.addEventListener(FocusEvent.FOCUS_IN, onTxfFocusIn, false, 0, true);
      mo_txfPort.addEventListener(FocusEvent.FOCUS_OUT, onTxfPortFocusOut, false, 0, true);
    }
    
    //==================================================================
    // isIpAddressValid
    //==================================================================
    private function isIpAddressValid():Boolean {
      var regEx:RegExp = /\b(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\b/;
      return(regEx.test(ipAddress));
    }
    
    //==================================================================
    // isPortValid
    //==================================================================
    private function isPortValid():Boolean {
      if (!mo_txfPort.text) {
        return(false);
      }
      return(port < 65536);
    }
    
    //==================================================================
    // notifyUpdateSettings
    //==================================================================
    private function notifyUpdateSettings():void { 
      var o_callbacks:I_SvMainViewCallbacks = callbacks as I_SvMainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnUpdateSettings();
      }
    }
    
    //==================================================================
    // onAddedToStage
    //==================================================================
    private function onAddedToStage(o_event:Event):void {
      removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
      display.stage.focus = mo_txfIpAddress;
      enableStartButton();
    }
    
    //==================================================================
    // onClearLogButtonClick
    //==================================================================
    private function onClearLogButtonClick(b_shift:Boolean):void {
      mo_txfLog.text = "";
      mo_txfLog.dispatchEvent(new Event(Event.CHANGE));
    }
    
    //==================================================================
    // onImportButtonClick
    //==================================================================
    private function onImportButtonClick(b_shift:Boolean):void {
      var o_callbacks:I_SvMainViewCallbacks = callbacks as I_SvMainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnImport();
      }
    }
    
    //==================================================================
    // onGamesButtonClick
    //==================================================================
    private function onGamesButtonClick(b_shift:Boolean):void {
      var o_callbacks:I_SvMainViewCallbacks = callbacks as I_SvMainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnGames();
      }
    }
    
    //==================================================================
    // onSettingsButtonClick
    //==================================================================
    private function onSettingsButtonClick(b_shift:Boolean):void {
      var o_callbacks:I_SvMainViewCallbacks = callbacks as I_SvMainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnSettingsFile();
      }
    }
    
    //==================================================================
    // onThemesButtonClick
    //==================================================================
    private function onThemesButtonClick(b_shift:Boolean):void {
      var o_callbacks:I_SvMainViewCallbacks = callbacks as I_SvMainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnThemes();
      }
    }
    
    //==================================================================
    // onStartButtonClick
    //==================================================================
    private function onStartButtonClick(b_shift:Boolean):void {
      var o_callbacks:I_SvMainViewCallbacks = callbacks as I_SvMainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnStartServer();
      }
    }
    
    //==================================================================
    // onStopButtonClick
    //==================================================================
    private function onStopButtonClick(b_shift:Boolean):void {
      var o_callbacks:I_SvMainViewCallbacks = callbacks as I_SvMainViewCallbacks;
      if (o_callbacks) {
        o_callbacks.mainViewOnStopServer();
      }
    }
    
    //==================================================================
    // onTxfFocusIn
    //==================================================================
    private function onTxfFocusIn(o_event:FocusEvent):void {
      var o_textField:TextField = o_event.currentTarget as TextField;
      ms_startText = o_textField.text;
    }
    
    //==================================================================
    // onTxfIpAddressChange
    //==================================================================
    private function onTxfIpAddressChange(o_event:Event):void {
      enableStartButton();
    }
    
    //==================================================================
    // onTxfIpAddressFocusOut
    //==================================================================
    private function onTxfIpAddressFocusOut(o_event:FocusEvent):void {
      if (ms_startText != mo_txfIpAddress.text) {
        notifyUpdateSettings();
      }
    }
    
    //==================================================================
    // onTxfLogChange
    //==================================================================
    private function onTxfLogChange(o_event:Event):void {
      updateLogScrollBar();
    }
    
    //==================================================================
    // onTxfPortChange
    //==================================================================
    private function onTxfPortChange(o_event:Event):void {
      enableStartButton();
    }
    
    //==================================================================
    // onTxfPortFocusOut
    //==================================================================
    private function onTxfPortFocusOut(o_event:FocusEvent):void {
      if (ms_startText != mo_txfPort.text) {
        notifyUpdateSettings();
      }
    }
    
    //==================================================================
    // updateLogScrollBar
    //==================================================================
    private function updateLogScrollBar():void {
      var o_scrollBar:UIScrollBar = getSprite("scr_log") as UIScrollBar;
      if (mo_txfLog.textHeight < mo_txfLog.height) {
        o_scrollBar.visible = false;
      } else {
        o_scrollBar.visible = true;
        o_scrollBar.update();
        o_scrollBar.scrollPosition = o_scrollBar.maxScrollPosition;
      }
    }
    
    //==================================================================
    // updateUserControlButtonsEnability
    //==================================================================
    private function updateUserControlButtonsEnability():void {
    }
  }
}