/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvMainViewCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-15-2017
  ActionScript:	3.0
  Description:	
  History:
    05-15-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.mainView {
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.common.data.UserData;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_SvMainViewCallbacks extends I_BaseViewCallbacks {
    function mainViewOnImport():void;
    function mainViewOnGames():void;
    function mainViewOnSettingsFile():void;
    function mainViewOnStartServer():void;
    function mainViewOnStopServer():void;
    function mainViewOnThemes():void;
    function mainViewOnUpdateSettings():void;
  }
}