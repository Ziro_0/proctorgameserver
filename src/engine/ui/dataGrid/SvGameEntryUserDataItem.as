/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGameEntryUserDataItem

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          09-04-2018
  ActionScript:  3.0
  Description:  
  History:
    09-04-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dataGrid {
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import flib.utils.FDataBlock;
  
  public class SvGameEntryUserDataItem extends SvBaseDataGridUserItem {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const STRING_DONE:String = "Done!";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public var round:Object;
    public var response:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGameEntryUserDataItem(o_serverState:FDataBlock, o_playerState:FDataBlock) {
      super(o_serverState, o_playerState);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // update
    //==================================================================
    override public function update():void {
      super.update();
      
      var o_progress:PlayerGameProgress = getPlayerGameProgress();
      if (o_progress) {
        updateRoundWithData(o_progress);
      } else {
        updateRoundWithNoData(o_progress);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // updateRoundWithData
    //==================================================================
    private function updateRoundWithData(o_progress:PlayerGameProgress):void {
      var u_round:uint = o_progress.round;
      round = o_progress.isComplete ? STRING_DONE : u_round + 1;
      
      if (o_progress.isComplete) {
        response = o_progress.responses[u_round];
      } else {
        response = o_progress.responses[u_round == 0 ? u_round : u_round - 1];
      }
    }
    
    //==================================================================
    // updateRoundWithNoData
    //==================================================================
    private function updateRoundWithNoData(o_progress:PlayerGameProgress):void {
      round = 0;
      response = null;
    }
  }
}