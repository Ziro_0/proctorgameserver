/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGameEntryUsersDataGrid

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          09-04-2018
  ActionScript:  3.0
  Description:  
  History:
    09-04-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dataGrid {

	import engine.common.baseView.BaseView;
	import engine.common.baseView.I_BaseViewCallbacks;
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import fl.controls.DataGrid;
  import fl.controls.dataGridClasses.DataGridColumn;
  import flib.utils.FDataBlock;
	
  public class SvGameEntryUsersDataGrid extends BaseView implements I_SvDataGridView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_dataGrid:DataGrid;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGameEntryUsersDataGrid(o_spriteOrSpriteClass:Object,
    o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, o_callbacks);
      init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addUser
    //==================================================================
    public function addUser(o_playerState:FDataBlock):Boolean {
      var o_userData:UserData = getUserData(o_playerState);
      if (!o_userData || !o_userData.id) {
        return(false); //sanity checks
      }
      
      var o_itemData:SvGameEntryUserDataItem = createDataItem(o_playerState);
      mo_dataGrid.addItem(o_itemData);
      return(true);
    }
    
    //==================================================================
    // getIndexOf
    //==================================================================
    public function getIndexOf(o_item:SvGameEntryUserDataItem):int {
      if (!o_item) {
        return( -1);
      }
      
      for (var i_index:int = 0; i_index < mo_dataGrid.length; i_index++) {
        var o_dataItem:SvGameEntryUserDataItem = mo_dataGrid.getItemAt(i_index) as SvGameEntryUserDataItem;
        if (o_dataItem == o_item || o_dataItem.userId == o_item.userId) {
          return(i_index);
        }
      }
      
      return( -1);
    }
    
    //==================================================================
    // isItemSelected (get)
    //==================================================================
    public function get isItemSelected():Boolean {
      return(selectedItem != null);
    }
    
    //==================================================================
    // removeAll
    //==================================================================
    public function removeAll():void {
      mo_dataGrid.removeAll();
    }
    
    //==================================================================
    // removeItem
    //==================================================================
    public function removeItem(o_item:SvGameEntryUserDataItem):void {
      mo_dataGrid.removeItem(o_item);
    }
    
    //==================================================================
    // selectedItem (get)
    //==================================================================
    public function get selectedItem():SvGameEntryUserDataItem {
      return(mo_dataGrid.selectedItem as SvGameEntryUserDataItem);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createDataItem
    //==================================================================
    private function createDataItem(o_playerState:FDataBlock):SvGameEntryUserDataItem {
      var o_userData:UserData = getUserData(o_playerState);
      var o_progress:PlayerGameProgress = getPlayerGameProgress(o_playerState);
      return(new SvGameEntryUserDataItem(o_userData.id, o_userData.name, o_progress.round, o_progress.round));
    }
    
    //==================================================================
    // getPlayerGameProgress
    //==================================================================
    private function getPlayerGameProgress(o_playerState:FDataBlock):PlayerGameProgress {
      return(o_playerState.getObject(PlayerStateKeys.GAME_PROGRESS) as PlayerGameProgress);
    }
    
    //==================================================================
    // getUserData
    //==================================================================
    private function getUserData(o_playerState:FDataBlock):UserData {
      return(o_playerState.getObject(PlayerStateKeys.USER_DATA) as UserData);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      mo_dataGrid = display as DataGrid;
      mo_dataGrid.addEventListener(Event.CHANGE, onDataGridChange, false, 0, true);
      initColumnHeaders();
    }
    
    //==================================================================
    // initColumnHeaders
    //==================================================================
    private function initColumnHeaders():void {
      for each (var o_cellData:FDataBlock in SvGameEntryUsersDataGridColumnsData.HEADER_DATA) {
        var o_column:DataGridColumn = new DataGridColumn();
        if (o_cellData.containsProperty("headerText")) {
          o_column.headerText = o_cellData.getString("headerText");
        } else {
          o_column.visible = false;
        }
        
        o_column.dataField = o_cellData.getString("dataField");
        mo_dataGrid.addColumn(o_column);
      }
    }
    
    //==================================================================
    // onDataGridChange
    //==================================================================
    private function onDataGridChange(o_event:Event):void {
      var o_callbacks:I_SvDataGridViewCallbacks = callbacks as I_SvDataGridViewCallbacks;
      if (o_callbacks) {
        o_callbacks.dataGridViewOnChange(this);
      }
    }
  }
}