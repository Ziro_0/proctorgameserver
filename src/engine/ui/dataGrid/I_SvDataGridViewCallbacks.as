/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvDataGridViewCallbacks

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-22-2017
  ActionScript:	3.0
  Description:	
  History:
    05-22-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dataGrid {
  import engine.common.baseView.I_BaseViewCallbacks;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_SvDataGridViewCallbacks extends I_BaseViewCallbacks {
    function dataGridViewOnChange(o_dataGridView:I_SvDataGridView):void;
  }
}