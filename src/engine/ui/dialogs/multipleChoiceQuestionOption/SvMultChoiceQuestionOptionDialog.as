/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultChoiceQuestionOptionDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-28-2018
  ActionScript:  3.0
  Description:  
  History:
    06-28-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.multipleChoiceQuestionOption {

  import engine.common.ui.buttons.Button;
  import engine.common.ui.buttons.ToggleButton;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.utils.inputLabel.I_InputLabelCallbacks;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.common.utils.inputLabel.InputLabelActions;
  import engine.data.settings.multipleChoice.SvMultChoiceQuestionOptionSettings;
  import engine.ui.dialogs.SvDialogIds;
  import flash.events.Event;
  
  public class SvMultChoiceQuestionOptionDialog extends BaseDialog implements I_InputLabelCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_inputQuestion:InputLabel;
    private var mo_isCorrectToggle:ToggleButton;
    private var mo_okButton:Button;
    private var mo_cancelButton:Button;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMultChoiceQuestionOptionDialog(s_inputText:String, b_isCorrect:Boolean,
    b_isCorrectToggleEnabled:Boolean, o_callbacks:IBaseDialogCallbacks = null) {
      super(MultipleChoiceQuestionOptionDialogDisplay, SvDialogIds.MULTIPLE_CHOICE_QUESTION_OPTION, o_callbacks);
      init(s_inputText, b_isCorrect, b_isCorrectToggleEnabled);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // inputLabelOnAction
    //==================================================================
    public function inputLabelOnAction(o_inputLabel:InputLabel, s_inputLabelAction:String):void {
      if (s_inputLabelAction == InputLabelActions.CHANGE ||
      s_inputLabelAction == InputLabelActions.FOCUS_OUT) {
        updateUiEnability();
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_inputQuestion.uninit();
      mo_isCorrectToggle.uninit();
      mo_okButton.uninit();
      mo_cancelButton.uninit();
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(s_inputText:String, b_isCorrect:Boolean, b_isCorrectToggleEnabled:Boolean):void {
      mo_inputQuestion = new InputLabel(this, this, s_inputText, "txf_input_question");
      initButtons(b_isCorrect, b_isCorrectToggleEnabled);
      updateUiEnability();
      initFocus();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons(b_isCorrect:Boolean, b_isCorrectToggleEnabled:Boolean):void {
      mo_isCorrectToggle = new ToggleButton(getSprite("yes_no_toggle_display"));
      mo_isCorrectToggle.isSelected = b_isCorrect;
      mo_isCorrectToggle.enabled = b_isCorrectToggleEnabled;
      
      mo_okButton = initButton("ok_button_display", onOkClick);
      mo_cancelButton = initButton("cancel_button_display", onCancelClick);
    }
    
    //==================================================================
    // initFocus
    //==================================================================
    private function initFocus():void {
      if (display.stage) {
        display.stage.focus = mo_inputQuestion.textField;
      } else {
        display.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
      }
    }
    
    //==================================================================
    // onAddedToStage
    //==================================================================
    private function onAddedToStage(o_event:Event):void {
      removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
      display.stage.focus = mo_inputQuestion.textField;
    }
    
    //==================================================================
    // onCancelClick
    //==================================================================
    private function onCancelClick(b_shift:Boolean):void {
      cancel();
    }
    
    //==================================================================
    // onOkClick
    //==================================================================
    private function onOkClick(b_shift:Boolean):void {
      var o_data:Object = { };
      o_data[SvMultChoiceQuestionOptionSettings.PROP_IS_CORRECT] = mo_isCorrectToggle.isSelected;
      o_data[SvMultChoiceQuestionOptionSettings.PROP_TEXT] = mo_inputQuestion.text;
      var o_settings:SvMultChoiceQuestionOptionSettings = new SvMultChoiceQuestionOptionSettings(o_data);
      complete(o_settings);
    }
    
    //==================================================================
    // updateUiEnability
    //==================================================================
    private function updateUiEnability():void {
      mo_okButton.enabled = mo_inputQuestion.text.length > 0;
    }
  }
}