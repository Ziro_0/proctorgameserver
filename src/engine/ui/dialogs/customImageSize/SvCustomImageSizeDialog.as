/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvCustomImageSizeDialog

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          06-06-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.customImageSize {

  import engine.common.data.CustomThemePath;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogManager;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.common.utils.deviceProfiles.DeviceProfiles;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.message.SvMessageDialog;
  import engine.ui.dialogs.progress.SvProgressDialog;
  import engine.utils.customImageFileCopier.I_SvCustomImageFileCopierCallbacks;
  import engine.utils.customImageFileCopier.SvCustomImageFileCopier;
  import engine.utils.customImageFileCopier.SvCustomImageFileCopyData;
  import engine.utils.customImageFileDeleter.I_SvCustomImageFileDeleterCallbacks;
  import engine.utils.customImageFileDeleter.SvCustomImageFileDeleter;
  import flash.display.SimpleButton;
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.text.TextField;
	
  public class SvCustomImageSizeDialog extends BaseDialog implements IDialogManagerCallbacks,
  I_SvCustomImageFileCopierCallbacks, I_SvCustomImageFileDeleterCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const DIALOG_HEADER:String = "Custom Themes Image Selector"
    
    private static const DIALOG_HEADER_COPYING:String = "Copying Files..."
    private static const DIALOG_HEADER_DELETING:String = "Removing Files..."
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_dialogManager:DialogManager;
    private var mo_customImageFileCopier:SvCustomImageFileCopier;
    private var mo_customImageFileDeleter:SvCustomImageFileDeleter;
    
    private var ms_themeId:String;
    private var ms_themeComponentName:String;
    
    //key: String (device profile id)
    //data: SvCustomImageSizePanel
    private var mo_customImageSizePanels:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvCustomImageSizeDialog(s_themeId:String, s_themeComponentName:String,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(CustomImageSizeDialogDisplay, SvDialogIds.CUSTOM_IMAGE_SIZE, o_callbacks);
			init(s_themeId, s_themeComponentName);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
      trace("SvCustomImageSizeDialog.dialogManagerOnAction", s_dialogId, s_dialogAction);
    }
    
    //==================================================================
    // fileCopierOnComplete
    //==================================================================
    public function fileCopierOnComplete(o_fileCopier:SvCustomImageFileCopier):void {
      if (!mo_customImageFileCopier || mo_customImageFileCopier != o_fileCopier) {
        return; //sanity checks
      }
      
      closeDialog();
      
      if (mo_customImageFileCopier.errorFileUrls) {
        openMessageDialog("Unable to copy one or more files");
      } else {
        notifyComplete(true);
      }
    }
    
    //==================================================================
    // fileDeleterOnComplete
    //==================================================================
    public function fileDeleterOnComplete(o_fileDeletier:SvCustomImageFileDeleter):void {
      if (!mo_customImageFileDeleter || mo_customImageFileDeleter != o_fileDeletier) {
        return; //sanity checks
      }
      
      closeDialog();
      
      if (mo_customImageFileDeleter.errorFileUrls) {
        openMessageDialog("Unable to remove one or more files");
      } else {
        notifyComplete(false);
      }
    }
    
    //==================================================================
    // initCisdButton
    //==================================================================
    public function initCisdButton(s_buttonDisplayName:String, f_callback:Function):Sprite {
      //This function is a kludge to address unknown issue when using File.browseForOpen,
      // buttons using the FButton code eventually stop responding to mouse input.
      var o_buttonDisplay:Sprite = getSprite(s_buttonDisplayName);
      if (!o_buttonDisplay) {
        return(null);
      }
      
      var o_txfLabel:TextField = getTextField("txf_label", null, o_buttonDisplay);
      if (o_txfLabel) {
        o_txfLabel.mouseEnabled = false;
      }
      
      getSimpleButton("btn", f_callback, o_buttonDisplay);
      return(o_buttonDisplay);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      if (mo_dialogManager) {
        mo_dialogManager.uninit();
        mo_dialogManager = null;
      }
      
      if (mo_customImageFileCopier) {
        mo_customImageFileCopier.uninit();
      }
      
      if (mo_customImageFileDeleter) {
        mo_customImageFileDeleter.uninit();
      }
      
      uninitCustomImageSizePanels();
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // openDialog
    //==================================================================
    internal function openDialog(o_dialog:BaseDialog):BaseDialog {
      if (o_dialog) {
        o_dialog.callbacks = dialogManager;
      }
      return(dialogManager.openDialog(o_dialog));
    }
    
    //==================================================================
    // openMessageDialog
    //==================================================================
    internal function openMessageDialog(s_message:String, s_header:String = null):BaseDialog {
      return(openDialog(new SvMessageDialog(s_header || SvCustomImageSizeDialog.DIALOG_HEADER,
        s_message)));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // areAllImagesSelectedOrCleared
    //==================================================================
    private function areAllImagesSelectedOrCleared():Boolean {
      var u_index:uint = 0;
      var b_value:Boolean;
      for each (var o_sizePanel:SvCustomImageSizePanel in mo_customImageSizePanels) {
        if (u_index == 0) {
          b_value = Boolean(o_sizePanel.selectedFileUrl);
        } else {
          if (b_value != Boolean(o_sizePanel.selectedFileUrl)) {
            return(false);
          }
        }
        u_index++;
      }
      
      return(true);
    }
    
    //==================================================================
    // buildTargetCopyFileUrl
    //==================================================================
    private function buildTargetCopyFileUrl(s_deviceProfileId:String):String {
      return(CustomThemePath.Image(ms_themeId, ms_themeComponentName, s_deviceProfileId));
    }
    
    //==================================================================
    // closeDialog
    //==================================================================
    private function closeDialog():void {
      if (mo_dialogManager) {
        mo_dialogManager.closeDialog();
      }
    }
    
    //==================================================================
    // copyCustomImageFiles
    //==================================================================
    private function copyCustomImageFiles():Boolean {
      var ao_fileUrls:Array = getUrlsOfFilesToCopy();
      if (!ao_fileUrls.length) {
        return(false);
      }
      
      if (!mo_customImageFileCopier) {
        mo_customImageFileCopier = new SvCustomImageFileCopier(this);
      }
      
      mo_customImageFileCopier.copyFiles(ao_fileUrls);
      return(true);
    }
    
    //==================================================================
    // deleteCustomImageFiles
    //==================================================================
    private function deleteCustomImageFiles():Boolean {
      var as_fileUrls:Array = getUrlsOfFilesToDelete();
      if (!as_fileUrls.length) {
        return(false);
      }
      
      if (!mo_customImageFileDeleter) {
        mo_customImageFileDeleter = new SvCustomImageFileDeleter(this);
      }
      
      mo_customImageFileDeleter.deleteFiles(as_fileUrls);
      return(true);
    }
    
    //==================================================================
    // dialogManager (get)
    //==================================================================
    private function get dialogManager():DialogManager {
      if (!mo_dialogManager) {
        mo_dialogManager = new DialogManager(display, this);
      }
      
      return(mo_dialogManager);
    }
    
    //==================================================================
    // getUrlsOfFilesToCopy
    //==================================================================
    private function getUrlsOfFilesToCopy():Array {
      //data: SvCustomImageFileCopyData
      var ao_fileUrls:Array = [];
      
      for (var s_deviceProfileId:String in mo_customImageSizePanels) {
        var o_sizePanel:SvCustomImageSizePanel = mo_customImageSizePanels[s_deviceProfileId];
        if (!o_sizePanel.isDirty) {
          continue;
        }
        
        var s_fileUrl:String = o_sizePanel.selectedFileUrl;
        if (s_fileUrl) {
          ao_fileUrls.push(new SvCustomImageFileCopyData(s_fileUrl,
            buildTargetCopyFileUrl(s_deviceProfileId)));
        }
      }
      return(ao_fileUrls);
    }
    
    //==================================================================
    // getUrlsOfFilesToDelete
    //==================================================================
    private function getUrlsOfFilesToDelete():Array {
      var as_files:Array = [];
      for (var s_deviceProfileId:String in mo_customImageSizePanels) {
        var o_sizePanel:SvCustomImageSizePanel = mo_customImageSizePanels[s_deviceProfileId];
        if (o_sizePanel.isDirty && !o_sizePanel.selectedFileUrl) {
          as_files.push(buildTargetCopyFileUrl(s_deviceProfileId));
        }
      }
      return(as_files);
    }
    
    //==================================================================
    // getSelectedFileUrlFromSizePanel
    //==================================================================
    private function getSelectedFileUrlFromSizePanel(s_deviceProfileId:String):String {
      var o_sizePanel:SvCustomImageSizePanel = getSizePanel(s_deviceProfileId);
      return(o_sizePanel ? o_sizePanel.selectedFileUrl : null);
    }
    
    //==================================================================
    // getSizePanel
    //==================================================================
    private function getSizePanel(s_deviceProfileId:String):SvCustomImageSizePanel {
      return(mo_customImageSizePanels[s_deviceProfileId] as SvCustomImageSizePanel);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(s_themeId:String, s_themeComponentName:String):void {
      ms_themeId = s_themeId;
      ms_themeComponentName = s_themeComponentName;
      initCustomImageSizePanels();
      initButtons();
      loadPanelImages();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      initCisdButton("clear_button_display", onClearClick);
      initCisdButton("ok_button_display", onOkClick);
      initCisdButton("cancel_button_display", onCancelClick);
    }
    
    //==================================================================
    // initCustomImageSizePanel
    //==================================================================
    private function initCustomImageSizePanel(s_deviceProfileId:String):void {
      var o_panel:SvCustomImageSizePanel = new SvCustomImageSizePanel(this, s_deviceProfileId, ms_themeId,
        ms_themeComponentName);
      mo_customImageSizePanels[s_deviceProfileId] = o_panel;
    }
    
    //==================================================================
    // initCustomImageSizePanels
    //==================================================================
    private function initCustomImageSizePanels():void {
      mo_customImageSizePanels = { };
      
      const DEVICE_PROFILE_IDS:Array = DeviceProfiles.GetIds();
      for each (var s_deviceProfileId:String in DEVICE_PROFILE_IDS) {
        initCustomImageSizePanel(s_deviceProfileId);
      }
    }
    
    //==================================================================
    // loadPanelImage
    //==================================================================
    private function loadPanelImage(s_deviceProfileId:String):void {
      var o_panel:SvCustomImageSizePanel = getSizePanel(s_deviceProfileId);
      o_panel.loadImage();
    }
    
    //==================================================================
    // loadPanelImages
    //==================================================================
    private function loadPanelImages():void {
      for (var s_deviceProfileId:String in mo_customImageSizePanels) {
        loadPanelImage(s_deviceProfileId);
      }
    }
    
    //==================================================================
    // notifyComplete
    //==================================================================
    private function notifyComplete(b_areImagesSpecified:Boolean):void {
      var o_data:SvCustomImageSizeDialogCompleteData = new SvCustomImageSizeDialogCompleteData();
      o_data.themeComponentName = ms_themeComponentName;
      o_data.areImagesSpecified = b_areImagesSpecified;
      complete(o_data);
    }
    
    //==================================================================
    // onCancelClick
    //==================================================================
    private function onCancelClick(o_event:Event):void {
      complete();
    }
    
    //==================================================================
    // onClearClick
    //==================================================================
    private function onClearClick(o_event:Event):void {
      for each (var o_sizePanel:SvCustomImageSizePanel in mo_customImageSizePanels) {
        o_sizePanel.clearImage();
      }
    }
    
    //==================================================================
    // onOkClick
    //==================================================================
    private function onOkClick(o_event:Event):void {
      if (!areAllImagesSelectedOrCleared()) {
        openMessageDialog("Either an image must be selected for all aspect " +
          "ratios, or all aspect ratios must be cleared of an image.");
        return;
      }
      
      if (copyCustomImageFiles()) {
        openProgressDialog(DIALOG_HEADER_COPYING);
        return;
      }
      
      if (deleteCustomImageFiles()) {
        openProgressDialog(DIALOG_HEADER_DELETING);
        return;
      }
      
      complete();
    }
    
    //==================================================================
    // openProgressDialog
    //==================================================================
    private function openProgressDialog(s_headerText:String):void {
      openDialog(new SvProgressDialog(s_headerText));
    }
    
    //==================================================================
    // uninitCustomImageSizePanels
    //==================================================================
    private function uninitCustomImageSizePanels():void {
      for (var s_deviceProfileId:String in mo_customImageSizePanels) {
        var o_panel:SvCustomImageSizePanel =
          mo_customImageSizePanels[s_deviceProfileId] as SvCustomImageSizePanel;
        o_panel.uninit();
        delete mo_customImageSizePanels[s_deviceProfileId];
      }
    }
  }
}