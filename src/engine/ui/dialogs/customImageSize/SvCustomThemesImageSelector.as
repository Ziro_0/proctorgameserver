/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvCustomThemesImageSelector

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-31-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.customImageSize {
  import engine.common.data.CustomThemePath;
  import engine.common.utils.binaryImagesLoader.BinaryImagesLoader;
  import engine.common.utils.binaryImagesLoader.I_BinaryImagesLoaderCallbacks;
  import flash.display.Loader;
  import flash.events.Event;
  import flash.events.IOErrorEvent;
  import flash.filesystem.File;
  import flash.geom.Point;
  import flash.net.FileFilter;
  import flash.utils.ByteArray;

  internal class SvCustomThemesImageSelector implements I_BinaryImagesLoaderCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const BIANRY_IMAGE_LOADER_KEY:String = "image";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_sizePanel:SvCustomImageSizePanel;
    
    private var mo_binaryImageLoader:BinaryImagesLoader;
    
    private var mo_loadedFileForDimCheck:File;
    private var mo_browseFile:File;
    
    private var mp_requiredImageSize:Point;
    
    private var ms_themeId:String;
    private var ms_selectedThemeComponentName:String;
    private var ms_deviceProfileId:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvCustomThemesImageSelector(o_sizePanel:SvCustomImageSizePanel) {
      super();
      init(o_sizePanel);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // binaryImagesLoaderComplete
    //==================================================================
    public function binaryImagesLoaderComplete(o_loader:BinaryImagesLoader):void {
      if (!o_loader) {
        trace("SvCustomThemesImageSelector.binaryImagesLoaderComplete. Warning: Invalid param.");
        openMessageDialog("Warning: Unable to use selected image (invalid parameter).");
        return;
      }
      
      var o_imageLoader:Loader = o_loader.getLoader(BIANRY_IMAGE_LOADER_KEY);
      if (!o_imageLoader) {
        trace("SvCustomThemesImageSelector.binaryImagesLoaderComplete. Warning: Image not found.");
        openMessageDialog("Warning: Unable to use selected image (image key not found).");
        return;
      }
      
      if (!mp_requiredImageSize) {
        trace("SvCustomThemesImageSelector.binaryImagesLoaderComplete. Warning: " +
          "No required image size specified.");
        openMessageDialog("Warning: Unable to use selected image (no image size specified).");
        return;
      }
      
      if (mp_requiredImageSize.x != o_imageLoader.width || mp_requiredImageSize.y != o_imageLoader.height) {
        trace("SvCustomThemesImageSelector.binaryImagesLoaderComplete. Warning: " +
          "Specified image's dimensions don't match the required dimensions " + mp_requiredImageSize);
        openMessageDialog(
          "Specified image's dimensions don't match the required dimensions (image must be " +
          mp_requiredImageSize.x + " x " + mp_requiredImageSize.y + ").");
        return;
      }
      
      if (!mo_loadedFileForDimCheck) {
        trace("SvCustomThemesImageSelector.binaryImagesLoaderComplete. Warning: No file loaded.");
        openMessageDialog("Warning: No file loaded.");
        return;
      }
      
      var o_file:File = mo_loadedFileForDimCheck.clone();
      uninitLoadedFileForDimCheck();
      
      notifyComplete(o_file);
    }
    
    //==================================================================
    // browserForThemeImageFile
    //==================================================================
    public function browserForThemeImageFile(s_themeId:String, s_themeComponentName:String,
    p_requiredImageSize:Point = null, s_deviceProfileId:String = null):void {
      var ao_typeFilters:Array = [
        new FileFilter("Supported Image Files", "*.png")
      ];
      
      mo_browseFile = new File();
      var o_excError:Error = null;
      
      try {
        mo_browseFile.browseForOpen("Select Image", ao_typeFilters);
      } catch (o_error:Error) {
        trace("SvCustomThemesImageSelector.browserForThemeImageFile. " +
          "Theme component name: " + s_themeComponentName + ", Error: " + o_error);
        o_excError = o_error;
        mo_browseFile = null;
      }
      
      if (o_excError) {
        openMessageDialog("Unable to browse for image file. Error ID: " + o_excError.errorID);
        return;
      }
      
      ms_themeId = s_themeId;
      ms_selectedThemeComponentName = s_themeComponentName;
      mp_requiredImageSize = p_requiredImageSize;
      ms_deviceProfileId = s_deviceProfileId;
      
      mo_browseFile.addEventListener(Event.SELECT, onFileSelect, false, 0, true);
      mo_browseFile.addEventListener(IOErrorEvent.IO_ERROR, onFileIoError, false, 0, true);
      mo_browseFile.addEventListener(Event.CANCEL, onFileCancel, false, 0, true);
    }
    
    //==================================================================
    // removeImage
    //==================================================================
    public function removeImage():void {
      mo_binaryImageLoader.removeImage(BIANRY_IMAGE_LOADER_KEY);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      mo_binaryImageLoader.uninit();
      uninitBrowseFile();
      uninitLoadedFileForDimCheck();
      mo_sizePanel = null;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // buildTargetCopyFileUrl
    //==================================================================
    private function buildTargetCopyFileUrl(o_file:File):String {
      return(CustomThemePath.Image(ms_themeId, ms_selectedThemeComponentName, ms_deviceProfileId));
    }
    
    //==================================================================
    // beginCheckImageDims
    //==================================================================
    private function beginCheckImageDims(o_file:File):Boolean {
      mo_loadedFileForDimCheck = o_file;
      mo_loadedFileForDimCheck.addEventListener(Event.COMPLETE, onFileLoadDimCheckComplete,
        false, 0, true);
      mo_loadedFileForDimCheck.addEventListener(IOErrorEvent.IO_ERROR, onFileLoadDimCheckError,
        false, 0, true);
      
      var o_excError:Error = null;
      try {
        mo_loadedFileForDimCheck.load();
      } catch (o_error:Error) {
        o_excError = o_error;
        uninitLoadedFileForDimCheck();
      }
      
      if (o_excError) {
        trace("SvCustomThemesImageSelector.beginCheckImageDims. Error: " + o_excError);
        openMessageDialog("Unable to load selected image file to check dimensions. Error ID: " +
          o_excError.errorID);
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // onFileLoadDimCheckComplete
    //==================================================================
    private function onFileLoadDimCheckComplete(o_event:Event):void {
      mo_binaryImageLoader.removeImage(BIANRY_IMAGE_LOADER_KEY);
      
      var o_file:File = o_event.currentTarget as File;
      var o_data:ByteArray = o_file.data;
      mo_binaryImageLoader.add(BIANRY_IMAGE_LOADER_KEY, o_file.data);
      mo_binaryImageLoader.load();
      
      removeFilForDimCheckEvents();
    }
    
    //==================================================================
    // onFileLoadDimCheckError
    //==================================================================
    private function onFileLoadDimCheckError(o_event:IOErrorEvent):void {
      trace("SvCustomThemesImageSelector.onFileLoadDimCheckError. Error: " + o_event.text);
      uninitLoadedFileForDimCheck();
      openMessageDialog("Unable to load selected image file: " + o_event.text);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_sizePanel:SvCustomImageSizePanel):void {
      mo_sizePanel = o_sizePanel;
      mo_binaryImageLoader = new BinaryImagesLoader(this);
    }
    
    //==================================================================
    // notifyComplete
    //==================================================================
    private function notifyComplete(o_file:File):void {
      if (!ms_selectedThemeComponentName) {
        return;
      }
      
      mo_sizePanel.customThemeImageSelectorOnSelected(ms_selectedThemeComponentName,
        o_file.url,
        mo_binaryImageLoader.getLoader(BIANRY_IMAGE_LOADER_KEY));
      ms_selectedThemeComponentName = null;
    }
    
    //==================================================================
    // onFileCancel
    //==================================================================
    private function onFileCancel(o_event:Event):void {
      uninitBrowseFile();
    }
    
    //==================================================================
    // onFileIoError
    //==================================================================
    private function onFileIoError(o_event:IOErrorEvent):void {
      ms_selectedThemeComponentName = null;
      uninitBrowseFile();
      openMessageDialog("Unable to browse selected image file: " + o_event.text);
    }
    
    //==================================================================
    // onFileSelect
    //==================================================================
    private function onFileSelect(o_event:Event):void {
      uninitBrowseFile();
      
      if (!mp_requiredImageSize) {
        return;
      }
      
      var o_file:File = o_event.currentTarget as File;
      if (!beginCheckImageDims(o_file.clone())) {
        uninitLoadedFileForDimCheck();
        return;
      }
    }
    
    //==================================================================
    // openMessageDialog
    //==================================================================
    private function openMessageDialog(s_message:String):void {
      if (mo_sizePanel) {
        mo_sizePanel.openMessageDialog(s_message, SvCustomImageSizeDialog.DIALOG_HEADER);
      }
    }
    
    //==================================================================
    // removeFilForDimCheckEvents
    //==================================================================
    private function removeFilForDimCheckEvents():void {
      if (mo_loadedFileForDimCheck) {
        mo_loadedFileForDimCheck.removeEventListener(Event.COMPLETE, onFileLoadDimCheckComplete);
        mo_loadedFileForDimCheck.removeEventListener(IOErrorEvent.IO_ERROR, onFileLoadDimCheckError);
      }
    }
    
    //==================================================================
    // uninitBrowseFile
    //==================================================================
    private function uninitBrowseFile():void {
      if (!mo_browseFile) {
        return;
      }
      
      mo_browseFile.removeEventListener(Event.SELECT, onFileSelect);
      mo_browseFile.removeEventListener(IOErrorEvent.IO_ERROR, onFileIoError);
      mo_browseFile.removeEventListener(Event.CANCEL, onFileCancel);
      mo_browseFile.cancel();
      mo_browseFile = null;
    }
    
    //==================================================================
    // uninitLoadedFileForDimCheck
    //==================================================================
    private function uninitLoadedFileForDimCheck():void {
      if (!mo_loadedFileForDimCheck) {
        return;
      }
      
      mo_loadedFileForDimCheck.cancel();
      mo_loadedFileForDimCheck.removeEventListener(Event.COMPLETE, onFileLoadDimCheckComplete);
      mo_loadedFileForDimCheck.removeEventListener(IOErrorEvent.IO_ERROR, onFileLoadDimCheckError);
      mo_loadedFileForDimCheck = null;
    }
  }
}