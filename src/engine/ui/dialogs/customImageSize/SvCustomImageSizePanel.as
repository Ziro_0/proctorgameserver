/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvCustomImageSizePanel

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          06-06-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.customImageSize {

  import com.greensock.events.LoaderEvent;
  import com.greensock.loading.ImageLoader;
  import engine.common.data.CustomThemePath;
  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.utils.ThemeImagePath;
  import engine.common.utils.deviceProfiles.DeviceProfile;
  import engine.common.utils.deviceProfiles.DeviceProfiles;
  import engine.ui.dialogs.message.SvMessageDialog;
  import flash.display.DisplayObject;
  import flash.display.Loader;
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.filesystem.File;
  import flash.geom.Point;
	
  internal class SvCustomImageSizePanel {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_sizeDialog:SvCustomImageSizeDialog;
    private var mo_imageSelector:SvCustomThemesImageSelector;
    
    private var mo_imageLoader:ImageLoader;
    
    private var mo_imageContainer:Sprite;
    
    private var mp_imageSize:Point;
    
    private var ms_deviceProfileId:String;
    private var ms_themeId:String;
    private var ms_themeComponentName:String;
    private var ms_selectedFileUrl:String;
    private var mb_isDirty:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvCustomImageSizePanel(o_sizeDialog:SvCustomImageSizeDialog,
    s_deviceProfileId:String, s_themeId:String, s_themeComponentName:String) {
			init(o_sizeDialog, s_deviceProfileId, s_themeId, s_themeComponentName);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // clearImage
    //==================================================================
    public function clearImage():void {
      mb_isDirty = true;
      ms_selectedFileUrl = null;
      removeImage(false);
    }
    
    //==================================================================
    // isDirty (get)
    //==================================================================
    public function get isDirty():Boolean {
      return(mb_isDirty);
    }
    
    //==================================================================
    // loadImage
    //==================================================================
    public function loadImage():void {
      uninitImageLoader();
      
      var o_path:ThemeImagePath = new ThemeImagePath(CustomThemePath.CUSTOM_BASE_PATH,
        ms_deviceProfileId);
      o_path.isApplicationStorageMode = true;
      
      var s_fileUrl:String = o_path.createNonGameFileUrl(ms_themeId, ms_themeComponentName);
      var o_file:File = new File(s_fileUrl);
      if (!o_file.exists) {
        return;
      }
      
      var o_vars:Object = {
        container: mo_imageContainer,
        width: mp_imageSize.x,
        height: mp_imageSize.y,
        scaleMode: "proportionalInside",
        onComplete: onLoadImageLoaderComplete,
        onFail: onLoadImageLoaderFail,
        fileUrl: s_fileUrl
      };
      
      mo_imageLoader = new ImageLoader(s_fileUrl, o_vars);
      mo_imageLoader.load();
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      removeImage(false);
      
      mo_imageSelector.uninit();
      mo_sizeDialog = null;
      mo_imageContainer = null;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // internal methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // customThemeImageSelectorOnSelected
    //==================================================================
    internal function customThemeImageSelectorOnSelected(s_themeComponentName:String,
    s_fileUrl:String, o_contentLoader:Loader):void {
      mb_isDirty = true;
      ms_selectedFileUrl = s_fileUrl;
      addImage(o_contentLoader);
    }
    
    //==================================================================
    // openMessageDialog
    //==================================================================
    internal function openMessageDialog(s_message:String, s_header:String = null):BaseDialog {
      return(mo_sizeDialog.openMessageDialog(s_message, s_header));
    }
    
    //==================================================================
    // selectedFileUrl (get)
    //==================================================================
    internal function get selectedFileUrl():String {
      return(ms_selectedFileUrl);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addImage
    //==================================================================
    private function addImage(o_image:DisplayObject):void {
      o_image.width = mp_imageSize.x;
      o_image.height = mp_imageSize.y;
      mo_imageContainer.addChild(o_image);
    }
    
    //==================================================================
    // deleteImageFile
    //==================================================================
    private function deleteImageFile():void {
      var s_path:String = CustomThemePath.Image(ms_themeId, ms_themeComponentName, ms_deviceProfileId);
      var o_file:File = File.applicationStorageDirectory.resolvePath(s_path);
      if (!o_file.exists) {
        return;
      }
      
      try {
        o_file.deleteFile();
      } catch (o_error:Error) {
        trace("SvCustomImageSizePanel.deleteImageFile. Error: " + o_error);
      }
    }
    
    //==================================================================
    // getDeviceProfileSize
    //==================================================================
    private function getDeviceProfileSize():Point {
      var o_profile:DeviceProfile = DeviceProfiles.GetProfileById(ms_deviceProfileId);
      return(new Point(
        o_profile.assetsWidth * o_profile.templateScale,
        o_profile.assetsHeight * o_profile.templateScale));
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_sizeDialog:SvCustomImageSizeDialog, s_deviceProfileId:String,
    s_themeId:String, s_themeComponentName:String):void {
      mo_sizeDialog = o_sizeDialog;
      ms_deviceProfileId = s_deviceProfileId;
      ms_themeId = s_themeId;
      ms_themeComponentName = s_themeComponentName;
      
      mo_imageSelector = new SvCustomThemesImageSelector(this);
      
      initImageSize();
      initImageContainer();
      initSelectButton();
    }
    
    //==================================================================
    // initImageContainer
    //==================================================================
    private function initImageContainer():void {
      const NAME_PREFIX:String = "image_node_";
      mo_imageContainer = mo_sizeDialog.getSprite(NAME_PREFIX + ms_deviceProfileId);
      mo_imageContainer.removeChildren();
    }
    
    //==================================================================
    // initImageSize
    //==================================================================
    private function initImageSize():void {
      const NAME_PREFIX:String = "custom_image_frame_";
      var o_imageFrame:Sprite = mo_sizeDialog.getSprite(NAME_PREFIX + ms_deviceProfileId);
      mp_imageSize = new Point(o_imageFrame.width, o_imageFrame.height);
    }
    
    //==================================================================
    // initSelectButton
    //==================================================================
    private function initSelectButton():void {
      mo_sizeDialog.initCisdButton("select_button_" + ms_deviceProfileId + "_display",
        onSelectButtonClick);
    }
    
    //==================================================================
    // onLoadImageLoaderComplete
    //==================================================================
    private function onLoadImageLoaderComplete(o_event:LoaderEvent):void {
      var o_loader:ImageLoader = o_event.currentTarget as ImageLoader;
      ms_selectedFileUrl = o_loader.vars.fileUrl;
    }
    
    //==================================================================
    // onLoadImageLoaderFail
    //==================================================================
    private function onLoadImageLoaderFail(o_event:LoaderEvent):void {
      trace("SvCustomImageSizePanel.onLoadImageLoaderFail. Warning: " + o_event.text);
      openMessageDialog("Unable to load an image file: " + o_event.text);
    }
    
    //==================================================================
    // onSelectButtonClick
    //==================================================================
    //private function onSelectButtonClick(o_button:Button, o_event:Event):void {
    private function onSelectButtonClick(o_event:Event):void {
      var p_imageSize:Point = getDeviceProfileSize();
      mo_imageSelector.browserForThemeImageFile(ms_themeId, ms_themeComponentName, p_imageSize,
        ms_deviceProfileId);
    }
    
    //==================================================================
    // removeImage
    //==================================================================
    private function removeImage(b_deleteFile:Boolean):void {
      ms_selectedFileUrl = null;
      
      uninitImageLoader();
      removeImageDisplay();
      
      if (b_deleteFile) {
        deleteImageFile();
      }
    }
    
    //==================================================================
    // removeImageDisplay
    //==================================================================
    private function removeImageDisplay():void {
      mo_imageSelector.removeImage();
      mo_imageContainer.removeChildren();
    }
    
    //==================================================================
    // uninitImageLoader
    //==================================================================
    private function uninitImageLoader():void {
      if (mo_imageLoader) {
        mo_imageLoader.dispose(true);
        mo_imageLoader = null;
      }
    }
  }
}