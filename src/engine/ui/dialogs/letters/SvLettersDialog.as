/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvLettersDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-12-2018
  ActionScript:  3.0
  Description:  
  History:
    07-12-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.letters {
  import engine.I_SvEngine;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.data.settings.SvMiniGameBaseEntrySettings;
  import engine.data.settings.letters.SvLettersEntrySettings;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.game.SvGameBaseDialog;
  import engine.ui.dialogs.lettersEntry.SvLettersEntryDialog;
  import engine.ui.dialogs.lettersEntry.SvLettersEntryDialogCompleteData;
  import engine.ui.dialogs.simpleEntry.SvSimpleEntryDialog;
	
  public class SvLettersDialog extends SvGameBaseDialog implements IBaseDialogCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const WORD_SIMPLE_ENTRY_DIALOG_HEADER:String = "New Word";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvLettersDialog(o_spriteOrSpriteClass:Object, o_engine:I_SvEngine,
    s_gameId:String, s_dialogId:String, o_settingsData:Object,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(o_spriteOrSpriteClass, o_engine, s_gameId, s_dialogId, o_settingsData, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // baseDialogOnAction
    //==================================================================
    public function baseDialogOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
      if (s_dialogId == SvDialogIds.SIMPLE_ENTRY) {
        handleDialogActionWordSimpleEntry(o_data as String);
      }
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    override public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String,
    o_data:Object):void {
      super.dialogManagerOnAction(s_dialogId, s_dialogAction, o_data);
      
      if (s_dialogAction != DialogActions.COMPLETE) {
        return;
      }
      
      if (s_dialogId == SvDialogIds.LETTERS_ENTRY) {
        handleDialogActionLettersEntry(o_data as SvLettersEntryDialogCompleteData);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // initContentDataGrid
    //==================================================================
    override protected function initContentDataGrid():void {
      mo_contentDataGrid = new SvLettersDataGridView(getSprite("dg_content_display"), this);
      super.initContentDataGrid();
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    override protected function onAddClick(b_shift:Boolean):void {
      super.onAddClick(b_shift);
      openWordSimpleEntryDialog();
    }
    
    //==================================================================
    // onCreateBaseEntrySettings
    //==================================================================
    override protected function onCreateBaseEntrySettings(o_entryData:Object):SvMiniGameBaseEntrySettings {
      return(new SvLettersEntrySettings(o_entryData));
    }
    
    //==================================================================
    // onEditSelectedEntry
    //==================================================================
    override protected function onEditSelectedEntry(o_data:Object):void {
      var o_entrySettings:SvLettersEntrySettings = new SvLettersEntrySettings(o_data);
      mo_dialogManager.openDialog(new SvLettersEntryDialog(o_entrySettings, mo_dialogManager));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createNewEmptyLetterEntries
    //==================================================================
    private function createNewEmptyLetterEntries(s_word:String):Array {
      var ao_entries:Array = [];
      for (var i_index:int = 0; i_index < s_word.length; i_index++) {
        ao_entries.push([]);
      }
      
      return(ao_entries);
    }
    
    //==================================================================
    // createNewEmptySettings
    //==================================================================
    private function createNewEmptySettings(s_word:String):SvLettersEntrySettings {
      var o_data:Object = { };
      o_data[SvLettersEntrySettings.PROP_WORD] = s_word;
      o_data[SvLettersEntrySettings.PROP_ENTRIES] = createNewEmptyLetterEntries(s_word);
      return(new SvLettersEntrySettings(o_data));
    }
    
    //==================================================================
    // handleDialogActionLettersEntry
    //==================================================================
    private function handleDialogActionLettersEntry(o_completeData:SvLettersEntryDialogCompleteData):void {
      var o_entrySettings:SvLettersEntrySettings = new SvLettersEntrySettings(o_completeData);
      addOrReplaceEntry(o_entrySettings);
    }
    
    //==================================================================
    // handleDialogActionWordSimpleEntry
    //==================================================================
    private function handleDialogActionWordSimpleEntry(s_word:String):void {
      if (!s_word) {
        mo_engine.traceAndLog("SvLettersDialog.handleDialogActionWordSimpleEntry. Warning: " +
          "No word to add.");
        return;
      }
      
      var o_entrySettings:SvLettersEntrySettings = createNewEmptySettings(s_word);
      mo_dialogManager.openDialog(new SvLettersEntryDialog(o_entrySettings, mo_dialogManager));
    }
    
    //==================================================================
    // openWordSimpleEntryDialog
    //==================================================================
    private function openWordSimpleEntryDialog():void {
      const DEFAULT_TEXT:String = "";
      mo_dialogManager.openDialog(new SvSimpleEntryDialog(DEFAULT_TEXT, this,
        WORD_SIMPLE_ENTRY_DIALOG_HEADER));
    }
  }
}