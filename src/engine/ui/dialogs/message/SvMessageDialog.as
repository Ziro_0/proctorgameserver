/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMessageDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-01-2019
  History:
    07-01-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.message {
  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.ui.dialogs.SvDialogIds;
	
  public class SvMessageDialog extends BaseDialog {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_closeButton:Button;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMessageDialog(s_headerText:String, s_messageText:String,
    o_callbacks:IBaseDialogCallbacks = null, s_dialogId:String = null) {
      super(MessageDialogDisplay, s_dialogId || SvDialogIds.MESSAGE, o_callbacks);
			init(s_headerText, s_messageText);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(s_headerText:String, s_messageText:String):void {
      initButtons();
      initHeaderText(s_headerText);
      initMessageTest(s_messageText);
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_closeButton = initButton("close_button_display", onCloseClicked);
    }
    
    //==================================================================
    // initHeaderText
    //==================================================================
    private function initHeaderText(s_headerText:String):void {
      getTextField("txf_header", s_headerText);
    }
    
    //==================================================================
    // initMessageTest
    //==================================================================
    private function initMessageTest(s_messageText:String):void {
      getTextField("txf_message", s_messageText || "");
    }
    
    //==================================================================
    // onCloseClicked
    //==================================================================
    private function onCloseClicked(b_shift:Boolean):void {
      complete();
    }
  }
}