/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGameBaseDialog

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          09-10-2018
  ActionScript:  3.0
  Description:  
  History:
    09-10-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.game {
  import engine.I_SvEngine;
  import engine.common.data.GameModels;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.DialogManager;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.common.utils.inputLabel.I_InputLabelCallbacks;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.common.utils.inputLabel.InputLabelActions;
  import engine.data.settings.SvMiniGameBaseEntrySettings;
  import engine.data.settings.SvMiniGameBaseSettings;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.dataGrid.SvGameEntryUserDataItem;
  import engine.ui.dataGrid.SvGameEntryUsersDataGridColumnsData;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.gameIdPanel.SvGameIdPanel;
  import engine.ui.usersDataGrid.SvUsersDataGridView;
  import fl.controls.ComboBox;
  import flash.events.Event;
  import flash.text.TextField;
  import flib.utils.FArrayUtils;
  import flib.utils.FDataBlock;
	
  public class SvGameBaseDialog extends BaseDialog implements I_SvDataGridViewCallbacks,
  IDialogManagerCallbacks, I_InputLabelCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const GAME_TEXT_ALLOWED_CHARS:String = "1234567890";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    protected var mo_contentDataGrid:SvGameBaseDataGridView;
    protected var mo_playersDataGrid:SvUsersDataGridView;
    protected var mo_engine:I_SvEngine;
    protected var mo_settings:SvMiniGameBaseSettings;
    protected var mo_dialogManager:DialogManager;
    
    private var mo_gameIdPanel:SvGameIdPanel;
    
    private var mo_cbxGameModel:ComboBox;
    
    private var mo_closeButton:Button;
    private var mo_addButton:Button;
    private var mo_editButton:Button;
    private var mo_removeButton:Button;
    
    private var mo_gameTimeInput:InputLabel;
    
    private var mo_txfCurrentGameTimeLabel:TextField;
    private var mo_txfCurrentGameTime:TextField;
    
    private var ms_gameId:String;
    private var ms_initialTimeText:String;
    private var mi_existingEntryBeingEditedIndex:int;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGameBaseDialog(o_spriteOrSpriteClass:Object, o_engine:I_SvEngine,
    s_gameId:String, s_dialogId:String, o_settingsData:Object,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(o_spriteOrSpriteClass, s_dialogId, o_callbacks);
      init(o_engine, s_gameId, o_settingsData);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addUser
    //==================================================================
    public function addUser(o_newPlayerData:FDataBlock):void {
      if (mo_playersDataGrid && o_newPlayerData) {
        mo_playersDataGrid.addUser(o_newPlayerData);
      }
    }
    
    //==================================================================
    // currentGameTime (set)
    //==================================================================
    public function set currentGameTime(u_value:uint):void {
      mo_txfCurrentGameTime.text = u_value.toString();
    }
    
    //==================================================================
    // dataGridViewOnChange
    //==================================================================
    public function dataGridViewOnChange(o_dataGridView:I_SvDataGridView):void {
      updatUiState();
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction != DialogActions.COMPLETE) {
        return;
      }
      
      if (s_dialogId == SvDialogIds.SIMPLE_ENTRY) {
        handleDialogActionSimpleEntry(o_data);
      }
    }
    
    //==================================================================
    // inputLabelOnAction
    //==================================================================
    public function inputLabelOnAction(o_inputLabel:InputLabel, s_inputLabelAction:String):void {
      if (o_inputLabel == mo_gameTimeInput) {
        if (s_inputLabelAction == InputLabelActions.FOCUS_IN) {
          ms_initialTimeText = mo_gameTimeInput.text;
        } else if (s_inputLabelAction == InputLabelActions.FOCUS_OUT) {
          if (ms_initialTimeText != mo_gameTimeInput.text) {
            mo_settings.time = parseInt(mo_gameTimeInput.text);
            mo_engine.writeSettings();
          }
        }
      }
    }
    
    //==================================================================
    // removeAllUsers
    //==================================================================
    public function removeAllUsers():void {
      if (mo_playersDataGrid) {
        mo_playersDataGrid.removeAllUsers();
      }
    }
    
    //==================================================================
    // removeUser
    //==================================================================
    public function removeUser(o_playerDataOrClientId:Object):void {
      if (!mo_playersDataGrid) {
        return;
      }
      
      var s_clientId:String;
      if (o_playerDataOrClientId is FDataBlock) {
        var o_playerData:FDataBlock = o_playerDataOrClientId as FDataBlock;
        var o_userData:UserData = o_playerData.getObject(PlayerStateKeys.USER_DATA) as UserData;
        s_clientId = o_userData.id;
      } else if (o_playerDataOrClientId is String) {
        s_clientId = o_playerDataOrClientId as String;
      }
      
      mo_playersDataGrid.removeUser(s_clientId);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      uninitButtons();
      
      if (mo_contentDataGrid) {
        mo_contentDataGrid.uninit();
      }
      
      if (mo_playersDataGrid) {
        mo_playersDataGrid.uninit();
      }
      
      if (mo_dialogManager) {
        mo_dialogManager.uninit();
      }
      
      if (mo_cbxGameModel) {
        mo_cbxGameModel.removeEventListener(Event.CHANGE, onCbxGameModelChange);
      }
      
      mo_gameTimeInput.uninit();
      
      mo_settings = null;
      mo_engine = null;
      
      super.uninit();
    }
    
    //==================================================================
    // updateUser
    //==================================================================
    public function updateUser(o_playerData:FDataBlock):void {
      if (mo_playersDataGrid) {
        mo_playersDataGrid.updateUser(o_playerData);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addEntry
    //==================================================================
    protected function addEntry(o_settingsEntry:SvMiniGameBaseEntrySettings):void {
      if (!mo_engine || !mo_settings || !o_settingsEntry) {
        return;
      }
      
      mo_settings.entries.push(o_settingsEntry.data);
      mo_contentDataGrid.addEntry(o_settingsEntry);
      mo_engine.writeSettings();
    }
    
    //==================================================================
    // addExistingEntries
    //==================================================================
    protected function addExistingEntries():void {
      if (!mo_contentDataGrid || !mo_settings) {
        return;
      }
      
      mo_contentDataGrid.removeAll();
      
      for each (var o_entryData:Object in mo_settings.entries) {
        var o_entrySettings:SvMiniGameBaseEntrySettings = onCreateBaseEntrySettings(o_entryData);
        if (o_entrySettings) {
          mo_contentDataGrid.addEntry(o_entrySettings);
        }
      }
    }
    
    //==================================================================
    // addOrReplaceEntry
    //==================================================================
    protected function addOrReplaceEntry(o_entrySettings:SvMiniGameBaseEntrySettings):void {
      if (mi_existingEntryBeingEditedIndex > -1) {
        replaceEntryAt(o_entrySettings, mi_existingEntryBeingEditedIndex);
      } else {
        addEntry(o_entrySettings);
      }
    }
    
    //==================================================================
    // handleDialogActionSimpleEntry
    //==================================================================
    protected function handleDialogActionSimpleEntry(o_data:Object):void {
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    protected function initButtons():void {
      mo_addButton = initButton("add_button_display", onAddClick);
      mo_removeButton = initButton("remove_button_display", onRemoveClick);
      mo_closeButton = initButton("close_button_display", onCloseClick);
      mo_editButton = initButton("edit_button_display", onEditClick);
      
      initGameModelComboBox();
    }
    
    //==================================================================
    // initGameModelComboBox
    //==================================================================
    private function initGameModelComboBox():void {
      mo_cbxGameModel = getSprite("cbx_game_model") as ComboBox;
      
      if (!mo_cbxGameModel) {
        return;
      }
      
      mo_cbxGameModel.addEventListener(Event.CHANGE, onCbxGameModelChange, false, 0, true);
      addModelsToComboBox();
      selectModel(mo_settings.model);
    }
    
    //==================================================================
    // onCbxGameModelChange
    //==================================================================
    private function onCbxGameModelChange(o_event:Event):void {
      mo_settings.model = mo_cbxGameModel.selectedItem.data;
      mo_engine.writeSettings();
    }
    
    //==================================================================
    // onCreateBaseEntrySettings
    //==================================================================
    protected function onCreateBaseEntrySettings(o_entryData:Object):SvMiniGameBaseEntrySettings {
      trace("SvGameBaseDialog.onCreateBaseEntrySettings. Warning: PLease override this method to return " +
        "a data object that extends SvMiniGameBaseEntrySettings.");
      return(null);
    }
    
    //==================================================================
    // uninitButtons
    //==================================================================
    protected function uninitButtons():void {
      mo_addButton = uninitButton(mo_addButton);
      mo_removeButton = uninitButton(mo_removeButton);
      mo_closeButton = uninitButton(mo_closeButton);
      mo_editButton = uninitButton(mo_editButton);
    }
    
    //==================================================================
    // initContentDataGrid
    //==================================================================
    protected function initContentDataGrid():void {
      addExistingEntries();
    }
    
    //==================================================================
    // initPlayersDataGrid
    //==================================================================
    protected function initPlayersDataGrid():void {
      mo_playersDataGrid = new SvUsersDataGridView(
        getSprite("dg_players_display"),
        mo_engine.state,
        SvGameEntryUsersDataGridColumnsData.HEADER_DATA,
        SvGameEntryUserDataItem);
    }
    
    //==================================================================
    // initText
    //==================================================================
    protected function initText():void {
      initGameTimeInput();
      initCurrentGameTimeTexts();
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    protected function onAddClick(b_shift:Boolean):void {
      mi_existingEntryBeingEditedIndex = -1;
    }
    
    //==================================================================
    // onCloseClick
    //==================================================================
    protected function onCloseClick(b_shift:Boolean):void {
      complete();
    }
    
    //==================================================================
    // onEditSelectedEntry
    //==================================================================
    protected function onEditSelectedEntry(o_data:Object):void {
    }
    
    //==================================================================
    // onRemoveClick
    //==================================================================
    protected function onRemoveClick(b_shift:Boolean):void {
      if (!mo_contentDataGrid) {
        mo_engine.traceAndLog("SvGameBaseDialog.onRemoveClick. Warning: No content data grid defined.");
        return;
      }
      
      var o_item:SvGameBaseDataGridItem = mo_contentDataGrid.selectedItem;
      var i_index:int = mo_contentDataGrid.getIndexOf(o_item);
      if (i_index == -1) {
        mo_engine.traceAndLog("SvGameBaseDialog.onRemoveClick. Warning: Unable to remove item.");
        return; //sanity check
      }
      
      mo_contentDataGrid.removeItem(o_item);
      FArrayUtils.RemoveAt(mo_settings.entries, i_index);
      mo_engine.writeSettings();
    }
    
    //==================================================================
    // replaceEntryAt
    //==================================================================
    protected function replaceEntryAt(o_entrySettings:SvMiniGameBaseEntrySettings, i_index:int):void {
      if (!mo_engine || !mo_settings || !mo_contentDataGrid || !o_entrySettings) {
        return;
      }
      
      mo_contentDataGrid.replaceEntryAt(o_entrySettings, i_index);
      mo_settings.entries[i_index] = o_entrySettings.data;
      mo_engine.writeSettings();
    }
    
    //==================================================================
    // selectModel
    //==================================================================
    private function selectModel(s_model:String):void {
      if (!mo_cbxGameModel) {
        return;
      }
      
      for (var i_index:int = 0; i_index < mo_cbxGameModel.length; i_index++) {
        var o_item:Object = mo_cbxGameModel.getItemAt(i_index);
        if (String(o_item.data) == mo_settings.model) {
          mo_cbxGameModel.selectedIndex = i_index;
          break;
        }
      }
    }
    
    //==================================================================
    // updatUiState
    //==================================================================
    protected function updatUiState():void {
      var b_isGameStarted:Boolean = mo_engine.isGameStarted;
      
      //disabled when a game is active
      mo_addButton.enabled = !b_isGameStarted;
      mo_removeButton.enabled = !b_isGameStarted && isContentItemSelected();
      mo_gameTimeInput.textField.mouseEnabled = !b_isGameStarted;
      
      if (mo_editButton) {
        mo_editButton.enabled = !b_isGameStarted && isContentItemSelected();
      }
      
      enableContentDataGrid(!b_isGameStarted);
      
      //enabled/visible when a game is active
      mo_txfCurrentGameTime.visible = b_isGameStarted;
      mo_txfCurrentGameTimeLabel.visible = b_isGameStarted;
      
      if (mo_playersDataGrid) {
        mo_playersDataGrid.visible = b_isGameStarted;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addModelsToComboBox
    //==================================================================
    private function addModelsToComboBox():void {
      for each (var s_gameModel:String in GameModels.ALL) {
        var o_item:Object = {
          label: s_gameModel,
          data: s_gameModel
        };
        mo_cbxGameModel.addItem(o_item);
      }
    }
    
    //==================================================================
    // enableContentDataGrid
    //==================================================================
    private function enableContentDataGrid(b_enabled:Boolean):void {
      if (mo_contentDataGrid) {
        mo_contentDataGrid.enabled = b_enabled;
      }
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_SvEngine, s_gameId:String, o_settingsData:Object):void {
      mo_engine = o_engine;
      ms_gameId = s_gameId;
      
      initSettings(o_settingsData);
      
      mo_dialogManager = new DialogManager(display, this);
      mo_gameIdPanel = new SvGameIdPanel(getSprite("game_id_display"), ms_gameId);
      
      initContentDataGrid();
      initPlayersDataGrid();
      initButtons();
      initText();
      
      updatUiState();
    }
    
    //==================================================================
    // initCurrentGameTimeTexts
    //==================================================================
    private function initCurrentGameTimeTexts():void {
      mo_txfCurrentGameTime = getTextField("txf_current_game_time");
      mo_txfCurrentGameTime.mouseEnabled = false;
      
      mo_txfCurrentGameTimeLabel = getTextField("txf_current_game_time_label");
      mo_txfCurrentGameTimeLabel.mouseEnabled = false;
    }
    
    //==================================================================
    // initGameTimeInput
    //==================================================================
    private function initGameTimeInput():void {
      mo_gameTimeInput = new InputLabel(this, this, mo_settings.time.toString(), "txf_game_time");
      mo_gameTimeInput.textField.restrict = GAME_TEXT_ALLOWED_CHARS;
    }
    
    //==================================================================
    // initSettings
    //==================================================================
    private function initSettings(o_settingsData:Object):void {
      mo_settings = new SvMiniGameBaseSettings(o_settingsData);
      if (mo_settings.newDataInitialized) {
        mo_engine.traceAndLog("SvGameBaseDialog.initSettings. Reset data.");
        
        if (!mo_engine.settingsData.games) {
          mo_engine.settingsData.games = { };
        }
        
        mo_engine.settingsData.games[ms_gameId] = mo_settings.block.data;
        mo_engine.writeSettings();
      }
    }
    
    //==================================================================
    // isContentItemSelected
    //==================================================================
    private function isContentItemSelected():Boolean {
      return(mo_contentDataGrid && mo_contentDataGrid.isItemSelected);
    }
    
    //==================================================================
    // onEditClick
    //==================================================================
    private function onEditClick(b_shift:Boolean):void {
      if (!mo_contentDataGrid) {
        return;
      }
      
      var i_index:int = mo_contentDataGrid.getSelectedIndexOf();
      var o_data:Object = mo_settings.entries[i_index];
      if (o_data == null) {
        return;
      }
      
      mi_existingEntryBeingEditedIndex = i_index;
      
      onEditSelectedEntry(o_data);
    }
  }
}