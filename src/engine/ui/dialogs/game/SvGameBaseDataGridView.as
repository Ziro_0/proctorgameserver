/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGameBaseDataGridView

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          09-11-2018
  ActionScript:  3.0
  Description:  
  History:
    09-11-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.game {
  import engine.common.baseView.BaseView;
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import fl.controls.DataGrid;
  import fl.controls.dataGridClasses.DataGridColumn;
  import flash.events.Event;
	
  public class SvGameBaseDataGridView extends BaseView implements I_SvDataGridView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_dataGrid:DataGrid;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGameBaseDataGridView(o_spriteOrSpriteClass:Object,
    vo_dataGridColumnsData:Vector.<SvGameBaseDataGridColumnData>, o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, o_callbacks);
      _init(vo_dataGridColumnsData);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addEntry
    //==================================================================
    public function addEntry(o_entryData:Object):Boolean {
      var o_item:SvGameBaseDataGridItem = onCreateDataGridItem(o_entryData);
      if (!o_item) {
        return(false);
      }
      mo_dataGrid.addItem(o_item);
      mo_dataGrid.invalidate();
      return(true);
    }
    
    //==================================================================
    // getIndexOf
    //==================================================================
    public function getIndexOf(o_item:SvGameBaseDataGridItem):int {
      if (!mo_dataGrid || !o_item) {
        return( -1);
      }
      
      for (var i_index:int = 0; i_index < mo_dataGrid.length; i_index++) {
        var o_existingItem:SvGameBaseDataGridItem = mo_dataGrid.getItemAt(i_index) as SvGameBaseDataGridItem;
        if (o_item.equals(o_existingItem)) {
          return(i_index);
        }
      }
      
      return( -1);
    }
    
    //==================================================================
    // getSelectedIndexOf
    //==================================================================
    public function getSelectedIndexOf():int {
      return(getIndexOf(selectedItem));
    }
    
    //==================================================================
    // isItemSelected (get)
    //==================================================================
    public function get isItemSelected():Boolean {
      return(selectedItem != null);
    }
    
    //==================================================================
    // removeAll
    //==================================================================
    public function removeAll():void {
      mo_dataGrid.removeAll();
    }
    
    //==================================================================
    // removeItem
    //==================================================================
    public function removeItem(o_item:SvGameBaseDataGridItem):void {
      mo_dataGrid.removeItem(o_item);
    }
    
    //==================================================================
    // replaceEntryAt
    //==================================================================
    public function replaceEntryAt(o_entryData:Object, u_index:uint):Boolean {
      var o_item:SvGameBaseDataGridItem = onCreateDataGridItem(o_entryData);
      if (!o_item) {
        return(false);
      }
      
      try {
        mo_dataGrid.replaceItemAt(o_item, u_index);
      } catch (o_error:Error) {
        trace("SvGameBaseDataGridView.replaceEntryAt. " + o_error.message);
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // selectedItem (get)
    //==================================================================
    public function get selectedItem():SvGameBaseDataGridItem {
      return(mo_dataGrid.selectedItem as SvGameBaseDataGridItem);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onCreateDataGridItem
    //==================================================================
    protected function onCreateDataGridItem(o_entryData:Object):SvGameBaseDataGridItem {
      trace("SvGameBaseDataGridView.onCreateDataGridItem. Warning: Please override this method to return " +
        "a valid SvGameBaseDataGridItem object, or an object that extends the SvGameBaseDataGridItem class.");
      return(null);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // _init
    //==================================================================
    private function _init(vo_dataGridColumnsData:Vector.<SvGameBaseDataGridColumnData>):void {
      initDataGrid(vo_dataGridColumnsData);
    }
    
    //==================================================================
    // initDataGrid
    //==================================================================
    private function initDataGrid(vo_dataGridColumnsData:Vector.<SvGameBaseDataGridColumnData>):void {
      mo_dataGrid = display as DataGrid;
      if (!mo_dataGrid) {
        trace("SvGameBaseDataGridView.initDataGrid, Warning: The display must be a DataGrid object.");
        return;
      }
      
      mo_dataGrid.addEventListener(Event.CHANGE, onDataGridChange, false, 0, true);
      initColumnHeaders(vo_dataGridColumnsData);
    }
    
    //==================================================================
    // initColumnHeaders
    //==================================================================
    private function initColumnHeaders(vo_dataGridColumnsData:Vector.<SvGameBaseDataGridColumnData>):void {
      for each (var o_cellData:SvGameBaseDataGridColumnData in vo_dataGridColumnsData) {
        var o_column:DataGridColumn = new DataGridColumn();
        if (o_cellData.headerText) {
          o_column.headerText = o_cellData.headerText;
        } else {
          o_column.visible = false;
        }
        
        o_column.dataField = o_cellData.dataField;
        mo_dataGrid.addColumn(o_column);
      }
    }
    
    //==================================================================
    // onDataGridChange
    //==================================================================
    private function onDataGridChange(o_event:Event):void {
      var o_callbacks:I_SvDataGridViewCallbacks = callbacks as I_SvDataGridViewCallbacks;
      if (o_callbacks) {
        o_callbacks.dataGridViewOnChange(this);
      }
    }
  }
}