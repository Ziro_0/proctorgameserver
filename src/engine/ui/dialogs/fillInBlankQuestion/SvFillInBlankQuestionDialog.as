/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvFillInBlankQuestionDialog

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          07-24-2018
  ActionScript:  3.0
  Description:  
  History:
    07-24-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.fillInBlankQuestion {

  import engine.common.ui.buttons.Button;
	import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogActions;
	import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.common.utils.inputLabel.I_InputLabelCallbacks;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.common.utils.inputLabel.InputLabelActions;
  import engine.data.settings.fillInBlank.SvFillInBlankEntrySettings;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.SvDialogManager;
  import fl.controls.DataGrid;
	
  public class SvFillInBlankQuestionDialog extends BaseDialog implements I_InputLabelCallbacks,
  IDialogManagerCallbacks, I_SvDataGridViewCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: String
    private var mas_entries:Array;
    
    private var mo_dialogManager:SvDialogManager;
    
    private var mo_questionInput:InputLabel;
    
    private var mo_entriesDataGridView:SvFillInBlankEntryDataGridView;
    
    private var mo_addButton:Button;
    private var mo_editButton:Button;
    private var mo_removeButton:Button;
    private var mo_okButton:Button;
    private var mo_cancelButton:Button;
    
    private var mi_existingBeingEditedIndex:int;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvFillInBlankQuestionDialog(o_entrySettings:SvFillInBlankEntrySettings,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(FillInBlankQuestionDialogDisplay, SvDialogIds.FILL_IN_BLANK_QUESTION, o_callbacks);
			init(o_entrySettings);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dataGridViewOnChange
    //==================================================================
    public function dataGridViewOnChange(o_dataGridView:I_SvDataGridView):void {
      updateUiEnability();
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String,
    o_data:Object):void {
      if (s_dialogAction == DialogActions.COMPLETE) {
        handleDialogWordEntryComplete(o_data as String);
      }
    }
    
    //==================================================================
    // inputLabelOnAction
    //==================================================================
    public function inputLabelOnAction(o_inputLabel:InputLabel, s_inputLabelAction:String):void {
      if (s_inputLabelAction == InputLabelActions.CHANGE) {
        updateUiEnability();
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      mo_questionInput.uninit();
      mo_entriesDataGridView.uninit();
      mo_dialogManager.uninit();
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // areEntriesValid
    //==================================================================
    private function areEntriesValid():Boolean {
      if (!mas_entries.length) {
        return(false);
      }
      
      for each (var s_entry:String in mas_entries) {
        if (!s_entry) {
          return(false);
        }
      }
      
      return(true);
    }
    
    //==================================================================
    // containsValidQuestionText
    //==================================================================
    private function containsValidQuestionText():Boolean {
      return(Boolean(mo_questionInput.text));
    }
    
    //==================================================================
    // getAnswerWordEntry
    //==================================================================
    private function getAnswerWordEntry():String {
      var o_item:SvFillInBlankEntryDataItem = mo_entriesDataGridView.selectedItem;
      return(o_item ? o_item.entry : null);
    }
    
    //==================================================================
    // handleDialogWordEntryComplete
    //==================================================================
    private function handleDialogWordEntryComplete(s_entry:String):void {
      if (!s_entry) {
        return;
      }
      
      if (mi_existingBeingEditedIndex == -1) {
        //adding a new word entry
        mas_entries.push(s_entry);
        mo_entriesDataGridView.addEntry(s_entry);
      } else {
        //editing an existing word entry
        mas_entries[mi_existingBeingEditedIndex] = s_entry;
        mo_entriesDataGridView.replaceEntryAt(s_entry, mi_existingBeingEditedIndex);
      }
      
      updateUiEnability();
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_entrySettings:SvFillInBlankEntrySettings):void {
      initButtons();
      initQuestionText(o_entrySettings);
      initDataGrid(o_entrySettings);
      
      mo_dialogManager = new SvDialogManager(display, this);
      updateUiEnability();
    }
    
    //==================================================================
    // initAnswerEntries
    //==================================================================
    private function initAnswerEntries(o_entrySettings:SvFillInBlankEntrySettings):void {
      var as_entries:Array = [];
      
      if (o_entrySettings && o_entrySettings.answers) {
        as_entries = o_entrySettings.answers.concat();
      }
      
      mas_entries = as_entries;
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_addButton = initButton("add_button_display", onAddClick);
      mo_editButton = initButton("edit_button_display", onEditClick);
      mo_removeButton = initButton("remove_button_display", onRemoveClick);
      mo_okButton = initButton("ok_button_display", onOkClick);
      mo_cancelButton = initButton("cancel_button_display", onCancelClick);
    }
    
    //==================================================================
    // initDataGrid
    //==================================================================
    private function initDataGrid(o_entrySettings:SvFillInBlankEntrySettings):void {
      mo_entriesDataGridView = new SvFillInBlankEntryDataGridView(getSprite("datagrid_display") as DataGrid,
        this);
        
      initAnswerEntries(o_entrySettings);
      
      for each (var s_answer:String in mas_entries) {
        mo_entriesDataGridView.addEntry(s_answer);
      }
    }
    
    //==================================================================
    // initQuestionText
    //==================================================================
    private function initQuestionText(o_entrySettings:SvFillInBlankEntrySettings):void {
      var s_question:String = o_entrySettings ? o_entrySettings.question  || "" : "";
      mo_questionInput = new InputLabel(this, this, s_question, "txf_question");
    }
    
    //==================================================================
    // isQuestionValid
    //==================================================================
    private function isQuestionValid():Boolean {
      return(containsValidQuestionText() && areEntriesValid());
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    private function onAddClick(b_shift:Boolean):void {
      mi_existingBeingEditedIndex = -1;
      mo_dialogManager.openSimpleEntry();
    }
    
    //==================================================================
    // onCancelClick
    //==================================================================
    private function onCancelClick(b_shift:Boolean):void {
      cancel();
    }
    
    //==================================================================
    // onEditClick
    //==================================================================
    private function onEditClick(b_shift:Boolean):void {
      var s_entry:String = getAnswerWordEntry();
      if (s_entry) {
        mi_existingBeingEditedIndex = mo_entriesDataGridView.selectedIndex;
        mo_dialogManager.openSimpleEntry(s_entry);
      }
    }
    
    //==================================================================
    // onOkClick
    //==================================================================
    private function onOkClick(b_shift:Boolean):void {
      var o_completeData:SvFillInBlankEntrySettings = new SvFillInBlankEntrySettings({ });
      o_completeData.data[SvFillInBlankEntrySettings.PROP_QUESTION] = mo_questionInput.text;
      o_completeData.data[SvFillInBlankEntrySettings.PROP_ANSWERS] = mas_entries.concat();
      complete(o_completeData);
    }
    
    //==================================================================
    // onRemoveClick
    //==================================================================
    private function onRemoveClick(b_shift:Boolean):void {
      var i_selectedEntryIndex:int = mo_entriesDataGridView.selectedIndex;
      var s_entry:String = mas_entries[i_selectedEntryIndex] as String;
      if (!s_entry) {
        return;
      }
      
      mas_entries.splice(i_selectedEntryIndex, 1);
      
      mo_entriesDataGridView.removeItemAt(i_selectedEntryIndex);
      updateUiEnability(false);
    }
    
    //==================================================================
    // updateUiEnability
    //==================================================================
    private function updateUiEnability(o_bIsItemSelected:Object = null):void {
      var b_isItemSelected:Boolean = o_bIsItemSelected is Boolean ? Boolean(o_bIsItemSelected) :
        mo_entriesDataGridView.selectedIndex > -1;
      mo_editButton.enabled = b_isItemSelected;
      mo_removeButton.enabled = b_isItemSelected;
      
      mo_okButton.enabled = isQuestionValid();
    }
  }
}