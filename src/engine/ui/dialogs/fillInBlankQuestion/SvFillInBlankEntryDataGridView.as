/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvFillInBlankEntryDataGridView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          11-24-2018
  ActionScript:  3.0
  Description:  
  History:
    11-24-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.fillInBlankQuestion {
  import engine.common.baseView.BaseView;
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.dialogs.game.SvGameBaseDataGridColumnData;
  import fl.controls.DataGrid;
  import fl.controls.dataGridClasses.DataGridColumn;
  import fl.data.DataProvider;
  import flash.events.Event;
  import flib.utils.FDataBlock;
	
  internal class SvFillInBlankEntryDataGridView extends BaseView implements I_SvDataGridView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_dataGrid:DataGrid;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvFillInBlankEntryDataGridView(o_spriteOrSpriteClass:Object,
    o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, o_callbacks);
      init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addEntry
    //==================================================================
    public function addEntry(s_entry:String):Boolean {
      if (!s_entry) {
        return(false); //sanity check
      }
      
      var o_itemData:SvFillInBlankEntryDataItem = new SvFillInBlankEntryDataItem(s_entry);
      mo_dataGrid.addItem(o_itemData);
      return(true);
    }
    
    //==================================================================
    // getItemAt
    //==================================================================
    public function getItemAt(u_selectedIndex:uint):SvFillInBlankEntryDataItem {
      var o_dataProvider:DataProvider = mo_dataGrid.dataProvider;
      if (u_selectedIndex >= o_dataProvider.length) {
        return(null);
      }
      
      return(o_dataProvider.getItemAt(u_selectedIndex) as SvFillInBlankEntryDataItem);
    }
    
    //==================================================================
    // isItemSelected (get)
    //==================================================================
    public function get isItemSelected():Boolean {
      return(selectedItem != null);
    }
    
    //==================================================================
    // removeAll
    //==================================================================
    public function removeAll():void {
      var o_dataProvider:DataProvider = mo_dataGrid.dataProvider;
      o_dataProvider.removeAll();
    }
    
    //==================================================================
    // removeItemAt
    //==================================================================
    public function removeItemAt(u_index:uint):void {
      var o_dataProvider:DataProvider = mo_dataGrid.dataProvider;
      if (u_index < o_dataProvider.length) {
        var b_isSelectedItemBeingRemoved:Boolean = int(u_index) == selectedIndex;
        if (b_isSelectedItemBeingRemoved) {
          mo_dataGrid.clearSelection();
        }
        
        o_dataProvider.removeItemAt(u_index);
      }
    }
    
    //==================================================================
    // replaceEntryAt
    //==================================================================
    public function replaceEntryAt(s_entry:String, u_index:uint):void {
      var o_dataProvider:DataProvider = mo_dataGrid.dataProvider;
      if (u_index < o_dataProvider.length) {
        o_dataProvider.replaceItemAt(s_entry, u_index);
      }
    }
    
    //==================================================================
    // selectedIndex (get)
    //==================================================================
    public function get selectedIndex():int {
      return(mo_dataGrid.selectedIndex);
    }
    
    //==================================================================
    // selectedItem (get)
    //==================================================================
    public function get selectedItem():SvFillInBlankEntryDataItem {
      return(mo_dataGrid.selectedItem as SvFillInBlankEntryDataItem);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      mo_dataGrid = display as DataGrid;
      mo_dataGrid.addEventListener(Event.CHANGE, onDataGridChange, false, 0, true);
      initColumnHeaders();
    }
    
    //==================================================================
    // initColumnHeaders
    //==================================================================
    private function initColumnHeaders():void {
      for each (var o_cellData:SvGameBaseDataGridColumnData in SvFillInBlankDataGridColumnsData.HEADER_DATA) {
        var o_column:DataGridColumn = new DataGridColumn();
        if (o_cellData.headerText) {
          o_column.headerText = o_cellData.headerText;
        } else {
          o_column.visible = false;
        }
        
        o_column.dataField = o_cellData.dataField;
        mo_dataGrid.addColumn(o_column);
      }
    }
    
    //==================================================================
    // onDataGridChange
    //==================================================================
    private function onDataGridChange(o_event:Event):void {
      var o_callbacks:I_SvDataGridViewCallbacks = callbacks as I_SvDataGridViewCallbacks;
      if (o_callbacks) {
        o_callbacks.dataGridViewOnChange(this);
      }
    }
  }
}