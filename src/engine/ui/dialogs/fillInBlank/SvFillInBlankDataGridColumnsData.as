/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvFillInBlankDataGridColumnsData

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-25-2018
  ActionScript:  3.0
  Description:  
  History:
    07-25-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.fillInBlank {
  import engine.data.settings.fillInBlank.SvFillInBlankEntrySettings;
  import engine.ui.dialogs.game.SvGameBaseDataGridColumnData;
  import flib.utils.FDataBlock;
  
  internal final class SvFillInBlankDataGridColumnsData {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    internal static const HEADER_DATA:Vector.<SvGameBaseDataGridColumnData> =
    new <SvGameBaseDataGridColumnData> [
      new SvGameBaseDataGridColumnData("Question", SvFillInBlankEntrySettings.PROP_QUESTION),
      new SvGameBaseDataGridColumnData("Answer", SvFillInBlankEntrySettings.PROP_ANSWERS)
    ];
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}