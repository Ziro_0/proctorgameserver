/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvFillInBlankDataGridView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          07-25-2018
  ActionScript:  3.0
  Description:  
  History:
    07-25-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.fillInBlank {
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.data.settings.fillInBlank.SvFillInBlankEntrySettings;
  import engine.ui.dialogs.game.SvGameBaseDataGridItem;
  import engine.ui.dialogs.game.SvGameBaseDataGridView;
	
  internal class SvFillInBlankDataGridView extends SvGameBaseDataGridView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvFillInBlankDataGridView(o_spriteOrSpriteClass:Object,
    o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, SvFillInBlankDataGridColumnsData.HEADER_DATA, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onCreateDataGridItem
    //==================================================================
    override protected function onCreateDataGridItem(o_entryData:Object):SvGameBaseDataGridItem {
      var o_entrySettings:SvFillInBlankEntrySettings = o_entryData as SvFillInBlankEntrySettings;
      return(o_entrySettings ? new SvFillInBlankDataItem(o_entrySettings.question, o_entrySettings.answers) : null);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}