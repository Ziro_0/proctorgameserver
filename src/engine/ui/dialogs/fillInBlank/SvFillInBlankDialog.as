/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvFillInBlankDialog

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          07-23-2018
  ActionScript:  3.0
  Description:  
  History:
    07-23-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.fillInBlank {

  import engine.I_SvEngine;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.data.settings.SvMiniGameBaseEntrySettings;
  import engine.data.settings.fillInBlank.SvFillInBlankEntrySettings;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.fillInBlankQuestion.SvFillInBlankQuestionDialog;
  import engine.ui.dialogs.game.SvGameBaseDialog;
	
  public class SvFillInBlankDialog extends SvGameBaseDialog {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvFillInBlankDialog(o_spriteOrSpriteClass:Object, o_engine:I_SvEngine,
    s_gameId:String, s_dialogId:String, o_settingsData:Object,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(o_spriteOrSpriteClass, o_engine, s_gameId, s_dialogId, o_settingsData, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    override public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String,
    o_data:Object):void {
      super.dialogManagerOnAction(s_dialogId, s_dialogAction, o_data);
      
      if (s_dialogAction != DialogActions.COMPLETE) {
        return;
      }
      
      if (s_dialogId == SvDialogIds.FILL_IN_BLANK_QUESTION) {
        handleFillInBlankQuestionDialogComplete(o_data as SvFillInBlankEntrySettings);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // initContentDataGrid
    //==================================================================
    override protected function initContentDataGrid():void {
      mo_contentDataGrid = new SvFillInBlankDataGridView(getSprite("dg_content_display"), this);
      super.initContentDataGrid();
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    protected override function onAddClick(b_shift:Boolean):void {
      super.onAddClick(b_shift);
      var o_entrySettings:SvFillInBlankEntrySettings = null;
      mo_dialogManager.openDialog(new SvFillInBlankQuestionDialog(o_entrySettings, mo_dialogManager));
    }
    
    //==================================================================
    // onCreateBaseEntrySettings
    //==================================================================
    override protected function onCreateBaseEntrySettings(o_entryData:Object):SvMiniGameBaseEntrySettings {
      return(new SvFillInBlankEntrySettings(o_entryData));
    }
    
    //==================================================================
    // onEditSelectedEntry
    //==================================================================
    override protected function onEditSelectedEntry(o_data:Object):void {
      var o_entrySettings:SvFillInBlankEntrySettings = new SvFillInBlankEntrySettings(o_data);
      mo_dialogManager.openDialog(new SvFillInBlankQuestionDialog(o_entrySettings, mo_dialogManager));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // handleFillInBlankQuestionDialogComplete
    //==================================================================
    private function handleFillInBlankQuestionDialogComplete(o_entrySettings:SvFillInBlankEntrySettings):void {
      addOrReplaceEntry(o_entrySettings);
    }
  }
}