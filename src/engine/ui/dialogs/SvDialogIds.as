/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvDialogIds

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-15-2018
  ActionScript:  3.0
  Description:  
  History:
    04-15-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs {
  
  public final class SvDialogIds {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public static const GAME_FILL_IN_BLANK:String = "game-fill-in-blank";
    public static const GAME_LETTERS:String = "game-letters";
    public static const GAME_MULTIPLE_CHOICE:String = "game-multiple-choice";
    public static const GAME_UNSCRAMBLE:String = "game-unscramble";
    
    public static const GAMES:String = "games";
    
    public static const FILL_IN_BLANK_QUESTION:String = "full-in-blank-question";
    public static const MULTIPLE_CHOICE_QUESTION:String = "multiple-choice-question";
    public static const MULTIPLE_CHOICE_QUESTION_OPTION:String = "multiple-choice-question-option";
    public static const LETTERS_ENTRY:String = "letters-entry";
    
    public static const CONFIRM:String = "confirm";
    public static const SIMPLE_ENTRY:String = "simple-entry";
    public static const THEMES:String = "themes";
    public static const SETTINGS_FILE:String = "settings-file";
    public static const MESSAGE:String = "message";
    public static const PROGRESS:String = "progress";
    public static const MESSAGE_IMPORT:String = "message-import";
    
    public static const CUSTOM_IMAGE_SIZE:String = "custom-image-size";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}