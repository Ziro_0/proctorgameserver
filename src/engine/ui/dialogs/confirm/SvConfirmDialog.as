/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvConfirmDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          01-04-2019
  ActionScript:  3.0
  Description:  
  History:
    01-04-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.confirm {
  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.ui.dialogs.SvDialogIds;
	
  public class SvConfirmDialog extends BaseDialog {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_yesButton:Button;
    private var mo_noButton:Button;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvConfirmDialog(s_messageText:String, o_callbacks:IBaseDialogCallbacks = null,
    s_headerText:String = null) {
      super(ConfirmDialogDisplay, SvDialogIds.CONFIRM, o_callbacks);
			init(s_messageText, s_headerText);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(s_messageText:String, s_headerText:String):void {
      initButtons();
      initMessageTest(s_messageText);
      initHeaderText(s_headerText);
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_yesButton = initButton("yes_button_display", onYesClick);
      mo_noButton = initButton("no_button_display", onNoClick);
    }
    
    //==================================================================
    // initHeaderText
    //==================================================================
    private function initHeaderText(s_headerText:String):void {
      getTextField("txf_header", s_headerText);
    }
    
    //==================================================================
    // initMessageTest
    //==================================================================
    private function initMessageTest(s_messageText:String):void {
      getTextField("txf_message", s_messageText || "");
    }
    
    //==================================================================
    // onNoClick
    //==================================================================
    private function onNoClick(b_shift:Boolean):void {
      cancel(false);
    }
    
    //==================================================================
    // onYesClick
    //==================================================================
    private function onYesClick(b_shift:Boolean):void {
      complete(true);
    }
  }
}