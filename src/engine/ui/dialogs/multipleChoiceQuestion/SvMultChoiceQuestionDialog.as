/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultChoiceQuestionDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-23-2018
  ActionScript:  3.0
  Description:  
  History:
    06-23-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.multipleChoiceQuestion {

  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.common.utils.inputLabel.I_InputLabelCallbacks;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.common.utils.inputLabel.InputLabelActions;
  import engine.data.settings.multipleChoice.SvMultChoiceEntrySettings;
  import engine.data.settings.multipleChoice.SvMultChoiceQuestionOptionSettings;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.SvDialogManager;
  import fl.controls.DataGrid;
  import flash.text.TextField;
  
  public class SvMultChoiceQuestionDialog extends BaseDialog implements I_InputLabelCallbacks,
  IDialogManagerCallbacks, I_SvDataGridViewCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: SvMultChoiceQuestionOptionSettings
    private var mao_choices:Array;
    
    private var mo_dialogManager:SvDialogManager;
    
    private var mo_dataGridView:SvMultChoiceQuestionDataGridView;
    
    private var mo_questionInputLabel:InputLabel;
    
    private var mo_addButton:Button;
    private var mo_editButton:Button;
    private var mo_removeButton:Button;
    private var mo_okButton:Button;
    private var mo_cancelButton:Button;
    private var mi_existingBeingEditedIndex:int;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMultChoiceQuestionDialog(o_entrySettings:SvMultChoiceEntrySettings,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(MultipleChoiceQuestionDialogDisplay, SvDialogIds.MULTIPLE_CHOICE_QUESTION, o_callbacks);
      init(o_entrySettings);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dataGridViewOnChange
    //==================================================================
    public function dataGridViewOnChange(o_dataGridView:I_SvDataGridView):void {
      updateUiEnability();
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction == DialogActions.COMPLETE) {
        handleChoiceComplete(o_data as SvMultChoiceQuestionOptionSettings);
      }
    }
    
    //==================================================================
    // inputLabelOnAction
    //==================================================================
    public function inputLabelOnAction(o_inputLabel:InputLabel, s_inputLabelAction:String):void {
      if (s_inputLabelAction == InputLabelActions.CHANGE ||
      s_inputLabelAction == InputLabelActions.FOCUS_OUT) {
        updateUiEnability();
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // areChoicesValid
    //==================================================================
    private function areChoicesValid():Boolean {
      if (mao_choices.length == 0) {
        //must contain at least once choice
        return(false);
      }
      
      var u_numCorrectChoices:uint;
      for each (var o_choiceSettings:SvMultChoiceQuestionOptionSettings in mao_choices) {
        if (!o_choiceSettings.text) {
          //choice must contain question text
          return(false);
        }
        
        if (o_choiceSettings.isCorrect) {
          u_numCorrectChoices++;
        }
      }
      
      //there can only be one correct choice
      return(u_numCorrectChoices == 1);
    }
    
    //==================================================================
    // getCorrectChoiceIndexOf
    //==================================================================
    private function getCorrectChoiceIndexOf():int {
      for (var i_index:int = 0; i_index < mao_choices.length; i_index++) {
        var o_choice:SvMultChoiceQuestionOptionSettings = mao_choices[i_index] as SvMultChoiceQuestionOptionSettings;
        if (o_choice && o_choice.isCorrect) {
          return(i_index);
        }
      }
      return(-1);
    }
    
    //==================================================================
    // handleChoiceComplete
    //==================================================================
    private function handleChoiceComplete(o_settings:SvMultChoiceQuestionOptionSettings):void {
      updateCorrectChoiceIntegrtity(o_settings);
      
      if (mi_existingBeingEditedIndex == -1) {
        //is adding a new question option
        mao_choices.push(o_settings);
        mo_dataGridView.addEntry(o_settings);
      } else {
        //is editing an existing question option
        mao_choices[mi_existingBeingEditedIndex] = o_settings;
        mo_dataGridView.replaceEntryAt(o_settings, mi_existingBeingEditedIndex);
      }
      
      updateUiEnability();
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_entrySettings:SvMultChoiceEntrySettings):void {
      o_entrySettings = o_entrySettings ? o_entrySettings.clone() as SvMultChoiceEntrySettings : null;
      initQuestionLabel(o_entrySettings);
      initDataGrid();
      initButtons();
      initChoices(o_entrySettings);
      
      mo_dialogManager = new SvDialogManager(display, this);
      updateUiEnability();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_addButton = initButton("add_button_display", onAddClick);
      mo_editButton = initButton("edit_button_display", onEditClick);
      mo_removeButton = initButton("remove_button_display", onRemoveClick);
      mo_okButton = initButton("ok_button_display", onOkClick);
      mo_cancelButton = initButton("cancel_button_display", onCancelClick);
    }
    
    //==================================================================
    // initChoices
    //==================================================================
    private function initChoices(o_entrySettings:SvMultChoiceEntrySettings):void {
      var ao_choices:Array = [ ];
      
      if (o_entrySettings) {
        for each (var o_rawQuestionOptionData:Object in o_entrySettings.choices) {
          var o_questionOptionSettings:SvMultChoiceQuestionOptionSettings = 
            new SvMultChoiceQuestionOptionSettings(o_rawQuestionOptionData);
          ao_choices.push(o_questionOptionSettings);
          mo_dataGridView.addEntry(o_questionOptionSettings);
        }
      }
      
      mao_choices = ao_choices;
    }
    
    //==================================================================
    // initDataGrid
    //==================================================================
    private function initDataGrid():void {
      mo_dataGridView = new SvMultChoiceQuestionDataGridView(getSprite("datagrid_display") as DataGrid, this);
    }
    
    //==================================================================
    // initQuestionLabel
    //==================================================================
    private function initQuestionLabel(o_entrySettings:SvMultChoiceEntrySettings):void {
      var s_defaultText:String = o_entrySettings ? o_entrySettings.question : "";
      mo_questionInputLabel = new InputLabel(this, this, s_defaultText, "txf_input_question");
    }
    
    //==================================================================
    // isQuestionValid
    //==================================================================
    private function isQuestionValid():Boolean {
      return(mo_questionInputLabel.text.length > 0);
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    private function onAddClick(b_shift:Boolean):void {
      var b_isCorrect:Boolean = mao_choices.length == 0;
      var b_isCorrectToggleEnabled:Boolean = mao_choices.length > 0;
      mi_existingBeingEditedIndex = -1;
      mo_dialogManager.openMultChoiceQuestionOption(null, b_isCorrect, b_isCorrectToggleEnabled);
    }
    
    //==================================================================
    // onCancelClick
    //==================================================================
    private function onCancelClick(b_shift:Boolean):void {
      cancel();
    }
    
    //==================================================================
    // onEditClick
    //==================================================================
    private function onEditClick(b_shift:Boolean):void {
      var o_choiceSettings:SvMultChoiceQuestionOptionSettings =
        mao_choices[mo_dataGridView.selectedIndex] as SvMultChoiceQuestionOptionSettings;
      if (!o_choiceSettings) {
        //sanity check
        return;
      }
      
      var b_isCorrectToggleEnabled:Boolean = mao_choices.length > 1;
      mi_existingBeingEditedIndex = mo_dataGridView.selectedIndex;
      mo_dialogManager.openMultChoiceQuestionOption(o_choiceSettings.text, o_choiceSettings.isCorrect,
        b_isCorrectToggleEnabled);
    }
    
    //==================================================================
    // onOkClick
    //==================================================================
    private function onOkClick(b_shift:Boolean):void {
      var o_completeData:SvMultChoiceQuestionDialogCompleteData = 
        new SvMultChoiceQuestionDialogCompleteData(mo_questionInputLabel.text, mao_choices);
      complete(o_completeData);
    }
    
    //==================================================================
    // onRemoveClick
    //==================================================================
    private function onRemoveClick(b_shift:Boolean):void {
      var i_selectedIndex:int = mo_dataGridView.selectedIndex;
      if (i_selectedIndex == -1) {
        return;
      }
      
      mao_choices.splice(i_selectedIndex, 1);
      mo_dataGridView.removeItemAt(i_selectedIndex);
      updateCorrectChoiceIntegrtity();
      updateUiEnability(false);
    }
    
    //==================================================================
    // setEntryIsCorrectAt
    //==================================================================
    private function setEntryIsCorrectAt(u_currentCorrectChoiceIndex:uint, b_isCorrect:Boolean):void {
      if (u_currentCorrectChoiceIndex >= mao_choices.length) {
        return;
      }
      
      var o_currentCorrectChoice:SvMultChoiceQuestionOptionSettings =
        mao_choices[u_currentCorrectChoiceIndex] as SvMultChoiceQuestionOptionSettings;
      if (!o_currentCorrectChoice) {
        return;
      }
      
      o_currentCorrectChoice.isCorrect = b_isCorrect;
      mo_dataGridView.replaceEntryAt(o_currentCorrectChoice, u_currentCorrectChoiceIndex);
    }
    
    //==================================================================
    // setFirstChoiceAsCorrect
    //==================================================================
    private function setFirstChoiceAsCorrect():void {
      setEntryIsCorrectAt(0, true);
    }
    
    //==================================================================
    // updateCorrectChoiceIntegrtity
    //==================================================================
    private function updateCorrectChoiceIntegrtity(
    o_newCorrectChoice:SvMultChoiceQuestionOptionSettings = null):void {
      var i_currentCorrectChoiceIndex:int = getCorrectChoiceIndexOf();
      
      if (o_newCorrectChoice && o_newCorrectChoice.isCorrect) {
        setEntryIsCorrectAt(i_currentCorrectChoiceIndex, false);
      } else {
        if (i_currentCorrectChoiceIndex == -1) {
          setFirstChoiceAsCorrect();
        }
      }
    }
    
    //==================================================================
    // updateUiEnability
    //==================================================================
    private function updateUiEnability(o_bIsItemSelected:Object = null):void {
      var b_isItemSelected:Boolean = o_bIsItemSelected is Boolean ? Boolean(o_bIsItemSelected) :
        mo_dataGridView.selectedIndex > -1;
      mo_editButton.enabled = b_isItemSelected;
      mo_removeButton.enabled = b_isItemSelected;
      
      mo_okButton.enabled = isQuestionValid() && areChoicesValid();
    }
  }
}