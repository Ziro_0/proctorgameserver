/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvSimpleEntryDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-18-2018
  ActionScript:  3.0
  Description:  
  History:
    05-18-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.simpleEntry {
  import engine.common.ui.buttons.Button;
	import engine.common.ui.dialogs.BaseDialog;
	import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.ui.dialogs.SvDialogIds;
  import flash.events.Event;
  import flash.text.TextField;
	
  public class SvSimpleEntryDialog extends BaseDialog {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_okButton:Button;
    private var mo_cancelButton:Button;
    private var mo_txfHeader:TextField;
    private var mo_txfInput:TextField;
    private var mb_initialFocusApplied:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvSimpleEntryDialog(s_defaultText:String = "", o_callbacks:IBaseDialogCallbacks = null,
    s_headerText:String = null) {
      super(SimpleEntryDialogDisplay, SvDialogIds.SIMPLE_ENTRY, o_callbacks);
			init(s_defaultText, s_headerText);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      display.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
      mo_txfInput.removeEventListener(Event.CHANGE, onTxfInputChange);
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // init
    //==================================================================
    private function init(s_defaultText:String, s_headerText:String):void {
      initDisplay();
      initButtons();
      initText(s_defaultText);
      initHeaderText(s_headerText);
      updateUiState();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_okButton = initButton("ok_button_display", onOkClick);
      mo_cancelButton = initButton("cancel_button_display", onCancelClick);
    }
    
    //==================================================================
    // initDisplay
    //==================================================================
    private function initDisplay():void {
      if (!display.stage) {
        display.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
      }
    }
    
    //==================================================================
    // initHeaderText
    //==================================================================
    private function initHeaderText(s_text:String):void {
      mo_txfHeader = getTextField("txf_header", s_text);
    }
    
    //==================================================================
    // onAddedToStage
    //==================================================================
    private function onAddedToStage(o_event:Event):void {
      display.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
      
      if (!mb_initialFocusApplied) {
        mb_initialFocusApplied = true;
        display.stage.focus = mo_txfInput;
        mo_txfInput.setSelection(0, mo_txfInput.text.length);
      }
    }
    
    //==================================================================
    // initText
    //==================================================================
    private function initText(s_defaultText:String):void {
      mo_txfInput = getTextField("txf_input", s_defaultText || "");
      mo_txfInput.addEventListener(Event.CHANGE, onTxfInputChange, false, 0, true);
    }
    
    //==================================================================
    // isInputTextReady
    //==================================================================
    private function isInputTextReady():Boolean {
      return(mo_txfInput.text.length > 0);
    }
    
    //==================================================================
    // onCancelClick
    //==================================================================
    private function onCancelClick(b_shift:Boolean):void {
      cancel();
    }
    
    //==================================================================
    // onOkClick
    //==================================================================
    private function onOkClick(b_shift:Boolean):void {
      complete(mo_txfInput.text);
    }
    
    //==================================================================
    // onTxfInputChange
    //==================================================================
    private function onTxfInputChange(o_event:Event):void {
      updateUiState();
    }
    
    //==================================================================
    // updateUiState
    //==================================================================
    private function updateUiState():void {
      mo_okButton.enabled = isInputTextReady();
    }
  }
}