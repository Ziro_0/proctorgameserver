/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultChoiceDataItem

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-20-2018
  ActionScript:  3.0
  Description:  
  History:
    06-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.multipleChoice {
  import engine.ui.dialogs.game.SvGameBaseDataGridItem;
  
  internal class SvMultChoiceDataItem extends SvGameBaseDataGridItem {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public var text:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMultChoiceDataItem(s_text:String) {
      text = s_text;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onEquals
    //==================================================================
    override protected function onEquals(o_item:SvGameBaseDataGridItem):Boolean {
      var o_customItem:SvMultChoiceDataItem = o_item as SvMultChoiceDataItem;
      return(o_customItem &&
        text == o_customItem.text);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}