/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultChoiceDataGridColumnsData

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-20-2018
  ActionScript:  3.0
  Description:  
  History:
    06-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.multipleChoice {
  import engine.data.settings.multipleChoice.SvMultChoiceQuestionOptionSettings;
  import engine.ui.dialogs.game.SvGameBaseDataGridColumnData;
  
  internal final class SvMultChoiceDataGridColumnsData {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    internal static const HEADER_DATA:Vector.<SvGameBaseDataGridColumnData> =
    new <SvGameBaseDataGridColumnData> [
      new SvGameBaseDataGridColumnData("Question", SvMultChoiceQuestionOptionSettings.PROP_TEXT)
    ];
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}