/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvMultChoiceDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          06-20-2018
  ActionScript:  3.0
  Description:  
  History:
    06-20-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.multipleChoice {
  import engine.I_SvEngine;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.data.settings.SvMiniGameBaseEntrySettings;
  import engine.data.settings.multipleChoice.SvMultChoiceEntrySettings;
  import engine.data.settings.multipleChoice.SvMultChoiceQuestionOptionSettings;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.game.SvGameBaseDialog;
  import engine.ui.dialogs.multipleChoiceQuestion.SvMultChoiceQuestionDialog;
  import engine.ui.dialogs.multipleChoiceQuestion.SvMultChoiceQuestionDialogCompleteData;
	
  public class SvMultChoiceDialog extends SvGameBaseDialog {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const GAME_TIME_TEXT_ALLOWED_CHARS:String = "1234567890";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvMultChoiceDialog(o_spriteOrSpriteClass:Object, o_engine:I_SvEngine,
    s_gameId:String, s_dialogId:String, o_settingsData:Object,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(o_spriteOrSpriteClass, o_engine, s_gameId, s_dialogId, o_settingsData, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    override public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String,
    o_data:Object):void {
      super.dialogManagerOnAction(s_dialogId, s_dialogAction, o_data);
      
      if (s_dialogAction != DialogActions.COMPLETE) {
        return;
      }
      
      if (s_dialogId == SvDialogIds.MULTIPLE_CHOICE_QUESTION) {
        handleMultipleChoiceQuestionDialogComplete(o_data as SvMultChoiceQuestionDialogCompleteData);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // initContentDataGrid
    //==================================================================
    override protected function initContentDataGrid():void {
      mo_contentDataGrid = new SvMultChoiceDataGridView(getSprite("dg_content_display"), this);
      super.initContentDataGrid();
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    protected override function onAddClick(b_shift:Boolean):void {
      super.onAddClick(b_shift);
      var o_entrySettings:SvMultChoiceEntrySettings = null;
      mo_dialogManager.openDialog(new SvMultChoiceQuestionDialog(o_entrySettings,
        mo_dialogManager));
    }
    
    //==================================================================
    // onCreateBaseEntrySettings
    //==================================================================
    override protected function onCreateBaseEntrySettings(o_entryData:Object):SvMiniGameBaseEntrySettings {
      return(new SvMultChoiceEntrySettings(o_entryData));
    }
    
    //==================================================================
    // onEditSelectedEntry
    //==================================================================
    override protected function onEditSelectedEntry(o_data:Object):void {
      var o_entrySettings:SvMultChoiceEntrySettings = new SvMultChoiceEntrySettings(o_data);
      mo_dialogManager.openDialog(new SvMultChoiceQuestionDialog(o_entrySettings, mo_dialogManager));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // buildEntrySettingsFromQuestionDialogCompleteData
    //==================================================================
    private function buildEntrySettingsFromQuestionDialogCompleteData(o_completeData:SvMultChoiceQuestionDialogCompleteData):SvMultChoiceEntrySettings {
      var ao_rawChoices:Array = [];
      for each (var o_choice:SvMultChoiceQuestionOptionSettings in o_completeData.choices) {
        ao_rawChoices.push(o_choice.data);
      }
      
      var o_entrySettings:SvMultChoiceEntrySettings = new SvMultChoiceEntrySettings();
      o_entrySettings.question = o_completeData.questionText;
      o_entrySettings.choices = ao_rawChoices;
      return(o_entrySettings);
    }
    
    //==================================================================
    // handleMultipleChoiceQuestionDialogComplete
    //==================================================================
    private function handleMultipleChoiceQuestionDialogComplete(o_completeData:SvMultChoiceQuestionDialogCompleteData):void {
      var o_entrySettings:SvMultChoiceEntrySettings =
        buildEntrySettingsFromQuestionDialogCompleteData(o_completeData);
      addOrReplaceEntry(o_entrySettings);
    }
  }
}