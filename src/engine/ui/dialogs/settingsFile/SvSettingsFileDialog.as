/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvSettingsFileDialog

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          03-15-2019
  ActionScript:  3.0
  Description:  
  History:
    03-15-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.settingsFile {
  import engine.I_SvEngine;
  import engine.common.ui.buttons.Button;
	import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.DialogManager;
	import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.ui.dialogs.SvDialogActions;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.confirm.SvConfirmDialog;
  import flash.desktop.Clipboard;
  import flash.desktop.ClipboardFormats;
  import flash.events.TimerEvent;
  import flash.filesystem.File;
  import flib.utils.timers.FTimer;
	
  public class SvSettingsFileDialog extends BaseDialog implements IDialogManagerCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const MSG_COPY_TO_CLIPBOARD_FAILED:String = "Copy to clipboard failed...";
    private static const MSG_COPY_OK:String = "Copy to clipboard OK!";
    
    private static const MESSAGE_TIMER_DELAY_MS:uint = 5 * 1000;
    
    private static const CONFIRM_RESET_SETTINGS_MESSAGE:String = "This will remove " +
      "all custom settings, including IP address, themes, and games. " +
      "Are you sure you want to do this?";
    
    private static const MSG_RESET_FAILED:String = "Failed to reset settings...";
    private static const MSG_RESET_OK:String = "Successfully reset settings!";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_SvEngine;
    private var mo_dialogManager:DialogManager;
    
    private var mo_closeButton:Button;
    private var mo_copyToClipboardButton:Button;
    private var mo_resetButton:Button;
    
    private var mo_messageTimer:FTimer;
    
    private var ms_fileUrl:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvSettingsFileDialog(o_engine:I_SvEngine, o_file:File,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(SettingsFileDialogDisplay, SvDialogIds.SETTINGS_FILE, o_callbacks);
			init(o_engine, o_file);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // data (set)
    //==================================================================
    override public function set data(o_value:Object):void {
      if (o_value is Boolean) {
        if (Boolean(o_value)) {
          presentStatusMessage(MSG_RESET_OK);
        } else {
          presentStatusMessage(MSG_RESET_FAILED);
        }
      }
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String,
    o_data:Object):void {
      if (s_dialogId == SvDialogIds.CONFIRM) {
        handleResetConfirmDialog(s_dialogAction, o_data);
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      stopMessageTimer();
      mo_dialogManager.uninit();
      mo_engine = null;
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // copyToClipboard
    //==================================================================
    private function copyToClipboard():void {
      try {
        if (!Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, ms_fileUrl)) {
          presentStatusMessage(MSG_COPY_TO_CLIPBOARD_FAILED);
          return;
        }
      } catch (o_error:Error) {
        trace("SvSettingsFileDialog.copyToClipboard. Error: " + o_error);
        presentStatusMessage(MSG_COPY_TO_CLIPBOARD_FAILED);
        return;
      }
      
      presentStatusMessage(MSG_COPY_OK);
    }
    
    //==================================================================
    // handleResetConfirmDialog
    //==================================================================
    private function handleResetConfirmDialog(s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction == DialogActions.COMPLETE) {
        notifyResetSettings();
      }
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_SvEngine, o_file:File):void {
      mo_engine = o_engine;
      ms_fileUrl = o_file ? o_file.nativePath : "";
      
      mo_dialogManager = new DialogManager(mo_engine.mainView.display, this);
      initButtons();
      initFileUrlText();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_closeButton = initButton("close_button_display", onCloseClick);
      mo_copyToClipboardButton = initButton("copy_to_clipboard_button_display",
        onCopyToClipboardClick);
      mo_resetButton = initButton("reset_button_display", onResetClick);
    }
    
    //==================================================================
    // initFileUrlText
    //==================================================================
    private function initFileUrlText():void {
      getTextField("txf_settings_file_url", ms_fileUrl || "[no file available]");
    }
    
    //==================================================================
    // notifyResetSettings
    //==================================================================
    private function notifyResetSettings():void {
      var o_callbacks:IBaseDialogCallbacks = callbacks as IBaseDialogCallbacks;
      if (o_callbacks) {
        o_callbacks.baseDialogOnAction(id, SvDialogActions.RESET_SETTINGS, null);
      }
    }
    
    //==================================================================
    // onCloseClick
    //==================================================================
    private function onCloseClick(b_shift:Boolean):void {
      complete();
    }
    
    //==================================================================
    // onCopyToClipboardClick
    //==================================================================
    private function onCopyToClipboardClick(b_shift:Boolean):void {
      copyToClipboard();
    }
    
    //==================================================================
    // onResetClick
    //==================================================================
    private function onResetClick(b_shift:Boolean):void {
      mo_dialogManager.openDialog(new SvConfirmDialog(CONFIRM_RESET_SETTINGS_MESSAGE,
        mo_dialogManager));
    }
    
    //==================================================================
    // presentStatusMessage
    //==================================================================
    private function presentStatusMessage(s_message:String):void {
      getTextField("txf_copy_status", s_message);
      startMessageTimer();
    }
    
    //==================================================================
    // onMessageTimerComplete
    //==================================================================
    private function onMessageTimerComplete(o_event:TimerEvent):void {
      getTextField("txf_copy_status", "");
    }
    
    //==================================================================
    // startMessageTimer
    //==================================================================
    private function startMessageTimer():void {
      stopMessageTimer();
      mo_messageTimer = new FTimer(MESSAGE_TIMER_DELAY_MS, 1, null, onMessageTimerComplete);
    }
    
    //==================================================================
    // stopMessageTimer
    //==================================================================
    private function stopMessageTimer():void {
      mo_messageTimer = FTimer.UninitTimer(mo_messageTimer);
    }
  }
}