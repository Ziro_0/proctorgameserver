/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGamesDataGridView

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-09-2018
  ActionScript:  3.0
  Description:  
  History:
    05-09-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.games {
	import engine.common.baseView.BaseView;
	import engine.common.baseView.I_BaseViewCallbacks;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import fl.controls.DataGrid;
  import fl.controls.dataGridClasses.DataGridColumn;
  import flash.events.Event;
  import flib.utils.FDataBlock;
	
  internal class SvGamesDataGridView extends BaseView implements I_SvDataGridView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_dataGrid:DataGrid;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGamesDataGridView(o_spriteOrSpriteClass:Object, o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, o_callbacks);
      init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addGame
    //==================================================================
    public function addGame(s_gameId:String, s_name:String = null):Boolean {
      if (!s_gameId) {
        return(false); //sanity check
      }
      
      if (!s_name) {
        s_name = s_gameId;
      }
      
      var o_itemData:SvGameDataItem = new SvGameDataItem(s_gameId, s_name);
      mo_dataGrid.addItem(o_itemData);
      return(true);
    }
    
    //==================================================================
    // isItemSelected (get)
    //==================================================================
    public function get isItemSelected ():Boolean {
      return(selectedGameId != null);
    }
    
    //==================================================================
    // selectedGameId (get)
    //==================================================================
    public function get selectedGameId():String {
      var o_item:SvGameDataItem = mo_dataGrid.selectedItem as SvGameDataItem;
      return(o_item ? o_item.id : null);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getGame
    //==================================================================
    private function getUser(s_gameId:String):SvGameDataItem {
      for (var u_index:uint = 0; u_index < mo_dataGrid.length; u_index++) {
        var o_item:SvGameDataItem = mo_dataGrid.getItemAt(u_index) as SvGameDataItem;
        if (o_item.id == s_gameId) {
          return(o_item);
        }
      }
      
      return(null);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      mo_dataGrid = display as DataGrid;
      mo_dataGrid.addEventListener(Event.CHANGE, onDataGridChange, false, 0, true);
      initColumnHeaders();
    }
    
    //==================================================================
    // initColumnHeaders
    //==================================================================
    private function initColumnHeaders():void {
      for each (var o_cellData:FDataBlock in SvGamesDataGridColumnsData.HEADER_DATA) {
        var o_column:DataGridColumn = new DataGridColumn();
        if (o_cellData.containsProperty("headerText")) {
          o_column.headerText = o_cellData.getString("headerText");
        } else {
          o_column.visible = false;
        }
        
        o_column.dataField = o_cellData.getString("dataField");
        mo_dataGrid.addColumn(o_column);
      }
    }
    
    //==================================================================
    // onDataGridChange
    //==================================================================
    private function onDataGridChange(o_event:Event):void {
      var o_callbacks:I_SvDataGridViewCallbacks = callbacks as I_SvDataGridViewCallbacks;
      if (o_callbacks) {
        o_callbacks.dataGridViewOnChange(this);
      }
    }
  }
}