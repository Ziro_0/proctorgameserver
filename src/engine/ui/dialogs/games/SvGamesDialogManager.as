/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGamesDialogManager

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-15-2018
  ActionScript:  3.0
  Description:  
  History:
    05-15-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.games {
  import engine.I_SvEngine;
  import engine.common.ui.dialogs.DialogManager;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.fillInBlank.SvFillInBlankDialog;
  import engine.ui.dialogs.letters.SvLettersDialog;
  import engine.ui.dialogs.multipleChoice.SvMultChoiceDialog;
  import engine.ui.dialogs.unscrambleWords.SvUnscrambleWordsDialog;
  import flash.display.DisplayObjectContainer;
	
  public class SvGamesDialogManager extends DialogManager {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGamesDialogManager(o_dialogDisplayContainer:DisplayObjectContainer,
    o_callbacks:IDialogManagerCallbacks) {
      super(o_dialogDisplayContainer, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // openFillInBlank
    //==================================================================
    public function openFillInBlank(o_engine:I_SvEngine, s_gameId:String,
    o_settingsData:Object):void {
      openDialog(new SvFillInBlankDialog(FillInBlankDialogDisplay, o_engine, s_gameId,
        SvDialogIds.GAME_FILL_IN_BLANK, o_settingsData, this));
    }
    
    //==================================================================
    // openLetters
    //==================================================================
    public function openLetters(o_engine:I_SvEngine, s_gameId:String, o_settingsData:Object):void {
      openDialog(new SvLettersDialog(LettersDialogDisplay, o_engine, s_gameId,
        SvDialogIds.GAME_LETTERS, o_settingsData, this));
    }
    
    //==================================================================
    // openMultipleChoice
    //==================================================================
    public function openMultipleChoice(o_engine:I_SvEngine, s_gameId:String,
    o_settingsData:Object):void {
      openDialog(new SvMultChoiceDialog(MultipleChoiceDialogDisplay, o_engine, s_gameId,
        SvDialogIds.GAME_MULTIPLE_CHOICE, o_settingsData, this));
    }
    
    //==================================================================
    // openUnscramble
    //==================================================================
    public function openUnscramble(o_engine:I_SvEngine, s_gameId:String, o_settingsData:Object):void {
      openDialog(new SvUnscrambleWordsDialog(UnscrambleDialogDisplay, o_engine, s_gameId,
        SvDialogIds.GAME_UNSCRAMBLE, o_settingsData, this));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}