/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGamesDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-09-2018
  ActionScript:  3.0
  Description:  
  History:
    05-09-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.games {
  import engine.I_SvEngine;
  import engine.common.data.GameIdToTypesMap;
  import engine.common.data.GameTypes;
  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.data.SvDialogData;
  import engine.common.data.GameIds;
  import engine.data.settings.SvMiniGameBaseSettings;
  import engine.data.settings.SvSettingsData;
  import engine.game.I_SvGame;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.dialogs.SvDialogActions;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.game.SvGameBaseDialog;
  import flash.events.TimerEvent;
  import flash.text.TextField;
  import flib.utils.timers.FTimer;
  
  public class SvGamesDialog extends BaseDialog implements IDialogManagerCallbacks,
  I_SvDataGridViewCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const NO_GAME_ENTRIES_MSG_DURATION_MS:Number = 10 * 1000;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_engine:I_SvEngine;
    private var mo_gamesDataGridView:SvGamesDataGridView;
    private var mo_gamesDialogManager:SvGamesDialogManager;
    private var mo_closeButton:Button;
    private var mo_activateButton:Button;
    private var mo_deactivateButton:Button;
    private var mo_startButton:Button;
    private var mo_stopButton:Button;
    private var mo_editButton:Button;
    private var mo_toGameButton:Button;
    private var mo_noGameEntriesTimer:FTimer;
    private var mo_txfSelectedGame:TextField;
    private var mo_txfNoGameEntries:TextField;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGamesDialog(o_engine:I_SvEngine, as_gameIds:Array,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(GamesDialogDisplay, SvDialogIds.GAMES, o_callbacks);
      init(o_engine, as_gameIds);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // data (set)
    //==================================================================
    override public function set data(o_value:Object):void {
      if (o_value is SvDialogData) {
        handleSetDialogData(o_value as SvDialogData);
      } else {
        super.data = o_value;
      }
    }
    
    //==================================================================
    // dataGridViewOnChange
    //==================================================================
    public function dataGridViewOnChange(o_dataGridView:I_SvDataGridView):void {
      updateButtonStates();
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String,
    o_data:Object):void {
    }
    
    //==================================================================
    // gameDialog (get)
    //==================================================================
    public function get gameDialog():SvGameBaseDialog {
      return(mo_gamesDialogManager.dialog as SvGameBaseDialog);
    }
    
    //==================================================================
    // openSelectedMiniGameDialog
    //==================================================================
    public function openSelectedMiniGameDialog():void {
      var s_gameId:String = mo_engine.isGameStarted ?
        mo_engine.activeGameId :
        mo_gamesDataGridView.selectedGameId;
      var s_gameType:String = GameIdToTypesMap.GetGameTypeFromId(s_gameId);
      var o_gameSettings:Object = getGameSettingsData(s_gameId);
      
      switch (s_gameType) {
        case GameTypes.FILL_IN_BLANK:
          mo_gamesDialogManager.openFillInBlank(mo_engine, s_gameId, o_gameSettings);
          break;
          
        case GameTypes.LETTERS:
          mo_gamesDialogManager.openLetters(mo_engine, s_gameId, o_gameSettings);
          break;
        
        case GameTypes.MULTIPLE_CHOICE:
          mo_gamesDialogManager.openMultipleChoice(mo_engine, s_gameId, o_gameSettings);
          break;
        
        case GameTypes.UNSCRAMBLE:
          mo_gamesDialogManager.openUnscramble(mo_engine, s_gameId, o_gameSettings);
          break;
          
        default:
          mo_engine.traceAndLog("SvGamesDialog.openSelectedMiniGameDialog. Warning: Invalid game ID: " +
            s_gameId);
          break;
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // doesSelectedGameHaveEntries
    //==================================================================
    private function doesSelectedGameHaveEntries():Boolean {
      var s_gameId:String = mo_gamesDataGridView.selectedGameId;
      var o_settingsData:SvSettingsData = mo_engine.settingsData;
      var o_gameSettingsData:Object = o_settingsData.games[s_gameId];
      if (o_gameSettingsData == null) {
        return(false);
      }
      
      var o_gameSettings:SvMiniGameBaseSettings = new SvMiniGameBaseSettings(o_gameSettingsData);
      return(o_gameSettings.entries && o_gameSettings.entries.length > 0);
    }
    
    //==================================================================
    // getGameSettingsData
    //==================================================================
    private function getGameSettingsData(s_gameId:String):Object {
      var o_settingsData:SvSettingsData = mo_engine.settingsData;
      var o_gameSettingsData:Object = o_settingsData.games[s_gameId];
      if (o_gameSettingsData == null) {
        o_settingsData.games[s_gameId] = { };
        mo_engine.writeSettings();
      }
      
      return(o_gameSettingsData);
    }
    
    //==================================================================
    // getSelectedGameText
    //==================================================================
    private function getSelectedGameText():String {
      var s_activeGameId:String = mo_engine.activeGameId;
      var s_text:String = s_activeGameId ?
        "Active game: " + s_activeGameId :
        "No game is currently active.";
      return(s_text);
    }
    
    //==================================================================
    // handleSetDialogData
    //==================================================================
    private function handleSetDialogData(o_data:SvDialogData):void {
      switch (o_data.type) {
        case SvDialogData.TYPE_ACTIVE_GAME_CHANGED:
          updateButtonStates();
          updateSelectedGameTextField();
          break;
          
        case SvDialogData.TYPE_WAITING_CLIENTS_CHANGED:
          updateButtonStates();
          break;
      }
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_SvEngine, as_gameIds:Array):void {
      mo_engine = o_engine;
      mo_gamesDialogManager = new SvGamesDialogManager(display, this);
      initGamesDataGrid(as_gameIds);
      initButtons();
      initNoGameEntriesTextField();
      updateButtonStates();
      mo_txfSelectedGame = getTextField("txf_selected_game", getSelectedGameText());
      mo_gamesDataGridView.enabled = mo_engine.activeGameId == null;
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_closeButton = initButton("close_button_display", onCloseClick);
      mo_activateButton = initButton("activate_button_display", onActivateClick);
      mo_deactivateButton = initButton("deactivate_button_display", onDeactivateClick);
      mo_editButton = initButton("edit_button_display", onEditClick);
      mo_startButton = initButton("start_button_display", onStartClick);
      mo_stopButton = initButton("stop_button_display", onStopClick);
      
      mo_toGameButton = initButton("to_game_button_display", onToGameClick);
      mo_toGameButton.visible = false;
    }
    
    //==================================================================
    // initGamesDataGrid
    //==================================================================
    private function initGamesDataGrid(as_gameIds:Array):void {
      mo_gamesDataGridView = new SvGamesDataGridView(getSprite("dg_games_display"), this);
      
      for each (var s_gameId:String in as_gameIds) {
        mo_gamesDataGridView.addGame(s_gameId);
      }
    }
    
    //==================================================================
    // initNoGameEntriesTextField
    //==================================================================
    private function initNoGameEntriesTextField():void {
      mo_txfNoGameEntries = getTextField("txf_no_game_entries");
      mo_txfNoGameEntries.visible = false;
    }
    
    //==================================================================
    // onActivateClick
    //==================================================================
    private function onActivateClick(b_shift:Boolean):void {
      if (!doesSelectedGameHaveEntries()) {
        presentSelectedGameHasNoEntries();
        return;
      }
      
      mo_gamesDataGridView.enabled = false;
      
      var o_callbacks:IBaseDialogCallbacks = callbacks as IBaseDialogCallbacks;
      if (o_callbacks) {
        o_callbacks.baseDialogOnAction(id, SvGamesDialogActions.ACTIVATE_GAME,
          mo_gamesDataGridView.selectedGameId);
      }
    }
    
    //==================================================================
    // onCloseClick
    //==================================================================
    private function onCloseClick(b_shift:Boolean):void {
      complete();
    }
    
    //==================================================================
    // onDeactivateClick
    //==================================================================
    private function onDeactivateClick(b_shift:Boolean):void {
      mo_gamesDataGridView.enabled = true;
      
      var o_callbacks:IBaseDialogCallbacks = callbacks as IBaseDialogCallbacks;
      if (o_callbacks) {
        o_callbacks.baseDialogOnAction(id, SvGamesDialogActions.ACTIVATE_GAME, null);
      }
    }
    
    //==================================================================
    // onEditClick
    //==================================================================
    private function onEditClick(b_shift:Boolean):void {
      openSelectedMiniGameDialog();
    }
    
    //==================================================================
    // onNoGameEntriesTimerComplete
    //==================================================================
    private function onNoGameEntriesTimerComplete(o_event:TimerEvent):void {
      mo_txfSelectedGame.visible = true;
      mo_txfNoGameEntries.visible = false;
      mo_noGameEntriesTimer = null;
    }
    
    //==================================================================
    // onStartClick
    //==================================================================
    private function onStartClick(b_shift:Boolean):void {
      mo_engine.isGameStarted = true;
      updateButtonStates();
    }
    
    //==================================================================
    // onStopClick
    //==================================================================
    private function onStopClick(b_shift:Boolean):void {
      mo_engine.isGameStarted = false;
      updateButtonStates();
      mo_gamesDataGridView.enabled = true;
    }
    
    //==================================================================
    // onToGameClick
    //==================================================================
    private function onToGameClick(b_shift:Boolean):void {
      openSelectedMiniGameDialog();
    }
    
    //==================================================================
    // presentSelectedGameHasNoEntries
    //==================================================================
    private function presentSelectedGameHasNoEntries():void {
      mo_txfSelectedGame.visible = false;
      mo_txfNoGameEntries.visible = true;
      
      if (mo_noGameEntriesTimer) {
        mo_noGameEntriesTimer.restart();
      } else {
        mo_noGameEntriesTimer = new FTimer(NO_GAME_ENTRIES_MSG_DURATION_MS, 1, null,
          onNoGameEntriesTimerComplete);
      }
    }
    
    //==================================================================
    // updateButtonStates
    //==================================================================
    private function updateButtonStates():void {
      var b_isGameStarted:Boolean = mo_engine.isGameStarted;
      var s_activeGameId:String = mo_engine.activeGameId;
      var b_isWaitingForPlayers:Boolean = mo_engine.isWaitingForPlayers();
      
      mo_activateButton.enabled = mo_gamesDataGridView.isItemSelected && !s_activeGameId &&
        !b_isGameStarted && !b_isWaitingForPlayers;
      
      mo_deactivateButton.enabled = s_activeGameId && !b_isGameStarted;
      mo_startButton.enabled = s_activeGameId && !b_isGameStarted && mo_engine.areAllWaitingPlayersReady();
      mo_stopButton.enabled = s_activeGameId && b_isGameStarted;
      
      if (b_isGameStarted) {
        mo_editButton.visible = false;
        mo_toGameButton.visible = true;
      } else {
        mo_editButton.enabled = mo_gamesDataGridView.isItemSelected;
        mo_editButton.visible = true;
        mo_toGameButton.visible = false;
      }
    }
    
    //==================================================================
    // updateSelectedGameTextField
    //==================================================================
    private function updateSelectedGameTextField():void {
      mo_txfSelectedGame.text = getSelectedGameText();
    }
  }
}