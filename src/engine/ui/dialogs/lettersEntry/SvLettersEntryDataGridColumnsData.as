/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvLettersEntryDataGridColumnsData

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          11-12-2018
  ActionScript:  3.0
  Description:  
  History:
    11-12-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.lettersEntry {
  import engine.ui.dialogs.game.SvGameBaseDataGridColumnData;
  import flib.utils.FDataBlock;
  
  internal final class SvLettersEntryDataGridColumnsData {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    internal static const HEADER_DATA:Vector.<SvGameBaseDataGridColumnData> =
    new <SvGameBaseDataGridColumnData> [
      new SvGameBaseDataGridColumnData("Entry", "entry")
    ];
  }
}