/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvLettersEntryDialog

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          11-12-2018
  ActionScript:  3.0
  Description:  
  History:
    11-12-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.lettersEntry {

  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.common.utils.inputLabel.I_InputLabelCallbacks;
  import engine.common.utils.inputLabel.InputLabel;
  import engine.common.utils.inputLabel.InputLabelActions;
  import engine.data.settings.letters.SvLettersEntrySettings;
  import engine.ui.dataGrid.I_SvDataGridView;
  import engine.ui.dataGrid.I_SvDataGridViewCallbacks;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.SvDialogManager;
  import fl.controls.ComboBox;
  import fl.controls.DataGrid;
  import flash.events.Event;
  import flash.events.MouseEvent;
  import flash.text.TextField;
  
  public class SvLettersEntryDialog extends BaseDialog implements IDialogManagerCallbacks,
  I_SvDataGridViewCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: Array
    //  data: String
    private var mao_entries:Array;
    
    private var mo_dialogManager:SvDialogManager;
    
    private var mo_entriesDataGridView:SvLettersEntryDataGridView;
    
    private var mo_addButton:Button;
    private var mo_editButton:Button;
    private var mo_removeButton:Button;
    private var mo_okButton:Button;
    private var mo_cancelButton:Button;
    
    private var mo_cbxLetters:ComboBox;
    
    private var ms_word:String;
    private var mi_existingBeingEditedIndex:int;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvLettersEntryDialog(o_entrySettings:SvLettersEntrySettings,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(LettersEntryDialogDisplay, SvDialogIds.LETTERS_ENTRY, o_callbacks);
      init(o_entrySettings);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dataGridViewOnChange
    //==================================================================
    public function dataGridViewOnChange(o_dataGridView:I_SvDataGridView):void {
      updateUiEnability();
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String,
    o_data:Object):void {
      if (s_dialogAction == DialogActions.COMPLETE) {
        handleDialogWordEntryComplete(o_data as String);
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      uninitLettersComboBox();
      mo_entriesDataGridView.uninit();
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // areEntriesValid
    //==================================================================
    private function areEntriesValid():Boolean {
      return(mao_entries.length > 0);
    }
    
    //==================================================================
    // fillLettersComboBoxContents
    //==================================================================
    private function fillLettersComboBoxContents(s_word:String):void {
      mo_cbxLetters.removeAll();
      
      if (!s_word) {
        return;
      }
      
      for (var i_index:int = 0; i_index < s_word.length; i_index++) {
        var s_char:String = s_word.charAt(i_index);
        mo_cbxLetters.addItem({
          label: s_char,
          data: s_char
        });
      }
      
      mo_cbxLetters.selectedIndex = 0;
    }
    
    //==================================================================
    // handleDialogWordEntryComplete
    //==================================================================
    private function handleDialogWordEntryComplete(s_entry:String):void {
      if (!s_entry) {
        return;
      }
      
      var as_entriesForLetter:Array = mao_entries[mo_cbxLetters.selectedIndex] as Array
      if (!as_entriesForLetter) {
        return;
      }
      
      if (mi_existingBeingEditedIndex == -1) {
        //adding a new word entry
        as_entriesForLetter.push(s_entry);
        mo_entriesDataGridView.addEntry(s_entry);
      } else {
        //editing an existing word entry
        as_entriesForLetter[mi_existingBeingEditedIndex] = s_entry;
        mo_entriesDataGridView.replaceEntryAt(s_entry, mi_existingBeingEditedIndex);
      }
      
      updateUiEnability();
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_entrySettings:SvLettersEntrySettings):void {
      ms_word = o_entrySettings.word;
      
      initDataGrid();
      initButtons();
      initWordText(o_entrySettings);
      initWordEntries(o_entrySettings);
      initLettersComboBox(o_entrySettings);
      
      mo_dialogManager = new SvDialogManager(display, this);
      updateUiEnability();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_addButton = initButton("add_button_display", onAddClick);
      mo_editButton = initButton("edit_button_display", onEditClick);
      mo_removeButton = initButton("remove_button_display", onRemoveClick);
      mo_okButton = initButton("ok_button_display", onOkClick);
      mo_cancelButton = initButton("cancel_button_display", onCancelClick);
    }
    
    //==================================================================
    // initDataGrid
    //==================================================================
    private function initDataGrid():void {
      mo_entriesDataGridView = new SvLettersEntryDataGridView(getSprite("datagrid_display") as DataGrid,
        this);
    }
    
    //==================================================================
    // initLettersComboBox
    //==================================================================
    private function initLettersComboBox(o_entrySettings:SvLettersEntrySettings):void {
      mo_cbxLetters = getSprite("cbx_letters") as ComboBox;
      mo_cbxLetters.addEventListener(Event.CHANGE, onCbxLettersChange, false, 0, true);
      fillLettersComboBoxContents(o_entrySettings ? o_entrySettings.word : null);
      updateWordUiEntries(mo_cbxLetters.selectedIndex);
    }
    
    //==================================================================
    // initLettersComboBoxContents
    //==================================================================
    private function initLettersComboBoxContents(o_entrySettings:SvLettersEntrySettings):void {
      var s_word:String = o_entrySettings ? o_entrySettings.word : null;
      if (!s_word) {
        return;
      }
      
      for (var i_index:int = 0; i_index < s_word.length; i_index++) {
        var s_char:String = s_word.charAt(i_index);
        mo_cbxLetters.addItem({
          label: s_char,
          data: s_char
        });
      }
    }
    
    //==================================================================
    // initWordEntries
    //==================================================================
    private function initWordEntries(o_entrySettings:SvLettersEntrySettings):void {
      var ao_entries:Array = [];
      
      if (o_entrySettings) {
        for each (var as_entriesForLetter:Array in o_entrySettings.entries) {
          var as_entries:Array = as_entriesForLetter.concat();
          ao_entries.push(as_entries);
        }
      }
      
      mao_entries = ao_entries;
    }
    
    //==================================================================
    // initWordText
    //==================================================================
    private function initWordText(o_entrySettings:SvLettersEntrySettings):void {
      var s_defaultText:String = o_entrySettings ? o_entrySettings.word : "";
      getTextField("txf_word", s_defaultText);
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    private function onAddClick(b_shift:Boolean):void {
      mi_existingBeingEditedIndex = -1;
      mo_dialogManager.openSimpleEntry();
    }
    
    //==================================================================
    // onCancelClick
    //==================================================================
    private function onCancelClick(b_shift:Boolean):void {
      cancel();
    }
    
    //==================================================================
    // onCbxLettersChange
    //==================================================================
    private function onCbxLettersChange(o_event:Event):void {
      updateWordUiEntries(mo_cbxLetters.selectedIndex);
    }
    
    //==================================================================
    // onEditClick
    //==================================================================
    private function onEditClick(b_shift:Boolean):void {
      var s_entry:String = getSelectedWordEntry();
      if (s_entry) {
        mi_existingBeingEditedIndex = mo_entriesDataGridView.selectedIndex;
        mo_dialogManager.openSimpleEntry(s_entry);
      }
    }
    
    //==================================================================
    // getSelectedWordEntry
    //==================================================================
    private function getSelectedWordEntry():String {
      var as_entriesForLetter:Array = mao_entries[mo_cbxLetters.selectedIndex] as Array;
      return(as_entriesForLetter ?
        as_entriesForLetter[mo_entriesDataGridView.selectedIndex] as String :
        null);
    }
    
    //==================================================================
    // onOkClick
    //==================================================================
    private function onOkClick(b_shift:Boolean):void {
      var o_completeData:SvLettersEntryDialogCompleteData =
        new SvLettersEntryDialogCompleteData(ms_word, mao_entries);
      complete(o_completeData);
    }
    
    //==================================================================
    // onRemoveClick
    //==================================================================
    private function onRemoveClick(b_shift:Boolean):void {
      var i_selectedLetterIndex:int = mo_cbxLetters.selectedIndex;
      var as_entriesForLetter:Array = mao_entries[i_selectedLetterIndex] as Array;
      if (!as_entriesForLetter) {
        return;
      }
      
      var i_selectedEntryIndex:int = mo_entriesDataGridView.selectedIndex;
      as_entriesForLetter.splice(i_selectedEntryIndex, 1);
      
      mo_entriesDataGridView.removeItemAt(i_selectedEntryIndex);
      updateUiEnability(false);
    }
    
    //==================================================================
    // uninitLettersComboBox
    //==================================================================
    private function uninitLettersComboBox():void {
      mo_cbxLetters.removeEventListener(Event.CHANGE, onCbxLettersChange);
    }
    
    //==================================================================
    // updateUiEnability
    //==================================================================
    private function updateUiEnability(o_bIsItemSelected:Object = null):void {
      var b_isItemSelected:Boolean = o_bIsItemSelected is Boolean ? Boolean(o_bIsItemSelected) :
        mo_entriesDataGridView.selectedIndex > -1;
      mo_editButton.enabled = b_isItemSelected;
      mo_removeButton.enabled = b_isItemSelected;
      
      mo_okButton.enabled = areEntriesValid();
    }
    
    //==================================================================
    // updateWordUiEntries
    //==================================================================
    private function updateWordUiEntries(i_entriesIndex:int):void {
      mo_entriesDataGridView.removeAll();
      
      var as_entriesForLetter:Array = mao_entries[i_entriesIndex] as Array;
      if (!as_entriesForLetter || !as_entriesForLetter.length) {
        return;
      }
      
      for each (var s_entry:String in as_entriesForLetter) {
        mo_entriesDataGridView.addEntry(s_entry);
      }
    }
  }
}