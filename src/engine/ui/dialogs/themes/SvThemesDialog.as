/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvThemesDialog

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          12-17-2018
  ActionScript:  3.0
  Description:  
  History:
    12-17-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.themes {

  import engine.I_SvEngine;
  import engine.common.baseView.BaseView;
  import engine.common.data.CustomThemePath;
  import engine.common.data.ThemeComponentNames;
  import engine.common.ui.buttons.Button;
  import engine.common.ui.dialogs.BaseDialog;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.DialogManager;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.data.settings.SvReadOnlyThemeIds;
  import engine.data.settings.SvThemeSettings;
  import engine.data.settings.SvThemesSettings;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.confirm.SvConfirmDialog;
  import engine.ui.dialogs.customImageSize.SvCustomImageSizeDialog;
  import engine.ui.dialogs.customImageSize.SvCustomImageSizeDialogCompleteData;
  import engine.ui.dialogs.simpleEntry.SvSimpleEntryDialog;
  import fl.controls.ComboBox;
  import fl.data.DataProvider;
  import flash.events.Event;
  import flash.utils.Dictionary;
	
  public class SvThemesDialog extends BaseDialog implements IDialogManagerCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const ADD_NEW_THEME_HEADER_TEXT:String = "Add New Theme";
    private static const CONFIRM_REMOVE_THEME_MESSAGE:String = "Delete this theme?";
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //key: String (theme component name)
    //data: ComboBox
    private var mo_compCmbBxsByCompThemeName:Dictionary;
    
    //key: String (theme component name)
    //data: Button
    private var mo_customButtonsByCompThemeName:Dictionary;
    
    private var mo_themesSettings:SvThemesSettings;
    
    private var mo_readOnlyThemesDataProvider:DataProvider;
    private var mo_themesDataProvider:DataProvider;
    
    private var mo_dialogManager:DialogManager;
    
    private var mo_previewNormView:SvThemesPreviewNormView;
    private var mo_previewBgImageView:SvThemesPreviewBgView;
    
    private var mo_addButton:Button;
    private var mo_removeButton:Button;
    private var mo_okButton:Button;
    private var mo_cancelButton:Button;
    
    private var mo_cbxAvailableThemes:ComboBox;
    private var mo_cbxActiveTheme:ComboBox;
    
    private var ms_activeThemeId:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvThemesDialog(o_engine:I_SvEngine, s_activeThemeId:String, o_themesSettings:Object,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(ThemesDialogDisplay, SvDialogIds.THEMES, o_callbacks);
      init(o_engine, s_activeThemeId, o_themesSettings);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction != DialogActions.COMPLETE) {
        return;
      }
      
      switch (s_dialogId) {
        case SvDialogIds.CONFIRM:
          handleConfirmDialog(o_data);
          break;
          
        case SvDialogIds.SIMPLE_ENTRY:
          handleAddThemeNameSimpleEntryDialog(o_data);
          break;
          
        case SvDialogIds.CUSTOM_IMAGE_SIZE:
          handleCustomImageSizeDialog(o_data);
          break;
      }
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      super.uninit();
      uninitPreview();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addCustomItemToThemeComponentComboBox
    //==================================================================
    private function addCustomItemToThemeComponentComboBox(o_comboBox:ComboBox):void {
      if (!SvThemesDialogDataManager.GetItemById(o_comboBox.dataProvider, CustomThemePath.ID_MARKER)) {
        SvThemesDialogDataManager.AddItem(o_comboBox.dataProvider, CustomThemePath.ID_MARKER);
      }
    }
    
    //==================================================================
    // addNewTheme
    //==================================================================
    private function addNewTheme(s_themeId:String):String {
      var s_themeId:String = createUniqueThemeId(s_themeId);
      addThemeSettings(s_themeId);
      SvThemesDialogDataManager.AddItem(mo_themesDataProvider, s_themeId);
      return(s_themeId);
    }
    
    //==================================================================
    // addThemeSettings
    //==================================================================
    private function addThemeSettings(s_themeId:String):void {
      var o_themeSettings:SvThemeSettings = createThemeSettings(s_themeId);
      mo_themesSettings.add(s_themeId, o_themeSettings);
    }
    
    //==================================================================
    // createCustomThemeComponentImageEntry
    //==================================================================
    private function createCustomThemeComponentImageEntry(s_fileUrl:String):String {
      const ENTRY_PREFIX:String = "custom:";
      return(ENTRY_PREFIX + s_fileUrl);
    }
    
    //==================================================================
    // createReadOnlyThemesDataProvider
    //==================================================================
    private function createReadOnlyThemesDataProvider():DataProvider {
      var o_dataProvider:DataProvider = new DataProvider();
      
      var as_themeIds:Array = mo_themesSettings.getThemeIds();
      for each (var s_themeId:String in as_themeIds) {
        var o_themeSettings:SvThemeSettings = mo_themesSettings.getThemeSettings(s_themeId);
        if (o_themeSettings.isReadOnly) {
          SvThemesDialogDataManager.AddItem(o_dataProvider, s_themeId);
        }
      }
      
      return(o_dataProvider);
    }
    
    //==================================================================
    // createThemeSettings
    //==================================================================
    private function createThemeSettings(s_themeId:String):SvThemeSettings {
      var o_themeSettings:SvThemeSettings = new SvThemeSettings();
      
      o_themeSettings.isReadOnly = false;
      
      var s_modelThemeId:String = getModelThemeId(s_themeId);
      o_themeSettings.resetComponents(s_modelThemeId);
      
      return(o_themeSettings);
    }
    
    //==================================================================
    // createUniqueThemeId
    //==================================================================
    private function createUniqueThemeId(s_originalThemeId:String):String {
      if (!mo_themesSettings.exists(s_originalThemeId)) {
        return(s_originalThemeId);
      }
      
      var u_index:uint = 1;
      var s_nextThemeId:String = s_originalThemeId + u_index;
      while (mo_themesSettings.exists(s_nextThemeId)) {
        u_index++;
        s_nextThemeId = s_originalThemeId + u_index;
      }
      
      return(s_nextThemeId);
    }
    
    //==================================================================
    // deleteSelectedTheme
    //==================================================================
    private function deleteSelectedTheme():void {
      var s_themeId:String = getSelectedThemeId();
      var i_themeIndex:int = SvThemesDialogDataManager.GetItemIndexOf(mo_themesDataProvider, s_themeId);
      
      deleteThemeSettings(s_themeId);
      SvThemesDialogDataManager.RemoveItem(mo_themesDataProvider, s_themeId);
      
      selectAfterDeletedTheme(i_themeIndex);
    }
    
    //==================================================================
    // deleteThemeSettings
    //==================================================================
    private function deleteThemeSettings(s_themeId:String):void {
      mo_themesSettings.remove(s_themeId);
    }
    
    //==================================================================
    // getActiveThemeId
    //==================================================================
    private function getActiveThemeId():String {
      var o_item:Object = mo_cbxActiveTheme.selectedItem;
      return(o_item != null ? o_item.data as String : null);
    }
    
    //==================================================================
    // getFirstReadOnlyThemeId
    //==================================================================
    private function getFirstReadOnlyThemeId():String {
      var o_item:Object = SvThemesDialogDataManager.GetItemAt(mo_readOnlyThemesDataProvider, 0);
      return(o_item != null ? o_item.data as String : null);
    }
    
    //==================================================================
    // getModelThemeId
    //==================================================================
    private function getModelThemeId(s_themeId:String):String {
      var s_firstReadOnlyId:String = getFirstReadOnlyThemeId();
      return(s_firstReadOnlyId || s_themeId);
    }
    
    //==================================================================
    // getSelectedThemeId
    //==================================================================
    private function getSelectedThemeId():String {
      var o_item:Object = SvThemesDialogDataManager.GetItemAt(mo_themesDataProvider, 
        mo_cbxAvailableThemes.selectedIndex);
      return(o_item != null ? String(o_item.data) : null);
    }
    
    //==================================================================
    // getSelectedThemeFromComboBox
    //==================================================================
    private function getSelectedThemeFromComboBox(o_comboBox:ComboBox):String {
      var o_item:Object = o_comboBox.selectedItem;
      return(o_item != null ? String(o_item.data) : null);
    }
    
    //==================================================================
    // getSelectedThemeSettings
    //==================================================================
    private function getSelectedThemeSettings():SvThemeSettings {
      return(mo_themesSettings.getThemeSettings(getSelectedThemeId()));
    }
    
    //==================================================================
    // getThemeCompNameFromCustomButton
    //==================================================================
    private function getThemeCompNameFromCustomButton(o_button:Button):String {
      for (var s_themeComponentName:String in mo_customButtonsByCompThemeName) {
        if (mo_customButtonsByCompThemeName[s_themeComponentName] === o_button) {
          return(s_themeComponentName);
        }
      }
      
      return(null);
    }
    
    //==================================================================
    // getThemeComponentComboBox
    //==================================================================
    private function getThemeComponentComboBox(s_themeComponentName:String):ComboBox {
      return(mo_compCmbBxsByCompThemeName[s_themeComponentName] as ComboBox);
    }
    
    //==================================================================
    // handleAddThemeNameSimpleEntryDialog
    //==================================================================
    private function handleAddThemeNameSimpleEntryDialog(o_data:Object):void {
      var s_themeId:String = addNewTheme(String(o_data));
      selectEditTheme(s_themeId);
    }
    
    //==================================================================
    // handleConfirmDialog
    //==================================================================
    private function handleConfirmDialog(o_data:Object):void {
      var b_isConfirmed:Boolean = Boolean(o_data);
      if (b_isConfirmed) {
        deleteSelectedTheme();
      }
    }
    
    //==================================================================
    // handleCustomImageSizeDialog
    //==================================================================
    private function handleCustomImageSizeDialog(o_data:Object):void {
      var o_sizeDialogData:SvCustomImageSizeDialogCompleteData = o_data as SvCustomImageSizeDialogCompleteData;
      if (!o_sizeDialogData) {
        return;
      }
      
      var s_themeComponentName:String = o_sizeDialogData.themeComponentName;
      var o_themeSettings:SvThemeSettings = getSelectedThemeSettings();
      var o_comboBox:ComboBox = mo_compCmbBxsByCompThemeName[s_themeComponentName] as ComboBox;
      if (!s_themeComponentName || !o_themeSettings || !o_comboBox) {
        return;
      }
      
      const THEME_ID:String = CustomThemePath.ID_MARKER;
      
      if (o_sizeDialogData.areImagesSpecified) {
        addCustomItemToThemeComponentComboBox(o_comboBox);
        o_themeSettings.components[s_themeComponentName] = THEME_ID;
        o_comboBox.selectedIndex = SvThemesDialogDataManager.GetItemIndexOf(o_comboBox.dataProvider,
          THEME_ID);
      } else {
        const DEFAULT_FIRST_READ_ONLY_THEME_ID:String = SvReadOnlyThemeIds.All()[0];
        o_themeSettings.components[s_themeComponentName] = DEFAULT_FIRST_READ_ONLY_THEME_ID;
        
        SvThemesDialogDataManager.RemoveItem(o_comboBox.dataProvider, THEME_ID);
        o_comboBox.selectedIndex = SvThemesDialogDataManager.GetItemIndexOf(o_comboBox.dataProvider,
          DEFAULT_FIRST_READ_ONLY_THEME_ID);
      }
      
      updatePreviewForComponent(s_themeComponentName);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init(o_engine:I_SvEngine, s_activeThemeId:String, o_themesSettingsData:Object):void {
      mo_themesSettings = SvThemesSettings.CloneFromData(o_themesSettingsData);
      mo_themesDataProvider = SvThemesDialogDataManager.Create(mo_themesSettings.getThemeIds());
      mo_readOnlyThemesDataProvider = createReadOnlyThemesDataProvider();
      mo_dialogManager = new DialogManager(display, this);
      
      initButtons();
      initComboBoxes();
      initCustomButtons();
      
      selectEditTheme(s_activeThemeId);
      selectActiveTheme(s_activeThemeId);
      
      initPreviewNodes();
    }
    
    //==================================================================
    // initButtons
    //==================================================================
    private function initButtons():void {
      mo_addButton = initButton2("add_button_display", onAddClicked);
      mo_removeButton = initButton2("remove_button_display", onRemoveClicked);
      mo_okButton = initButton2("ok_button_display", onOkClicked);
      mo_cancelButton = initButton2("cancel_button_display", onCancelClicked);
    }
    
    //==================================================================
    // initComboBox
    //==================================================================
    private function initComboBox(s_displayName:String, o_dataProvider:DataProvider,
    f_onSelectionChanged:Function = null):ComboBox {
      var o_cbx:ComboBox = getSprite(s_displayName) as ComboBox;
      if (!o_cbx) {
        return(null);
      }
      
      o_cbx.dataProvider = o_dataProvider;
      
      if (f_onSelectionChanged is Function) {
        o_cbx.addEventListener(Event.CHANGE, f_onSelectionChanged, false, 0, true);
      }
      
      return(o_cbx);
    }
    
    //==================================================================
    // initComboBoxes
    //==================================================================
    private function initComboBoxes():void {
      mo_cbxAvailableThemes = initThemesComboBox("cbx_themes", onCbxThemesChanged);
      mo_cbxActiveTheme = initThemesComboBox("cbx_active_theme");
      initComponentComboBoxes();
    }
    
    //==================================================================
    // initComponentComboBox
    //==================================================================
    private function initComponentComboBox(s_themeComponentName:String, s_displayName:String,
    f_onSelectionChanged:Function = null):ComboBox {
      var o_comboBox:ComboBox = initComboBox(s_displayName, mo_readOnlyThemesDataProvider.clone(),
        f_onSelectionChanged);
      mo_compCmbBxsByCompThemeName[s_themeComponentName] = o_comboBox;
      return(o_comboBox);
    }
    
    //==================================================================
    // initComponentComboBoxes
    //==================================================================
    private function initComponentComboBoxes():void {
      mo_compCmbBxsByCompThemeName = new Dictionary();
      
      initComponentComboBox(ThemeComponentNames.BACK, "cbx_back", onCbxBackChanged);
      initComponentComboBox(ThemeComponentNames.PANEL, "cbx_panel", onCbxPanelChanged);
      initComponentComboBox(ThemeComponentNames.TITLE, "cbx_title", onCbxTitleChanged);
      initComponentComboBox(ThemeComponentNames.LOBBY_NO_GAME_SELECTED, "cbx_lobbyNoGameSelected",
        onCbxLobbyNoGameSelectedChanged);
      initComponentComboBox(ThemeComponentNames.LOBBY_BABY_GAME_SELECTED, "cbx_lobbyBabyGameSelected",
        onCbxLobbyBabyGameSelectedChanged);
      initComponentComboBox(ThemeComponentNames.LOBBY_BRIDAL_GAME_SELECTED, "cbx_lobbyBridalGameSelected",
        onCbxLobbyBridalGameSelectedChanged);
      initComponentComboBox(ThemeComponentNames.BANNER, "cbx_banners", onCbxBannersChanged);
      initComponentComboBox(ThemeComponentNames.RADIO_BUTTON_OFF, "cbx_radio_button_off",
        onCbxRadioButtonOffChanged);
      initComponentComboBox(ThemeComponentNames.RADIO_BUTTON_ON, "cbx_radio_button_on",
        onCbxRadioButtonOnChanged);
      initComponentComboBox(ThemeComponentNames.RESULTS_BACK, "cbx_radio_button_results_back",
        onCbxRadioButtonResultsBackChanged);
    }
    
    //==================================================================
    // initCustomButtons
    //==================================================================
    private function initCustomButtons():void {
      mo_customButtonsByCompThemeName = new Dictionary();
      
      for each (var s_themeComponentName:String in ThemeComponentNames.All()) {
        var o_button:Button = initButton2("custom_" + s_themeComponentName + "_button_display",
          onCustomButtonClicked);
        if (o_button) {
          mo_customButtonsByCompThemeName[s_themeComponentName] = o_button;
        }
      }
    }
    
    //==================================================================
    // initPreviewNodes
    //==================================================================
    private function initPreviewNodes():void {
      mo_previewNormView = new SvThemesPreviewNormView(getSprite("preview_norm_node"));
      mo_previewBgImageView = new SvThemesPreviewBgView(getSprite("preview_bg_node"));
      updatePreviewNorm();
    }
    
    //==================================================================
    // initThemesComboBox
    //==================================================================
    private function initThemesComboBox(s_displayName:String,
    f_onSelectionChanged:Function = null):ComboBox {
      return(initComboBox(s_displayName, mo_themesDataProvider, f_onSelectionChanged));
    }
    
    //==================================================================
    // isReadOnlyThemeSelected
    //==================================================================
    private function isReadOnlyThemeSelected():Boolean {
      var o_themeSettings:SvThemeSettings = getSelectedThemeSettings();
      return(o_themeSettings && o_themeSettings.isReadOnly);
    }
    
    //==================================================================
    // onAddClicked
    //==================================================================
    private function onAddClicked(o_button:Button, o_event:Event):void {
      openThemeNameDialog();
    }
    
    //==================================================================
    // onCancelClicked
    //==================================================================
    private function onCancelClicked(o_button:Button, o_event:Event):void {
      cancel();
    }
    
    //==================================================================
    // onCbxBackChanged
    //==================================================================
    private function onCbxBackChanged(o_event:Event):void {
      updateComponentOfSelectedTheme(ThemeComponentNames.BACK);
      updatePreviewNorm();
    }
    
    //==================================================================
    // onCbxBannersChanged
    //==================================================================
    private function onCbxBannersChanged(o_event:Event):void {
      updateComponentOfSelectedTheme(ThemeComponentNames.BANNER);
      updatePreviewNorm();
    }
    
    //==================================================================
    // onCbxLobbyBabyGameSelectedChanged
    //==================================================================
    private function onCbxLobbyBabyGameSelectedChanged(o_event:Event):void {
      const THEME_COMP_NAME:String = ThemeComponentNames.LOBBY_BABY_GAME_SELECTED;
      updateComponentOfSelectedTheme(THEME_COMP_NAME);
      updatePreviewBgImage(THEME_COMP_NAME);
    }
    
    //==================================================================
    // onCbxLobbyBridalGameSelectedChanged
    //==================================================================
    private function onCbxLobbyBridalGameSelectedChanged(o_event:Event):void {
      const THEME_COMP_NAME:String = ThemeComponentNames.LOBBY_BRIDAL_GAME_SELECTED;
      updateComponentOfSelectedTheme(THEME_COMP_NAME);
      updatePreviewBgImage(THEME_COMP_NAME);
    }
    
    //==================================================================
    // onCbxLobbyNoGameSelectedChanged
    //==================================================================
    private function onCbxLobbyNoGameSelectedChanged(o_event:Event):void {
      const THEME_COMP_NAME:String = ThemeComponentNames.LOBBY_NO_GAME_SELECTED;
      updateComponentOfSelectedTheme(THEME_COMP_NAME);
      updatePreviewBgImage(THEME_COMP_NAME);
    }
    
    //==================================================================
    // onCbxPanelChanged
    //==================================================================
    private function onCbxPanelChanged(o_event:Event):void {
      updateComponentOfSelectedTheme(ThemeComponentNames.PANEL);
      updatePreviewNorm();
    }
    
    //==================================================================
    // onCbxRadioButtonOffChanged
    //==================================================================
    private function onCbxRadioButtonOffChanged(o_event:Event):void {
      updateComponentOfSelectedTheme(ThemeComponentNames.RADIO_BUTTON_OFF);
      updatePreviewNorm();
    }
    
    //==================================================================
    // onCbxRadioButtonOnChanged
    //==================================================================
    private function onCbxRadioButtonOnChanged(o_event:Event):void {
      updateComponentOfSelectedTheme(ThemeComponentNames.RADIO_BUTTON_ON);
      updatePreviewNorm();
    }
    
    //==================================================================
    // onCbxRadioButtonResultsBackChanged
    //==================================================================
    private function onCbxRadioButtonResultsBackChanged(o_event:Event):void {
      const THEME_COMP_NAME:String = ThemeComponentNames.RESULTS_BACK;
      updateComponentOfSelectedTheme(THEME_COMP_NAME);
      updatePreviewBgImage(THEME_COMP_NAME);
    }
    
    //==================================================================
    // onCbxThemesChanged
    //==================================================================
    private function onCbxThemesChanged(o_event:Event):void {
      updateSelectedTheme();
      updatePreviewNorm();
    }
    
    //==================================================================
    // onCbxTitleChanged
    //==================================================================
    private function onCbxTitleChanged(o_event:Event):void {
      const THEME_COMP_NAME:String = ThemeComponentNames.TITLE;
      updateComponentOfSelectedTheme(THEME_COMP_NAME);
      updatePreviewBgImage(THEME_COMP_NAME);
    }
    
    //==================================================================
    // onCustomButtonClicked
    //==================================================================
    private function onCustomButtonClicked(o_button:Button, o_event:Event):void {
      var s_themeComponentName:String = getThemeCompNameFromCustomButton(o_button);
      if (s_themeComponentName) {
        openCustomImageSizeDialog(getSelectedThemeId(), s_themeComponentName);
      }
    }
    
    //==================================================================
    // onOkClicked
    //==================================================================
    private function onOkClicked(o_button:Button, o_event:Event):void {
      ms_activeThemeId = getActiveThemeId();
      complete(new SvThemesDialogCompleteData(mo_themesSettings, ms_activeThemeId));
    }
    
    //==================================================================
    // onRemoveClicked
    //==================================================================
    private function onRemoveClicked(o_button:Button, o_event:Event):void {
      mo_dialogManager.openDialog(new SvConfirmDialog(CONFIRM_REMOVE_THEME_MESSAGE, mo_dialogManager));
    }
    
    //==================================================================
    // openCustomImageSizeDialog
    //==================================================================
    private function openCustomImageSizeDialog(s_themeId:String, s_themeComponentName:String):void {
      mo_dialogManager.openDialog(
        new SvCustomImageSizeDialog(s_themeId, s_themeComponentName, mo_dialogManager));
    }
    
    //==================================================================
    // openThemeNameDialog
    //==================================================================
    private function openThemeNameDialog():void {
      mo_dialogManager.openDialog(new SvSimpleEntryDialog("", mo_dialogManager, ADD_NEW_THEME_HEADER_TEXT));
    }
    
    //==================================================================
    // selectActiveTheme
    //==================================================================
    private function selectActiveTheme(s_activeThemeId:String):Boolean {
      var i_index:int = SvThemesDialogDataManager.GetItemIndexOf(mo_themesDataProvider, s_activeThemeId);
      if (i_index == -1) {
        if (!selectActiveTheme(getFirstReadOnlyThemeId())) {
          return(false);
        }
      }
      
      ms_activeThemeId = s_activeThemeId;
      mo_cbxActiveTheme.selectedIndex = i_index;
      return(true);
    }
    
    //==================================================================
    // selectEditTheme
    //==================================================================
    private function selectEditTheme(s_themeId:String):void {
      var i_index:int = SvThemesDialogDataManager.GetItemIndexOf(mo_themesDataProvider, s_themeId);
      if (i_index > -1) {
        mo_cbxAvailableThemes.selectedIndex = i_index;
        updateSelectedTheme();
      }
    }
    
    //==================================================================
    // selectAfterDeletedTheme
    //==================================================================
    private function selectAfterDeletedTheme(i_themeIndex:int):void {
      selectAfterDeletedThemeEdited(i_themeIndex);
      selectActiveTheme(ms_activeThemeId);
    }
    
    //==================================================================
    // selectAfterDeletedThemeEdited
    //==================================================================
    private function selectAfterDeletedThemeEdited(i_themeIndex:int):void {
      i_themeIndex = Math.min(i_themeIndex, mo_themesSettings.getThemeIds().length - 1);
      var o_item:Object = SvThemesDialogDataManager.GetItemAt(mo_themesDataProvider, i_themeIndex);
      selectEditTheme(o_item != null ? String(o_item.data) : null);
    }
    
    //==================================================================
    // showPreview
    //==================================================================
    private function showPreview(o_preview:BaseView):void {
      var b_isNormViewVisible:Boolean = o_preview == mo_previewNormView;
      mo_previewNormView.visible = b_isNormViewVisible;
      mo_previewBgImageView.visible = !b_isNormViewVisible;
    }
    
    //==================================================================
    // uninitPreview
    //==================================================================
    private function uninitPreview():void {
      mo_previewNormView.uninit();
      mo_previewBgImageView.uninit();
    }
    
    //==================================================================
    // updateButtons
    //==================================================================
    private function updateButtons():void {
      var b_isRemoveButtonEnabled:Boolean = !isReadOnlyThemeSelected();
      enableView(mo_removeButton, b_isRemoveButtonEnabled);
      enableCustomButtons(b_isRemoveButtonEnabled);
    }
    
    //==================================================================
    // updateComponentComboBox
    //==================================================================
    private function updateComponentComboBox(o_themeSettings:SvThemeSettings,
    s_componentName:String):void {
      var o_comboBox:ComboBox = getThemeComponentComboBox(s_componentName);
      if (!o_comboBox) {
        return;
      }
      
      var s_themeId:String = o_themeSettings.getThemeIdOfComponent(s_componentName);
      
      if (s_themeId == CustomThemePath.ID_MARKER) {
        addCustomItemToThemeComponentComboBox(o_comboBox);
      }
      
      o_comboBox.selectedIndex = SvThemesDialogDataManager.GetItemIndexOf(o_comboBox.dataProvider,
        s_themeId);
      
      o_comboBox.enabled = !o_themeSettings.isReadOnly;
    }
    
    //==================================================================
    // updateComponentComboBoxes
    //==================================================================
    private function updateComponentComboBoxes(o_themeSettings:SvThemeSettings):void {
      for each (var s_themeComponentName:String in ThemeComponentNames.All()) {
        updateComponentComboBox(o_themeSettings, s_themeComponentName);
      }
    }
    
    //==================================================================
    // updateComponentOfSelectedTheme
    //==================================================================
    private function updateComponentOfSelectedTheme(s_themeComponentName:String):void {
      var o_themeSettings:SvThemeSettings = getSelectedThemeSettings();
      if (!o_themeSettings) {
        return;
      }
      
      var o_comboBox:ComboBox = getThemeComponentComboBox(s_themeComponentName);
      if (o_comboBox) {
        var s_themeId:String = getSelectedThemeFromComboBox(o_comboBox);
        o_themeSettings.components[s_themeComponentName] = s_themeId;
      }
    }
    
    //==================================================================
    // enableCustomButtons
    //==================================================================
    private function enableCustomButtons(b_isEnabled:Boolean):void {
      for each (var o_button:Button in mo_customButtonsByCompThemeName) {
        o_button.enabled = b_isEnabled;
      }
    }
    
    //==================================================================
    // updatePreviewBgImage
    //==================================================================
    private function updatePreviewBgImage(s_themeComponentName:String):void {
      var o_themeSettings:SvThemeSettings = getSelectedThemeSettings();
      if (o_themeSettings) {
        mo_previewBgImageView.apply(o_themeSettings, s_themeComponentName);
      } else {
        mo_previewBgImageView.clear();
      }
      
      showPreview(mo_previewBgImageView);
    }
    
    //==================================================================
    // updatePreviewForComponent
    //==================================================================
    private function updatePreviewForComponent(s_themeComponentName:String):void {
      switch (s_themeComponentName) {
        case ThemeComponentNames.LOBBY_BABY_GAME_SELECTED:
        case ThemeComponentNames.LOBBY_BRIDAL_GAME_SELECTED:
        case ThemeComponentNames.LOBBY_NO_GAME_SELECTED:
        case ThemeComponentNames.RESULTS_BACK:
        case ThemeComponentNames.TITLE:
          updatePreviewBgImage(s_themeComponentName);
          break;
          
        default:
          updatePreviewNorm();
          break;
      }
    }
    
    //==================================================================
    // updatePreviewNorm
    //==================================================================
    private function updatePreviewNorm():void {
      var o_themeSettings:SvThemeSettings = getSelectedThemeSettings();
      if (o_themeSettings) {
        mo_previewNormView.apply(o_themeSettings);
      } else {
        mo_previewNormView.clear();
      }
      
      showPreview(mo_previewNormView);
    }
    
    //==================================================================
    // updateSelectedTheme
    //==================================================================
    private function updateSelectedTheme():void {
      var o_themeSettings:SvThemeSettings = getSelectedThemeSettings();
      if (o_themeSettings) {
        updateButtons();
        updateComponentComboBoxes(o_themeSettings);
      }
    }
  }
}