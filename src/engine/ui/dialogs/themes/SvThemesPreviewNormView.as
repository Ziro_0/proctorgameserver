/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvThemesPreviewNormView

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          02-25-2019
  ActionScript:  3.0
  Description:  
  History:
    02-25-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.themes {

  import com.greensock.events.LoaderEvent;
  import com.greensock.loading.ImageLoader;
  import com.greensock.loading.LoaderMax;
  import engine.common.baseView.BaseView;
  import engine.common.data.CustomThemePath;
  import engine.common.data.ThemeComponentNames;
  import engine.data.SvDesignData;
  import engine.data.settings.SvThemeSettings;
  import flash.display.DisplayObject;
  import flash.display.Sprite;
  import flash.filesystem.File;
  import flash.geom.Point;
  import flash.text.TextField;
	
  public final class SvThemesPreviewNormView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const FILE_IMAGE_EXT:String = ".png";
    private static const FILE_ASPECT_RATIO_PART:String = "_3_4";
    private static const BASE_IMAGE_HEIGHT:uint =  2048;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_themesPreviewView:SvThemesPreviewNormView;
    
    private var mo_loaderMax:LoaderMax;
    
    private var mo_txfLoading:TextField;
    private var mo_txfOffButton:TextField;
    private var mo_txfOnButton:TextField;
    
    private var mo_back:Sprite;
    private var mo_banner:Sprite;
    private var mo_panel:Sprite;
    private var mo_radioButtonOn:Sprite;
    private var mo_radioButtonOff:Sprite;
    
    private var mp_size:Point;
    
    private var mn_scale:Number;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvThemesPreviewNormView(o_spriteOrSpriteClass:Object) {
      super(o_spriteOrSpriteClass);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // apply
    //==================================================================
    public function apply(o_themeSettings:SvThemeSettings):Boolean {
      if (!o_themeSettings) {
        clear();
        return(false);
      }
      
      applyBanner(o_themeSettings);
      applyBack(o_themeSettings);
      applyPanel(o_themeSettings);
      applyRadioButtons(o_themeSettings);
      
      beginLoad();
      
      return(true);
    }
    
    //==================================================================
    // clear
    //==================================================================
    public function clear():void {
      const FLUSH_CONTENT:Boolean = true;
      mo_loaderMax.dispose(FLUSH_CONTENT);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      clear();
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // appendTrailingPathDelimiter
    //==================================================================
    private function appendTrailingPathDelimiter(s_path:String):String {
      var i_lastIndex:int = s_path.length - 1;
      if (s_path.lastIndexOf("\\") != i_lastIndex &&
      s_path.lastIndexOf("/") != i_lastIndex) {
        s_path = s_path.concat("/");
      }
      return(s_path);
    }
    
    //==================================================================
    // applyBack
    //==================================================================
    private function applyBack(o_themeSettings:SvThemeSettings):void {
      applyThemeComponentImage(o_themeSettings, ThemeComponentNames.BACK, mo_back);
    }
    
    //==================================================================
    // applyBanner
    //==================================================================
    private function applyBanner(o_themeSettings:SvThemeSettings):void {
      applyThemeComponentImage(o_themeSettings, ThemeComponentNames.BANNER, mo_banner);
    }
    
    //==================================================================
    // applyPanel
    //==================================================================
    private function applyPanel(o_themeSettings:SvThemeSettings):void {
      applyThemeComponentImage(o_themeSettings, ThemeComponentNames.PANEL, mo_panel);
    }
    
    //==================================================================
    // applyRadioButtons
    //==================================================================
    private function applyRadioButtons(o_themeSettings:SvThemeSettings):void {
      applyThemeComponentImage(o_themeSettings, ThemeComponentNames.RADIO_BUTTON_ON,
        mo_radioButtonOn);
      applyThemeComponentImage(o_themeSettings, ThemeComponentNames.RADIO_BUTTON_OFF,
        mo_radioButtonOff);
    }
    
    //==================================================================
    // applyThemeComponentImage
    //==================================================================
    private function applyThemeComponentImage(o_themeSettings:SvThemeSettings,
    s_componentName:String, o_containerNode:Sprite):void {
      var s_componentThemeId:String = o_themeSettings.getThemeIdOfComponent(s_componentName);
      
      var o_vars:Object = {
        //greensock properties
        container: o_containerNode,
        onComplete: onImageLoaderComplete,
        
        //custom properties
        tempName: s_componentName
      };
      
      var s_url:String = getThemeCompnentUrl(s_componentName, o_themeSettings.id, s_componentThemeId);
      mo_loaderMax.append(new ImageLoader(s_url, o_vars));
    }
    
    //==================================================================
    // beginLoad
    //==================================================================
    private function beginLoad():void {
      mo_loaderMax.load();
      mo_txfLoading.visible = true;
    }
    
    //==================================================================
    // createBasePath
    //==================================================================
    private function createBasePath(b_isCustom:Boolean):String {
      var o_file:File;
      
      if (b_isCustom) {
        o_file = File.applicationStorageDirectory.resolvePath(CustomThemePath.CUSTOM_BASE_PATH);
      } else {
        o_file = File.applicationDirectory.resolvePath(SvDesignData.THEMES_BASE_URL);
      }
      
      var s_path:String = appendTrailingPathDelimiter(o_file.url);
      return(s_path);
    }
    
    //==================================================================
    // createImageLoaderUrlPath
    //==================================================================
    private function createImageLoaderUrlPath(s_componentThemeId:String, s_themeId:String,
    s_themeComponentName:String):String {
      var b_isCustom:Boolean = s_componentThemeId == CustomThemePath.ID_MARKER;
      const BASE_PATH:String = createBasePath(b_isCustom);
      const THEME_COMPONENT_NAME_PATH:String = s_themeComponentName + "/";
      
      var s_themeIdPath:String;
      if (b_isCustom) {
        s_themeIdPath = s_themeId + "/";
      } else {
        s_themeIdPath = s_componentThemeId + "/";
      }
      
      return(BASE_PATH + s_themeIdPath + THEME_COMPONENT_NAME_PATH);
    }
    
    //==================================================================
    // getThemeCompnentUrl
    //==================================================================
    private function getThemeCompnentUrl(s_componentName:String, s_themeId:String,
    s_componentThemeId:String):String {
      var s_path:String = createImageLoaderUrlPath(s_componentThemeId, s_themeId, s_componentName);
      return(s_path + s_componentName + FILE_ASPECT_RATIO_PART + FILE_IMAGE_EXT);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      initAreaSize();
      initBannerContainer();
      initPanelContainer();
      initBackContainer();
      initRadioButtonContainers();
      initTexts();
      
      initLoader();
    }
    
    //==================================================================
    // initAreaSize
    //==================================================================
    private function initAreaSize():void {
      var o_node:Sprite = getSprite("area_size");
      mp_size = new Point(o_node.width, o_node.height);
      display.removeChild(o_node);
      mn_scale = mp_size.y / BASE_IMAGE_HEIGHT;
    }
    
    //==================================================================
    // initBackContainer
    //==================================================================
    private function initBackContainer():void {
      mo_back = initContainerNode("back_node");
    }
    
    //==================================================================
    // initBannerContainer
    //==================================================================
    private function initBannerContainer():void {
      mo_banner = initContainerNode("banner_node");
    }
    
    //==================================================================
    // initContainerNode
    //==================================================================
    private function initContainerNode(s_nodeName:String):Sprite {
      var o_node:Sprite = getSprite(s_nodeName);
      if (o_node) {
        o_node.removeChildren();
      }
      return(o_node);
    }
    
    //==================================================================
    // initLoader
    //==================================================================
    private function initLoader():void {
      var o_vars:Object = {
        onComplete: onLoaderMaxComplete
      };
      
      mo_loaderMax = new LoaderMax(o_vars);
    }
    
    //==================================================================
    // initPanelContainer
    //==================================================================
    private function initPanelContainer():void {
      mo_panel = initContainerNode("panel_node");
    }
    
    //==================================================================
    // initRadioButtonContainers
    //==================================================================
    private function initRadioButtonContainers():void {
      mo_radioButtonOff = initContainerNode("radio_button_off_node");
      mo_radioButtonOn = initContainerNode("radio_button_on_node");
    }
    
    //==================================================================
    // initText
    //==================================================================
    private function initText(s_txfName:String, b_visible:Boolean = false):TextField {
      var o_txf:TextField = getTextField(s_txfName);
      if (o_txf) {
        o_txf.visible = b_visible;
      }
      return(o_txf);
    }
    
    //==================================================================
    // initTexts
    //==================================================================
    private function initTexts():void {
      mo_txfLoading = initText("txf_loading");
      mo_txfOffButton = initText("txf_off_button");
      mo_txfOnButton = initText("txf_on_button");
    }
    
    //==================================================================
    // onImageLoaderComplete
    //==================================================================
    private function onImageLoaderComplete(o_event:LoaderEvent):void {
      var o_loader:ImageLoader = o_event.currentTarget as ImageLoader;
      
      removeExistingImage(o_loader);
      resizeImage(o_loader);
      o_loader.name = o_loader.vars.tempName;
    }
    
    //==================================================================
    // onLoaderMaxComplete
    //==================================================================
    private function onLoaderMaxComplete(o_event:LoaderEvent):void {
      mo_txfLoading.visible = false;
      mo_txfOffButton.visible = mo_txfOnButton.visible = true;
    }
    
    //==================================================================
    // removeExistingImage
    //==================================================================
    private function removeExistingImage(o_loader:ImageLoader):void {
      var s_componentName:String = o_loader.vars.tempName as String;
      
      var o_existingLoader:ImageLoader = mo_loaderMax.getLoader(s_componentName) as ImageLoader;
      if (!o_existingLoader) {
        return;
      }
      
      mo_loaderMax.remove(o_existingLoader);
      
      const FLUSH_CONTENT:Boolean = true;
      o_existingLoader.dispose(FLUSH_CONTENT);
    }
    
    //==================================================================
    // resizeImage
    //==================================================================
    private function resizeImage(o_loader:ImageLoader):void {
      var o_image:DisplayObject = o_loader.rawContent as DisplayObject;
      o_image.scaleX = o_image.scaleY = mn_scale;
    }
  }
}