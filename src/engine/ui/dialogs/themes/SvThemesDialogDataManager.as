/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvThemesDialogDataManager

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          12-23-2018
  ActionScript:  3.0
  Description:  
  History:
    12-23-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.themes {
  import engine.data.settings.SvReadOnlyThemeIds;
  import engine.data.settings.SvThemesSettings;
  import fl.data.DataProvider;

  internal final class SvThemesDialogDataManager {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    //private var mo_dataProvider:DataProvider;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    /*
    public function SvThemesDialogDataManager(as_themeIds:Array = null) {
      super();
      init(as_themeIds);
    }
    */
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // AddItem
    //==================================================================
    public static function AddItem(o_dataProvider:DataProvider, s_themeId:String):void {
      if (o_dataProvider && s_themeId) {
        o_dataProvider.addItem({
          data: s_themeId,
          label: s_themeId
        });
      }
    }
    
    //==================================================================
    // Create
    //==================================================================
    public static function Create(as_themeIds:Array = null):DataProvider {
      var o_dataProvider:DataProvider = new DataProvider();
      
      for each (var s_themeId:String in as_themeIds) {
        AddItem(o_dataProvider, s_themeId);
      }
      
      return(o_dataProvider);
    }
    
    /*
    //==================================================================
    // clone
    //==================================================================
    public function clone():SvThemesDialogDataManager {
      var o_target:SvThemesDialogDataManager = new SvThemesDialogDataManager();
      o_target.mo_dataProvider = mo_dataProvider.clone();
      return(o_target);
    }
    
    //==================================================================
    // dataProvider (get)
    //==================================================================
    public function get dataProvider():DataProvider {
      return(mo_dataProvider);
    }
    */
    
    //==================================================================
    // GetItemAt
    //==================================================================
    public static function GetItemAt(o_dataProvider:DataProvider, i_index:int):Object {
      return(o_dataProvider ? o_dataProvider.getItemAt(i_index) : null);
    }
    
    //==================================================================
    // GetItemById
    //==================================================================
    public static function GetItemById(o_dataProvider:DataProvider, s_themeId:String):Object {
      var i_index:int = GetItemIndexOf(o_dataProvider, s_themeId);
      return(i_index > -1 ? o_dataProvider.getItemIndex(i_index) : null);
    }
    
    //==================================================================
    // GetItemIndexOf
    //==================================================================
    public static function GetItemIndexOf(o_dataProvider:DataProvider, s_themeId:String):int {
      if (!o_dataProvider) {
        return( -1);
      }
      
      for (var u_index:uint = 0; u_index < o_dataProvider.length; u_index++) {
        var o_item:Object = o_dataProvider.getItemAt(u_index);
        if (String(o_item.data) == s_themeId) {
          return(u_index);
        }
      }
      
      return(-1);
    }
    
    //==================================================================
    // RemoveItem
    //==================================================================
    public static function RemoveItem(o_dataProvider:DataProvider, s_themeId:String):void {
      if (!o_dataProvider) {
        return;
      }
      
      var i_index:int = GetItemIndexOf(o_dataProvider, s_themeId);
      if (i_index > -1) {
        o_dataProvider.removeItemAt(i_index);
      }
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
   
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    /*
    //==================================================================
    // init
    //==================================================================
    private function init(as_themeIds:Array):void {
      mo_dataProvider = new DataProvider();
      
      for each (var s_themeId:String in as_themeIds) {
        addItem(s_themeId);
      }
    }
    */
  }
}