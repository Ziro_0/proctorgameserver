/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvThemesPreviewBgView

  Author:        Cartrell (Ziro)
    http://www.gameplaycoder.com
  Date:          04-25-2019
  ActionScript:  3.0
  Description:  
  History:
    02-45-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.themes {

  import com.greensock.events.LoaderEvent;
  import com.greensock.loading.ImageLoader;
  import com.greensock.loading.LoaderMax;
  import engine.common.baseView.BaseView;
  import engine.common.data.CustomThemePath;
  import engine.common.data.ThemeComponentNames;
  import engine.data.SvDesignData;
  import engine.data.settings.SvThemeSettings;
  import flash.display.DisplayObject;
  import flash.display.Sprite;
  import flash.filesystem.File;
  import flash.geom.Point;
  import flash.text.TextField;
	
  public final class SvThemesPreviewBgView extends BaseView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const FILE_IMAGE_EXT:String = ".png";
    private static const FILE_ASPECT_RATIO_PART:String = "_3_4";
    private static const BASE_IMAGE_HEIGHT:uint =  2048;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_themesPreviewView:SvThemesPreviewBgView;
    
    private var mo_loaderMax:LoaderMax;
    
    private var mo_txfLoading:TextField;
    
    private var mo_bgImage:Sprite;
    
    private var mp_size:Point;
    
    private var mn_scale:Number;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvThemesPreviewBgView(o_spriteOrSpriteClass:Object) {
      super(o_spriteOrSpriteClass);
			init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // apply
    //==================================================================
    public function apply(o_themeSettings:SvThemeSettings, s_themeComponentName:String):Boolean {
      if (!o_themeSettings) {
        clear();
        return(false);
      }
      
      applyBgImage(o_themeSettings, s_themeComponentName);
      beginLoad();
      return(true);
    }
    
    //==================================================================
    // clear
    //==================================================================
    public function clear():void {
      const FLUSH_CONTENT:Boolean = true;
      mo_loaderMax.dispose(FLUSH_CONTENT);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    override public function uninit():void {
      clear();
      super.uninit();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // appendTrailingPathDelimiter
    //==================================================================
    private function appendTrailingPathDelimiter(s_path:String):String {
      var i_lastIndex:int = s_path.length - 1;
      if (s_path.lastIndexOf("\\") != i_lastIndex &&
      s_path.lastIndexOf("/") != i_lastIndex) {
        s_path = s_path.concat("/");
      }
      return(s_path);
    }
    
    //==================================================================
    // applyBgImage
    //==================================================================
    private function applyBgImage(o_themeSettings:SvThemeSettings, s_themeComponentName:String):void {
      applyThemeComponentImage(o_themeSettings, s_themeComponentName, mo_bgImage);
    }
    
    //==================================================================
    // applyThemeComponentImage
    //==================================================================
    private function applyThemeComponentImage(o_themeSettings:SvThemeSettings,
    s_componentName:String, o_containerNode:Sprite):void {
      var s_componentThemeId:String = o_themeSettings.getThemeIdOfComponent(s_componentName);
      
      var o_vars:Object = {
        //greensock properties
        container: o_containerNode,
        onComplete: onImageLoaderComplete,
        
        //custom properties
        tempName: s_componentName
      };
      
      var s_url:String = getThemeCompnentUrl(s_componentName, o_themeSettings.id, s_componentThemeId);
      mo_loaderMax.append(new ImageLoader(s_url, o_vars));
    }
    
    //==================================================================
    // beginLoad
    //==================================================================
    private function beginLoad():void {
      mo_loaderMax.load();
      mo_txfLoading.visible = true;
    }
    
    //==================================================================
    // createBasePath
    //==================================================================
    private function createBasePath(b_isCustom:Boolean):String {
      var o_file:File;
      
      if (b_isCustom) {
        o_file = File.applicationStorageDirectory.resolvePath(CustomThemePath.CUSTOM_BASE_PATH);
      } else {
        o_file = File.applicationDirectory.resolvePath(SvDesignData.THEMES_BASE_URL);
      }
      
      var s_path:String = appendTrailingPathDelimiter(o_file.url);
      return(s_path);
    }
    
    //==================================================================
    // createImageLoaderUrlPath
    //==================================================================
    private function createImageLoaderUrlPath(s_componentThemeId:String, s_themeId:String,
    s_themeComponentName:String):String {
      var b_isCustom:Boolean = s_componentThemeId == CustomThemePath.ID_MARKER;
      const BASE_PATH:String = createBasePath(b_isCustom);
      const THEME_COMPONENT_NAME_PATH:String = s_themeComponentName + "/";
      
      var s_themeIdPath:String;
      if (b_isCustom) {
        s_themeIdPath = s_themeId + "/";
      } else {
        s_themeIdPath = s_componentThemeId + "/";
      }
      
      return(BASE_PATH + s_themeIdPath + THEME_COMPONENT_NAME_PATH);
    }
    
    //==================================================================
    // getThemeCompnentUrl
    //==================================================================
    private function getThemeCompnentUrl(s_componentName:String, s_themeId:String,
    s_componentThemeId:String):String {
      var s_path:String = createImageLoaderUrlPath(s_componentThemeId, s_themeId, s_componentName);
      return(s_path + s_componentName + FILE_ASPECT_RATIO_PART + FILE_IMAGE_EXT);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      initAreaSize();
      initBgImageContainer();
      initTexts();
      
      initLoader();
    }
    
    //==================================================================
    // initAreaSize
    //==================================================================
    private function initAreaSize():void {
      var o_node:Sprite = getSprite("area_size");
      mp_size = new Point(o_node.width, o_node.height);
      display.removeChild(o_node);
      mn_scale = mp_size.y / BASE_IMAGE_HEIGHT;
    }
    
    //==================================================================
    // initBgImageContainer
    //==================================================================
    private function initBgImageContainer():void {
      mo_bgImage = initContainerNode("bg_image_node");
    }
    
    //==================================================================
    // initContainerNode
    //==================================================================
    private function initContainerNode(s_nodeName:String):Sprite {
      var o_node:Sprite = getSprite(s_nodeName);
      if (o_node) {
        o_node.removeChildren();
      }
      return(o_node);
    }
    
    //==================================================================
    // initLoader
    //==================================================================
    private function initLoader():void {
      var o_vars:Object = {
        onComplete: onLoaderMaxComplete
      };
      
      mo_loaderMax = new LoaderMax(o_vars);
    }
    
    //==================================================================
    // initText
    //==================================================================
    private function initText(s_txfName:String, b_visible:Boolean = false):TextField {
      var o_txf:TextField = getTextField(s_txfName);
      if (o_txf) {
        o_txf.visible = b_visible;
      }
      return(o_txf);
    }
    
    //==================================================================
    // initTexts
    //==================================================================
    private function initTexts():void {
      mo_txfLoading = initText("txf_loading");
    }
    
    //==================================================================
    // onImageLoaderComplete
    //==================================================================
    private function onImageLoaderComplete(o_event:LoaderEvent):void {
      var o_loader:ImageLoader = o_event.currentTarget as ImageLoader;
      
      removeExistingImage(o_loader);
      resizeImage(o_loader);
      o_loader.name = o_loader.vars.tempName;
    }
    
    //==================================================================
    // onLoaderMaxComplete
    //==================================================================
    private function onLoaderMaxComplete(o_event:LoaderEvent):void {
      mo_txfLoading.visible = false;
    }
    
    //==================================================================
    // removeExistingImage
    //==================================================================
    private function removeExistingImage(o_loader:ImageLoader):void {
      var s_componentName:String = o_loader.vars.tempName as String;
      
      var o_existingLoader:ImageLoader = mo_loaderMax.getLoader(s_componentName) as ImageLoader;
      if (!o_existingLoader) {
        return;
      }
      
      mo_loaderMax.remove(o_existingLoader);
      
      const FLUSH_CONTENT:Boolean = true;
      o_existingLoader.dispose(FLUSH_CONTENT);
    }
    
    //==================================================================
    // resizeImage
    //==================================================================
    private function resizeImage(o_loader:ImageLoader):void {
      var o_image:DisplayObject = o_loader.rawContent as DisplayObject;
      o_image.scaleX = o_image.scaleY = mn_scale;
    }
  }
}