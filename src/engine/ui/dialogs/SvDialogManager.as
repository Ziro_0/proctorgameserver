/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvDialogManager

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          04-18-2018
  ActionScript:  3.0
  Description:  
  History:
    04-18-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs {
  import engine.common.utils.AppDescriptorData;
  import engine.ui.dialogs.message.SvMessageDialog;
  import engine.ui.dialogs.settingsFile.SvSettingsFileDialog;
  import flash.filesystem.File;
  import engine.I_SvEngine;
  import engine.SvEngine;
  import engine.common.ui.dialogs.DialogManager;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.ui.dialogs.games.SvGamesDialog;
  import engine.ui.dialogs.multipleChoiceQuestionOption.SvMultChoiceQuestionOptionDialog;
  import engine.ui.dialogs.simpleEntry.SvSimpleEntryDialog;
  import engine.ui.dialogs.themes.SvThemesDialog;
  import flash.display.DisplayObjectContainer;
  
  public class SvDialogManager extends DialogManager {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvDialogManager(o_dialogDisplayContainer:DisplayObjectContainer,
    o_callbacks:IDialogManagerCallbacks) {
      super(o_dialogDisplayContainer, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // openGames
    //==================================================================
    public function openGames(o_engine:I_SvEngine, as_gameIds:Array):void {
      openDialog(new SvGamesDialog(o_engine, as_gameIds, this));
    }
    
    //==================================================================
    // openMessage
    //==================================================================
    public function openMessage(s_message:String, s_header:String = null,
    s_dialogId:String = null):void {
      const HEADER:String = s_header || AppDescriptorData.Name;
      const DIALOG_ID:String = s_dialogId || SvDialogIds.MESSAGE;
      openDialog(new SvMessageDialog(HEADER, s_message, this, DIALOG_ID));
    }
    
    //==================================================================
    // openMultChoiceQuestionOption
    //==================================================================
    public function openMultChoiceQuestionOption(s_inputText:String, b_isCorrect:Boolean,
    b_isCorrectToggleEnabled:Boolean):void {
      openDialog(new SvMultChoiceQuestionOptionDialog(s_inputText, b_isCorrect,
        b_isCorrectToggleEnabled, this));
    }
    
    //==================================================================
    // openSettingsFile
    //==================================================================
    public function openSettingsFile(o_engine:I_SvEngine, o_file:File):void {
      openDialog(new SvSettingsFileDialog(o_engine, o_file, this));
    }
    
    //==================================================================
    // openSimpleEntry
    //==================================================================
    public function openSimpleEntry(s_defaultText:String = ""):void {
      openDialog(new SvSimpleEntryDialog(s_defaultText, this));
    }
    
    //==================================================================
    // openThemes
    //==================================================================
    public function openThemes(o_engine:I_SvEngine, s_activeThemeId:String,
    o_themesSettings:Object):void {
      openDialog(new SvThemesDialog(o_engine, s_activeThemeId, o_themesSettings, this));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}