/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvUnscrambleWordsDialog

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          05-15-2018
  ActionScript:  3.0
  Description:  
  History:
    05-15-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.unscrambleWords {
  import engine.I_SvEngine;
  import engine.common.ui.dialogs.IBaseDialogCallbacks;
  import engine.data.settings.SvMiniGameBaseEntrySettings;
  import engine.data.settings.scrambleWords.SvScrambleWordEntrySettings;
  import engine.ui.dialogs.game.SvGameBaseDialog;
  import engine.ui.dialogs.simpleEntry.SvSimpleEntryDialog;
  import engine.utils.SvUnscrambleUtils;
	
  public class SvUnscrambleWordsDialog extends SvGameBaseDialog {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvUnscrambleWordsDialog(o_spriteOrSpriteClass:Object, o_engine:I_SvEngine,
    s_gameId:String, s_dialogId:String, o_settingsData:Object,
    o_callbacks:IBaseDialogCallbacks = null) {
      super(o_spriteOrSpriteClass, o_engine, s_gameId, s_dialogId, o_settingsData, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // handleDialogActionSimpleEntry
    //==================================================================
    override protected function handleDialogActionSimpleEntry(o_data:Object):void {
      addWordEntry(o_data as String);
    }
    
    //==================================================================
    // initContentDataGrid
    //==================================================================
    override protected function initContentDataGrid():void {
      mo_contentDataGrid = new SvUnscrambleDataGridView(getSprite("dg_content_display"), this);
      super.initContentDataGrid();
    }
    
    //==================================================================
    // onAddClick
    //==================================================================
    override protected function onAddClick(b_shift:Boolean):void {
      mo_dialogManager.openDialog(new SvSimpleEntryDialog(null, mo_dialogManager));
    }
    
    //==================================================================
    // onCreateBaseEntrySettings
    //==================================================================
    override protected function onCreateBaseEntrySettings(o_entryData:Object):SvMiniGameBaseEntrySettings {
      return(new SvScrambleWordEntrySettings(o_entryData));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addWordEntry
    //==================================================================
    private function addWordEntry(s_word:String):void {
      var o_utils:SvUnscrambleUtils = new SvUnscrambleUtils();
      var o_entry:SvScrambleWordEntrySettings = o_utils.createWordEntry(s_word);
      if (o_entry) {
        addEntry(o_entry);
      } else {
        mo_engine.traceAndLog("SvUnscrambleWordsDialog.addWordEntry. Warning: Unable to add word '" + s_word + "'.");
      }
    }
  }
}