/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvUnscrambleDataGridView

  Author:        Cartrell (Ziro)
    https://gameplaycoder.com
  Date:          05-16-2018
  ActionScript:  3.0
  Description:  
  History:
    05-16-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.ui.dialogs.unscrambleWords {
  import engine.common.baseView.I_BaseViewCallbacks;
  import engine.data.settings.scrambleWords.SvScrambleWordEntrySettings;
  import engine.ui.dialogs.game.SvGameBaseDataGridItem;
  import engine.ui.dialogs.game.SvGameBaseDataGridView;
	
  internal class SvUnscrambleDataGridView extends SvGameBaseDataGridView {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvUnscrambleDataGridView(o_spriteOrSpriteClass:Object,
    o_callbacks:I_BaseViewCallbacks = null) {
      super(o_spriteOrSpriteClass, SvUnscrambleDataGridColumnsData.HEADER_DATA, o_callbacks);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // onCreateDataGridItem
    //==================================================================
    override protected function onCreateDataGridItem(o_entryData:Object):SvGameBaseDataGridItem {
      var o_entrySettings:SvScrambleWordEntrySettings = new SvScrambleWordEntrySettings(o_entryData);
      return(o_entrySettings ? new SvUnscrambleDataItem(o_entrySettings.word, o_entrySettings.scramble) : null);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
  }
}