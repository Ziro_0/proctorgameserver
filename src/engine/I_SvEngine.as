/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvEngine

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-26-2017
  ActionScript:	3.0
  Description:	
  History:
    05-26-2017:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine {
  import engine.data.settings.SvSettingsData;
  import engine.game.SvGame;
  import engine.ui.dialogs.SvDialogManager;
  import engine.ui.mainView.SvMainView;
  import flib.utils.FDataBlock;
  import net.server.NetServer;
  
  ////////////////////////////////////////////////////////////////////
  //==================================================================
  // interface
  //==================================================================
  ////////////////////////////////////////////////////////////////////
  public interface I_SvEngine {
    function get activeGameId():String;
    function set activeGameId(s_value:String):void;
    function areAllWaitingPlayersReady():Boolean;
    function get dialogManager():SvDialogManager;
    function get game():SvGame;
    function getActiveClientIds():Array;
    function getClientIds():Array;
    function getPlayerState(s_clientId:String):FDataBlock;
    function get isGameStarted():Boolean;
    function set isGameStarted(b_value:Boolean):void;
    function isValidClientId(s_clientId:String):Boolean;
    function isWaitingForPlayers():Boolean;
    function get mainView():SvMainView;
    function get netServer():NetServer;
    function sendMessageToClient(s_clientId:String, o_message:Object):void;
    function sendMessageToClients(o_message:Object):void;
    function sendStateToClient(s_clientId:String):void;
    function sendStateToClients():void;
    function get settingsData():SvSettingsData;
    function get state():FDataBlock;
    function traceAndLog(s_message:String, o_bUseDateTime:Object = null):void;
    function writeSettings():Boolean;
  }
}