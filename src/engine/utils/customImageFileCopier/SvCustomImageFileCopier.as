/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvCustomImageFileCopier

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          07-03-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.customImageFileCopier {
  import flash.events.Event;
  import flash.events.IOErrorEvent;
  import flash.filesystem.File;

  public final class SvCustomImageFileCopier {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private static const COPY_ERROR_MSG:String = "Error: Unable to copy file. The file you are " +
      "trying to copy does not exist, or the file cannot be copied because it is already open by an app."
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    private var mas_errorFileUrls:Array;
    
    //data: SvCustomImageFileCopyData
    private var mao_fileUrls:Array;
    
    private var mo_callbacks:I_SvCustomImageFileCopierCallbacks;
    
    private var mo_sourceCopyFile:File;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvCustomImageFileCopier(o_callbacks:I_SvCustomImageFileCopierCallbacks = null) {
      super();
      callbacks = o_callbacks;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_SvCustomImageFileCopierCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_SvCustomImageFileCopierCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // cancel
    //==================================================================
    public function cancel():void {
      mao_fileUrls = null;
      uninitCopyFiles();
    }
    
    //==================================================================
    // copyFiles
    //==================================================================
    public function copyFiles(ao_fileUrls:Array):Boolean {
      if (!ao_fileUrls || !ao_fileUrls.length) {
        return(false);
      }
      
      mas_errorFileUrls = null;
      mao_fileUrls = ao_fileUrls.concat();
      return(copyNextFile());
    }
    
    //==================================================================
    // errorFileUrls (get)
    //==================================================================
    public function get errorFileUrls():Array {
      return(mas_errorFileUrls);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      callbacks = null;
      cancel();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addErrorFileUrl
    //==================================================================
    private function addErrorFileUrl(s_url:String):void {
      if (!mas_errorFileUrls) {
        mas_errorFileUrls = [];
      }
      
      mas_errorFileUrls.push(s_url);
    }
    
    //==================================================================
    // complete
    //==================================================================
    private function complete():void {
      uninitCopyFiles();
      mao_fileUrls = null;
      
      if (mo_callbacks) {
        mo_callbacks.fileCopierOnComplete(this);
      }
    }
    
    //==================================================================
    // copyFile
    //==================================================================
    private function copyFile(o_copyData:SvCustomImageFileCopyData):Boolean {
      var o_targetCopyFile:File = File.applicationStorageDirectory.resolvePath(o_copyData.targetFileUrl);
      
      mo_sourceCopyFile = new File(o_copyData.sourceFileUrl);
      mo_sourceCopyFile.addEventListener(Event.COMPLETE, onFileCopyComplete, false, 0, true);
      mo_sourceCopyFile.addEventListener(IOErrorEvent.IO_ERROR, onFileCopyError, false, 0, true);
      
      var o_excError:Error = null;
      try {
        mo_sourceCopyFile.copyToAsync(o_targetCopyFile, true);
      } catch (o_error:Error) {
        o_excError = o_error;
        trace("SvCustomImageFileCopier.copyFileToCustomFilesDirectory. " + COPY_ERROR_MSG);
        addErrorFileUrl(mo_sourceCopyFile.url);
        uninitCopyFiles();
      }
      
      if (o_excError) {
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // copyNextFile
    //==================================================================
    private function copyNextFile():Boolean {
      if (!mao_fileUrls.length) {
        return(false);
      }
      
      var o_copyData:SvCustomImageFileCopyData = mao_fileUrls.pop() as SvCustomImageFileCopyData;
      if (!o_copyData) {
        return(copyNextFile());
      }
      
      if (!copyFile(o_copyData)) {
        return(copyNextFile());
      }
      
      return(true);
    }
    
    //==================================================================
    // onFileCopyComplete
    //==================================================================
    private function onFileCopyComplete(o_event:Event):void {
      uninitCopyFiles();
      
      if (mao_fileUrls.length) {
        copyNextFile();
      } else {
        complete();
      }
    }
    
    //==================================================================
    // onFileCopyError
    //==================================================================
    private function onFileCopyError(o_event:IOErrorEvent):void {
      trace("SvCustomImageFileCopier.onFileCopyError. Error: unable to copy file. " + o_event);
      addErrorFileUrl(mo_sourceCopyFile.url);
      uninitCopyFiles();
      
      if (mao_fileUrls.length) {
        copyNextFile();
      } else {
        complete();
      }
    }
    
    //==================================================================
    // uninitCopyFiles
    //==================================================================
    private function uninitCopyFiles():void {
      if (!mo_sourceCopyFile) {
        return;
      }
      
      mo_sourceCopyFile.cancel();
      mo_sourceCopyFile.removeEventListener(Event.COMPLETE, onFileCopyComplete);
      mo_sourceCopyFile.removeEventListener(IOErrorEvent.IO_ERROR, onFileCopyError);
      mo_sourceCopyFile = null;
    }
  }
}