/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvCustomImageFileCopierCallbacks

  Author:        Cartrell (Ziro)
    zir0@sbcglobal.net
    https://www.upwork.com/users/~0180f72de0fb06b175
  Date:          07-04-2019
  ActionScript:  3.0
  Description:  
  History:
    07-04-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.customImageFileCopier {
  
  public interface I_SvCustomImageFileCopierCallbacks {
    function fileCopierOnComplete(o_fileCopier:SvCustomImageFileCopier):void;
  }
}