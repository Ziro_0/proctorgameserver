/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvCustomImageFileDeleter

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          07-04-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.customImageFileDeleter {
  import flash.events.Event;
  import flash.events.IOErrorEvent;
  import flash.filesystem.File;

  public final class SvCustomImageFileDeleter {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mas_fileUrls:Array;
    private var mas_errorFileUrls:Array;
    
    private var mo_callbacks:I_SvCustomImageFileDeleterCallbacks;
    
    private var mo_targetFile:File;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvCustomImageFileDeleter(o_callbacks:I_SvCustomImageFileDeleterCallbacks = null) {
      super();
      callbacks = o_callbacks;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_SvCustomImageFileDeleterCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_SvCustomImageFileDeleterCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // cancel
    //==================================================================
    public function cancel():void {
      mas_fileUrls = null;
      uninitFiles();
    }
    
    //==================================================================
    // deleteFiles
    //==================================================================
    public function deleteFiles(as_fileUrls:Array):Boolean {
      if (!as_fileUrls || !as_fileUrls.length) {
        return(false);
      }
      
      mas_errorFileUrls = null;
      mas_fileUrls = as_fileUrls.concat();
      return(deleteNextFile());
    }
    
    //==================================================================
    // errorFileUrls (get)
    //==================================================================
    public function get errorFileUrls():Array {
      return(mas_errorFileUrls);
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      callbacks = null;
      cancel();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addErrorFileUrl
    //==================================================================
    private function addErrorFileUrl(s_url:String):void {
      if (!mas_errorFileUrls) {
        mas_errorFileUrls = [];
      }
      
      mas_errorFileUrls.push(s_url);
    }
    
    //==================================================================
    // complete
    //==================================================================
    private function complete():void {
      uninitFiles();
      mas_fileUrls = null;
      
      if (mo_callbacks) {
        mo_callbacks.fileDeleterOnComplete(this);
      }
    }
    
    //==================================================================
    // deleteFile
    //==================================================================
    private function deleteFile(o_targetFile:File):Boolean {
      mo_targetFile = o_targetFile;
      mo_targetFile.addEventListener(Event.COMPLETE, onFileDeleteComplete, false, 0, true);
      mo_targetFile.addEventListener(IOErrorEvent.IO_ERROR, onFileDeleteError, false, 0, true);
      
      var o_excError:Error = null;
      try {
        mo_targetFile.moveToTrashAsync();
      } catch (o_error:Error) {
        o_excError = o_error;
        trace("SvCustomImageFileDeleter.deleteFile. " +
          "Error: Unable to delete file. Either the file you are trying to delete does not exist, or the " +
          "the server app does not the nexessary permissions to delete the file.");
        addErrorFileUrl(mo_targetFile.url);
        uninitFiles();
      }
      
      if (o_excError) {
        return(false);
      }
      
      return(true);
    }
    
    //==================================================================
    // deleteNextFile
    //==================================================================
    private function deleteNextFile():Boolean {
      if (!mas_fileUrls.length) {
        return(false);
      }
      
      var s_fileUrl:String = mas_fileUrls.pop();
      if (!s_fileUrl) {
        return(deleteNextFile());
      }
      
      var o_targetFile:File = File.applicationStorageDirectory.resolvePath(s_fileUrl);
      if (!deleteFile(o_targetFile)) {
        return(deleteNextFile());
      }
      
      return(true);
    }
    
    //==================================================================
    // onFileDeleteComplete
    //==================================================================
    private function onFileDeleteComplete(o_event:Event):void {
      uninitFiles();
      
      if (mas_fileUrls.length) {
        deleteNextFile();
      } else {
        complete();
      }
    }
    
    //==================================================================
    // onFileDeleteError
    //==================================================================
    private function onFileDeleteError(o_event:IOErrorEvent):void {
      trace("SvCustomImageFileDeleter.onFileDeleteError. Error: unable to remove a file. " + o_event);
      addErrorFileUrl(mo_targetFile.url);
      uninitFiles();
      
      if (mas_fileUrls.length) {
        deleteNextFile();
      } else {
        complete();
      }
    }
    
    //==================================================================
    // uninitFiles
    //==================================================================
    private function uninitFiles():void {
      if (!mo_targetFile) {
        return;
      }
      
      mo_targetFile.cancel();
      mo_targetFile.removeEventListener(Event.COMPLETE, onFileDeleteComplete);
      mo_targetFile.removeEventListener(IOErrorEvent.IO_ERROR, onFileDeleteError);
      mo_targetFile = null;
    }
  }
}