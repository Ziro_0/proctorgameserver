/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvCustomImageFileDeleterCallbacks

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          07-05-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.customImageFileDeleter {
  
  public interface I_SvCustomImageFileDeleterCallbacks {
    function fileDeleterOnComplete(o_fileDeletier:SvCustomImageFileDeleter):void;
  }
}