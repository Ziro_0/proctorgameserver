/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvGamesImport

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          08-23-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.gamesImport {
  import engine.data.settings.SvSettings;
  import engine.ui.mainView.SvMainView;
  import flash.events.ErrorEvent;
  import flash.events.Event;
  import flash.filesystem.File;
  import flash.filesystem.FileMode;
  import flash.filesystem.FileStream;
  import flash.net.FileFilter;

  public class SvGamesImport {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    private var mo_callbacks:I_SvGamesImportCallbacks;
    private var mo_settings:SvSettings;
    private var mo_workingFile:File;
    private var mo_selectedFile:File;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvGamesImport() {
      super();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // run
    //==================================================================
    public function run(o_settings:SvSettings, o_callbacks:I_SvGamesImportCallbacks):Boolean {
      if (mo_workingFile || !o_settings) {
        return(false);
      }
      
      mo_settings = o_settings;
      mo_callbacks = o_callbacks;
      
      mo_workingFile = new File();
      var ao_filters:Array = [
        new FileFilter("JSON Files", "*.json")
      ];
      
      try {
        mo_workingFile.browseForOpen("Select The JSON Settings File To Import", ao_filters);
      } catch (o_error:Error) {
        notifyError(o_error.message);
        uninitFile();
        return(false);
      }
      
      mo_workingFile.addEventListener(Event.SELECT, onFileSelected, false, 0, true);
      mo_workingFile.addEventListener(Event.CANCEL, onFileCancelled, false, 0, true);
      mo_workingFile.addEventListener(ErrorEvent.ERROR, onFileError, false, 0, true);
      return(true);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // notifyError
    //==================================================================
    private function notifyError(o_data:Object):void {
      if (mo_callbacks) {
        mo_callbacks.gamesImportOnComplete(this, SvGamesImportResults.ERROR, o_data);
      }
    }
    
    //==================================================================
    // notifySuccess
    //==================================================================
    private function notifySuccess():void {
      if (mo_callbacks) {
        mo_callbacks.gamesImportOnComplete(this, SvGamesImportResults.COMPLETE);
      }
    }
    
    //==================================================================
    // onFileCancelled
    //==================================================================
    private function onFileCancelled(o_event:Event):void {
      uninitFile();
      
      if (mo_callbacks) {
        mo_callbacks.gamesImportOnComplete(this, SvGamesImportResults.CANCEL);
      }
    }
    
    //==================================================================
    // onFileError
    //==================================================================
    private function onFileError(o_event:ErrorEvent):void {
      uninitFile();
      
      var o_errorData:Object = {
        message: "Unable to import games settings, due to a file error. (Error ID: " +
          o_event.errorID + ")",
        errorEvent: o_event
      }
      
      notifyError(o_errorData);
    }
    
    //==================================================================
    // onFileSelected
    //==================================================================
    private function onFileSelected(o_event:Event):void {
      uninitFile();
      
      var o_file:File = o_event.currentTarget as File;
      if (mo_settings.importGameSettings(o_file)) {
        notifySuccess();
      } else {
        notifyError("Error importing the file (Code: SvGamesImport.onFileSelected");
      }
    }
    
    //==================================================================
    // uninitFile
    //==================================================================
    private function uninitFile():void {
      mo_workingFile.cancel();
      mo_workingFile.removeEventListener(Event.SELECT, onFileSelected);
      mo_workingFile.removeEventListener(Event.CANCEL, onFileCancelled);
      mo_workingFile.removeEventListener(ErrorEvent.ERROR, onFileError);
      mo_workingFile = null;
    }
  }
}