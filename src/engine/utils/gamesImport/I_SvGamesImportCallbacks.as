/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvGamesImportCallbacks

  Author:        Cartrell (Ziro)
    zir0@sbcglobal.net
    https://www.upwork.com/users/~0180f72de0fb06b175
  Date:          08-23-2019
  ActionScript:  3.0
  Description:  
  History:
    08-23-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.gamesImport {
  
  public interface I_SvGamesImportCallbacks {
    function gamesImportOnComplete(o_import:SvGamesImport, u_action:uint, o_data:Object = null):void;
  }
}