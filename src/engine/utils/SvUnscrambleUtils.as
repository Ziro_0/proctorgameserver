/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvUnscrambleUtils

  Author:        Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:          05-18-2018
  ActionScript:  3.0
  Description:  
  History:
    05-18-2018:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils {
  import engine.data.settings.scrambleWords.SvScrambleWordEntrySettings;
  import flib.input.FAscii;
  import flib.utils.FArrayUtils;
  
  public final class SvUnscrambleUtils {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvUnscrambleUtils() {
      super();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createWordEntry
    //==================================================================
    public function createWordEntry(s_word:String):SvScrambleWordEntrySettings {
      if (!s_word) {
        return(null);
      }
      
      var o_entryData:Object = {};
      o_entryData[SvScrambleWordEntrySettings.PROP_WORD] = s_word;
      o_entryData[SvScrambleWordEntrySettings.PROP_SCRAMBLE] = getScramble(s_word);
      return(new SvScrambleWordEntrySettings(o_entryData));
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getCharCodes
    //==================================================================
    private function getCharCodes(s_source:String):Array {
      var au_charCodes:Array = [];
      for (var i_index:int = 0; i_index < s_source.length; i_index++) {
        var u_charCode:uint = s_source.charCodeAt(i_index);
        if (u_charCode > FAscii.SPACE) {
          //only printable, non-whitespace characters are used in the scramble
          au_charCodes.push(u_charCode);
        }
      }
      return(au_charCodes);
    }
    
    //==================================================================
    // getScramble
    //==================================================================
    private function getScramble(s_word:String):String {
      var au_charCodes:Array = getCharCodes(s_word);
      FArrayUtils.RandomizeEntries(au_charCodes);
      var s_scramble:String = "";
      for each (var u_charCode:uint in au_charCodes) {
        s_scramble += String.fromCharCode(u_charCode);
      }
      
      return(s_scramble);
    }
  }
}