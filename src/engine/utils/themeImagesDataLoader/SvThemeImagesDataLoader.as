/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvThemeImagesDataLoader

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-24-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.themeImagesDataLoader {
  import com.greensock.events.LoaderEvent;
  import com.greensock.loading.DataLoader;
  import com.greensock.loading.LoaderMax;
  import com.greensock.loading.LoaderStatus;
  import com.greensock.loading.core.LoaderCore;
  import engine.I_SvEngine;
  import engine.common.data.CustomThemePath;
  import engine.common.utils.ThemeImagePath;
  import engine.common.utils.themeImagesDataLoader.ThemeImagesData;
  import engine.data.SvDesignData;
  import engine.utils.themeImagesDataLoader.I_SvThemeImagesDataLoaderCallbacks;
  import flash.filesystem.File;
  import flash.utils.ByteArray;
  import flash.utils.Dictionary;
  import flib.utils.FDataBlock;

  public final class SvThemeImagesDataLoader {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //key: String (request id)
    //data: LoaderMax
    private var mo_loadersByRequestId:Dictionary;
    
    private var mo_callbacks:I_SvThemeImagesDataLoaderCallbacks;
    private var mo_engine:I_SvEngine;
    
    private var ms_deviceProfileId:String;
    
    //key: String (image key [theme id]-[component name]-[game id])
    //data: ByteArray
    private var mo_imageDataByKey:Object;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvThemeImagesDataLoader(s_deviceProfileId:String, o_engine:I_SvEngine,
    o_callbacks:I_SvThemeImagesDataLoaderCallbacks = null) {
      super();
      mo_imageDataByKey = { };
      ms_deviceProfileId = s_deviceProfileId;
      mo_engine = o_engine;
      callbacks = o_callbacks;
      mo_loadersByRequestId = new Dictionary();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // callbacks (get)
    //==================================================================
    public function get callbacks():I_SvThemeImagesDataLoaderCallbacks {
      return(mo_callbacks);
    }
    
    //==================================================================
    // callbacks (set)
    //==================================================================
    public function set callbacks(o_value:I_SvThemeImagesDataLoaderCallbacks):void {
      mo_callbacks = o_value;
    }
    
    //==================================================================
    // dispose
    //==================================================================
    public function dispose():void {
      for (var s_requestId:String in mo_loadersByRequestId) {
        var o_loaderMax:LoaderMax = mo_loadersByRequestId[s_requestId] as LoaderMax;
        if (o_loaderMax) {
          o_loaderMax.dispose(true);
          delete mo_loadersByRequestId[s_requestId];
        }
      }
    }
    
    //==================================================================
    // getImageData
    //==================================================================
    public function getImageData(s_themeId:String, s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):ByteArray {
      var s_key:String = getImageKey(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName);
      return(s_key in mo_imageDataByKey ? mo_imageDataByKey[s_key] as ByteArray : null);
    }
    
    //==================================================================
    // imageDataByKey (get)
    //==================================================================
    public function get imageDataByKey():Object {
      return(mo_imageDataByKey);
    }
    
    //==================================================================
    // imageDataExists
    //==================================================================
    public function imageDataExists(s_themeId:String, s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):Boolean {
      return(getImageData(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName) != null);  
    }
    
    //==================================================================
    // load
    //==================================================================
    public function load(s_requestId:String, o_componentThemeIdsByName:Object, s_themeComponentName:String,
    s_themeId:String, s_gameId:String):Boolean {
      if (!s_requestId) {
        mo_engine.traceAndLog("SvThemeImagesDataLoader.load. Warning: Request ID is required.");
        return(false);
      }
      
      var s_componentThemeId:String = o_componentThemeIdsByName[s_themeComponentName] as String;
      if (!s_componentThemeId) {
        mo_engine.traceAndLog("SvThemeImagesDataLoader.load. Warning: No component theme ID found for theme " +
          "component name: " + s_themeComponentName);
        return(false);
      }
      
      if (imageDataExists(s_themeId, s_componentThemeId, s_gameId, s_themeComponentName)) {
        //image data already exists for the specified params - no need to load the image file
        return(false);
      }
      
      var o_loaderMax:LoaderMax = createLoaderMax(s_requestId);
      
      var o_loader:DataLoader = createDataLoader(s_themeId, s_gameId, s_themeComponentName,
        s_componentThemeId);
      
      if (o_loader) {
        o_loaderMax.append(o_loader);
        o_loaderMax.load();
      }
      
      return(isLoading(o_loaderMax));
    }
    
    //==================================================================
    // uninit
    //==================================================================
    public function uninit():void {
      callbacks = null;
      dispose();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // createDataLoader
    //==================================================================
    private function createDataLoader(s_themeId:String, s_gameId:String,
    s_themeComponentName:String, s_themeComponentId:String):DataLoader {
      var o_imagePath:ThemeImagePath;
      var s_themesBaseUrl:String;
      
      if (CustomThemePath.IsCustom(s_themeComponentId)) {
        s_themesBaseUrl = CustomThemePath.CUSTOM_BASE_PATH;
        o_imagePath = new ThemeImagePath(s_themesBaseUrl, ms_deviceProfileId);
        o_imagePath.isApplicationStorageMode = true;
      } else {
        s_themesBaseUrl = SvDesignData.THEMES_BASE_URL;
        o_imagePath = new ThemeImagePath(s_themesBaseUrl, ms_deviceProfileId);
      }
      
      var s_imageLoaderUrl:String = o_imagePath.createGameOrNonGameFileUrl(s_themeId, s_gameId,
        s_themeComponentName);
        
      if (!fileExists(s_imageLoaderUrl)) {
        return(null);
      }
      
      var o_vars:Object = {
        format: "binary",
        gameId: s_gameId,
        themeId: s_themeId,
        themeComponentName: s_themeComponentName,
        themeComponentId: s_themeComponentId
      };
      return(new DataLoader(s_imageLoaderUrl, o_vars));
    }
    
    //==================================================================
    // createLoaderMax
    //==================================================================
    private function createLoaderMax(s_requestId:String):LoaderMax {
      var o_loaderMax:LoaderMax = mo_loadersByRequestId[s_requestId] as LoaderMax;
      if (o_loaderMax) {
        //loader already created
        return(o_loaderMax);
      }
      
      var o_vars:Object = {
        onComplete: onLoaderMaxComplete,
        onError: onLoaderMaxError,
        onChildComplete: onLoaderMaxChildComplete,
        onChildFail: onLoaderMaxChildFail,
        
        requestId: s_requestId
      };
      
      o_loaderMax = mo_loadersByRequestId[s_requestId] = new LoaderMax(o_vars);
      return(o_loaderMax);
    }
    
    //==================================================================
    // fileExists
    //==================================================================
    private function fileExists(s_url:String):Boolean {
      var o_file:File = new File(s_url);
      return(o_file.exists);
    }
    
    //==================================================================
    // getImageKey
    //==================================================================
    private function getImageKey(s_themeId:String, s_componentThemeId:String, s_gameId:String,
    s_themeComponentName:String):String {
      var s_keyPart1:String =
        CustomThemePath.IsCustom(s_componentThemeId) ? s_themeId : s_themeComponentName;
      var s_keyPart2:String = resolveImageKeyComp(s_gameId);
      var s_keyPart3:String = s_themeComponentName;
      
      var s_key:String =
        resolveImageKeyComp(s_keyPart1) + "-" +
        String(s_keyPart2 ? String(s_keyPart2 + "-") : "") +
        resolveImageKeyComp(s_keyPart3);
      
      return(s_key);
    }
    
    //==================================================================
    // isLoading
    //==================================================================
    private function isLoading(o_loaderMax:LoaderMax):Boolean {
      for (var i_index:int = 0; i_index < o_loaderMax.numChildren; i_index++) {
        var o_loader:LoaderCore = o_loaderMax.getChildAt(i_index);
        if (o_loader.status == LoaderStatus.LOADING ||
        o_loader.status == LoaderStatus.READY) {
          return(true);
        }
      }
      
      return(false);
    }
    
    //==================================================================
    // onLoaderMaxChildComplete
    //==================================================================
    private function onLoaderMaxChildComplete(o_event:LoaderEvent):void {
      var o_loader:DataLoader = o_event.target as DataLoader;
      var o_block:FDataBlock = new FDataBlock(o_loader.vars);
      var s_themeId:String = o_block.getString("themeId", "");
      var s_gameId:String = o_block.getString("gameId", "");
      var s_themeComponentName:String = o_block.getString("themeComponentName", "");
      var s_themeComponentId:String = o_block.getString("themeComponentId", "");
      
      var s_imageKey:String = getImageKey(s_themeId, s_themeComponentId, s_gameId, s_themeComponentName);
      if (s_imageKey) {
        mo_imageDataByKey[s_imageKey] = new ThemeImagesData(s_imageKey, s_themeId, s_gameId,
          s_themeComponentName, s_themeComponentId, o_loader.content as ByteArray);
      }
    }
    
    //==================================================================
    // onLoaderMaxChildFail
    //==================================================================
    private function onLoaderMaxChildFail(o_event:LoaderEvent):void {
      var o_loader:DataLoader = o_event.target as DataLoader;
      var o_block:FDataBlock = new FDataBlock(o_loader.vars);
      var s_imageKey:String = getImageKey(
        o_block.getString("themeId", ""),
        o_block.getString("themeComponentId", ""),
        o_block.getString("gameId", ""),
        o_block.getString("themeComponentName", ""));
      trace("SvThemeImagesDataLoader.onLoaderMaxChildFail: " + o_event + "; image key: " + s_imageKey);
      mo_engine.traceAndLog("Error loading image using key: " + s_imageKey );
    }
    
    //==================================================================
    // onLoaderMaxComplete
    //==================================================================
    private function onLoaderMaxComplete(o_event:LoaderEvent):void {
      var o_loader:LoaderMax = o_event.currentTarget as LoaderMax;
      var o_block:FDataBlock = new FDataBlock(o_loader.vars);
      var s_requestId:String = o_block.getString("requestId");
      delete mo_loadersByRequestId[s_requestId];
      
      if (mo_callbacks) {
        mo_callbacks.themeImagesDataLoaderOnComplete(this, s_requestId);
      }
    }
    
    //==================================================================
    // onLoaderMaxError
    //==================================================================
    private function onLoaderMaxError(o_event:LoaderEvent):void {
      mo_engine.traceAndLog("SvThemeImagesDataLoader.onLoaderMaxError: " + o_event);
    }
    
    //==================================================================
    // resolveImageKeyComp
    //==================================================================
    private function resolveImageKeyComp(s_keyComp:String):String {
      return(s_keyComp || "");
    }
  }
}