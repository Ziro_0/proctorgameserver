/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  I_SvThemeImagesDataLoaderCallbacks

  Author:        Cartrell (Ziro)
    zir0@sbcglobal.net
    https://www.upwork.com/users/~0180f72de0fb06b175
  Date:          05-24-2019
  ActionScript:  3.0
  Description:  
  History:
    05-24-2019:  Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils.themeImagesDataLoader {
  
  public interface I_SvThemeImagesDataLoaderCallbacks {
    function themeImagesDataLoaderOnComplete(o_loader:SvThemeImagesDataLoader, s_clientId:String):void;
  }
}