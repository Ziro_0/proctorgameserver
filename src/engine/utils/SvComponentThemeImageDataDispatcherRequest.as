/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvComponentThemeImageDataDispatcherRequest

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-24-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils {
  import engine.I_SvEngine;
  import engine.common.data.CustomThemePath;
  import engine.common.data.messageKeys.RequestComponentThemeImageDataKeys;
  import engine.common.utils.ThemeImagePath;
  import engine.data.SvDesignData;
  import engine.utils.themeImagesDataLoader.SvThemeImagesDataLoader;
  import flash.filesystem.File;
  import flib.utils.FDataBlock;

  public final class SvComponentThemeImageDataDispatcherRequest {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: Object
    // (see RequestComponentThemeImageDataKeys for the format of the data)
    private var mao_requests:Array;
    
    private var mo_engine:I_SvEngine;
    
    private var mo_messageBlock:FDataBlock;
    private var ms_clientId:String;
    private var ms_id:String;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvComponentThemeImageDataDispatcherRequest(o_engine:I_SvEngine, s_id:String) {
      super();
      mo_engine = o_engine;
      ms_id = s_id;
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // clientId (get)
    //==================================================================
    public function get clientId():String {
      return(ms_clientId);
    }
    
    //==================================================================
    // id (get)
    //==================================================================
    public function get id():String {
      return(ms_id);
    }
    
    //==================================================================
    // load
    //==================================================================
    public function load(s_clientId:String, o_messageBlock:FDataBlock,
    o_componentThemeIdsByName:Object, o_themeImagesData:SvThemeImagesDataLoader):Boolean {
      if (!s_clientId || !o_messageBlock || !o_componentThemeIdsByName || !o_themeImagesData) {
        trace("SvComponentThemeImageDataDispatcher.load. Warning: Invalid params");
        traceAndLog("Error requesting images. Reason: Invalid params.");
        return(false);
      }
      
      var ao_requests:Array = o_messageBlock.getArray(RequestComponentThemeImageDataKeys.REQUESTS);
      if (!ao_requests || !ao_requests.length) {
        trace("SvComponentThemeImageDataDispatcher.load. Warning: No requests sent");
        traceAndLog("Error requesting images. Reason: Request data is empty.");
        return(false);
      }
      
      ms_clientId = s_clientId;
      mao_requests = ao_requests.concat();
      
      var o_readOnlyThemesImagePath:ThemeImagePath = new ThemeImagePath(SvDesignData.THEMES_BASE_URL,
        o_messageBlock.getString(RequestComponentThemeImageDataKeys.DEVICE_PROFILE_ID));
      
      var o_customThemesImagePath:ThemeImagePath = new ThemeImagePath(CustomThemePath.CUSTOM_BASE_PATH,
        o_messageBlock.getString(RequestComponentThemeImageDataKeys.DEVICE_PROFILE_ID));
      o_customThemesImagePath.isApplicationStorageMode = true;
      
      var o_activeThemesImagePath:ThemeImagePath;
      
      var o_block:FDataBlock = new FDataBlock();
      var b_isLoading:Boolean;
      
      for each (var o_request:Object in mao_requests) {
        o_block.data = o_request;
        
        var s_themeComponentName:String =
          o_block.getString(RequestComponentThemeImageDataKeys.REQ_THEME_COMPONENT_NAME);
        var s_themeId:String = o_block.getString(RequestComponentThemeImageDataKeys.REQ_THEME_ID);
        var s_themeComponentId:String = o_componentThemeIdsByName[s_themeComponentName];
        if (!s_themeComponentName || !s_themeId || !s_themeComponentId) {
          continue;
        }
        
        var s_gameId:String = o_block.getString(RequestComponentThemeImageDataKeys.REQ_GAME_ID);
        var n_fileSize:Number = o_block.getNumber(RequestComponentThemeImageDataKeys.REQ_FILE_SIZE);
        
        if (CustomThemePath.IsCustom(s_themeComponentId)) {
          o_activeThemesImagePath = o_customThemesImagePath;
        } else {
          s_themeId = s_themeComponentId;
          o_activeThemesImagePath = o_readOnlyThemesImagePath;
        }
        
        if (shouldLoadFileForClient(o_activeThemesImagePath, s_themeComponentName, s_themeId,
        s_gameId, n_fileSize)) {
          if (o_themeImagesData.load(ms_id, o_componentThemeIdsByName, s_themeComponentName, s_themeId,
          s_gameId)) {
            b_isLoading = true;
          }
        }
      }
      
      return(b_isLoading);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // getExistingFile
    //==================================================================
    private function getExistingFile(s_fileUrl:String):File {
      var o_file:File;
      try {
        o_file = new File(s_fileUrl);
      } catch (o_error:Error) {
        return(null);
      }
      
      return(o_file.exists ? o_file : null);
    }
    
    //==================================================================
    // shouldLoadFileForClient
    //==================================================================
    private function shouldLoadFileForClient(o_themesImagePath:ThemeImagePath, s_themeComponentName:String,
    s_themeId:String, s_gameId:String, n_fileSize:Number):Boolean {
      var s_fileUrl:String = o_themesImagePath.createFileUrl(s_themeId, s_gameId, s_themeComponentName);
      
      var o_file:File = getExistingFile(s_fileUrl);
      if (!o_file) {
        s_fileUrl = o_themesImagePath.createNonGameFileUrl(s_themeId, s_themeComponentName);
        o_file = getExistingFile(s_fileUrl);
        if (!o_file) {
          trace("SvComponentThemeImageDataDispatcherRequest.shouldLoadFileForClient. Warning: " +
            "file doesn't exist for: theme id: " + s_themeId +
            "; game id: " + s_gameId +
            "; theme component name: " + s_themeComponentName);
          return(false);
        }
      }
      
      //The zero-filesize check could be enforced before getting this far. However, need to check that
      // a file actually exists, first.
      return(n_fileSize == 0 || o_file.size != n_fileSize);
    }
    
    //==================================================================
    // traceAndLog
    //==================================================================
    private function traceAndLog(s_message:String):void {
      if (mo_engine) {
        mo_engine.traceAndLog(s_message);
      }
    }
  }
}