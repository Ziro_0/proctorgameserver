/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvComponentThemeImageDataDispatcher

  Author:        Cartrell (Ziro)
    cartrell@gameplaycoder.com
    https://gameplaycoder.com/
  Date:          05-23-2019
  ActionScript:  3.0
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine.utils {
  import engine.I_SvEngine;
  import engine.common.data.ServerMessageTypes;
  import engine.common.data.messageKeys.RequestComponentThemeImageDataKeys;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.common.utils.messages.Message;
  import engine.utils.themeImagesDataLoader.I_SvThemeImagesDataLoaderCallbacks;
  import engine.utils.themeImagesDataLoader.SvThemeImagesDataLoader;
  import flash.utils.Dictionary;
  import flib.utils.FDataBlock;

  public final class SvComponentThemeImageDataDispatcher implements
  I_SvThemeImagesDataLoaderCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // member variables
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //key: String (request id)
    //data: SvComponentThemeImageDataDispatcherRequest
    private var mo_requestsById:Dictionary;
    
    //key: String (device id)
    //data: SvThemeImagesData;
    private var mo_imagesRepo:Dictionary;
    
    private var mo_engine:I_SvEngine;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvComponentThemeImageDataDispatcher(o_engine:I_SvEngine) {
      mo_engine = o_engine;
      mo_imagesRepo = new Dictionary();
      mo_requestsById = new Dictionary();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // load
    //==================================================================
    public function load(s_clientId:String, o_messageBlock:FDataBlock):void {
      if (!mo_engine || !s_clientId || !o_messageBlock) {
        return;
      }
      
      var s_deviceProfileId:String =
        o_messageBlock.getString(RequestComponentThemeImageDataKeys.DEVICE_PROFILE_ID);
      if (!s_deviceProfileId) {
        return;
      }
      
      var o_request:SvComponentThemeImageDataDispatcherRequest = addRequest(s_clientId);
      
      var o_componentThemeIdsByName:Object =
        mo_engine.state.getObject(ServerStateKeys.THEME_IDS_BY_COMP_NAME);
      
      var o_themeImagesData:SvThemeImagesDataLoader = getThemesImagesData(s_deviceProfileId);
      
      if (!o_request.load(s_clientId, o_messageBlock, o_componentThemeIdsByName, o_themeImagesData)) {
        themeImagesDataLoaderOnComplete(o_themeImagesData, o_request.id);
      }
    }
    
    //==================================================================
    // themeImagesDataLoaderOnComplete
    //==================================================================
    public function themeImagesDataLoaderOnComplete(o_loader:SvThemeImagesDataLoader,
    s_requestId:String):void {
      var o_request:SvComponentThemeImageDataDispatcherRequest = deleteRequest(s_requestId);
      
      var o_message:Object = Message.Create(ServerMessageTypes.COMPONENT_THEME_IMAGES);
      o_message[RequestComponentThemeImageDataKeys.REQUESTS] = o_loader.imageDataByKey;
      mo_engine.sendMessageToClient(o_request.clientId, o_message);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private methods
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addRequest
    //==================================================================
    private function addRequest(s_clientId:String):SvComponentThemeImageDataDispatcherRequest {
      var s_requestId:String = createRequestId(s_clientId);
      var o_request:SvComponentThemeImageDataDispatcherRequest =
        new SvComponentThemeImageDataDispatcherRequest(mo_engine, s_requestId);
      mo_requestsById[s_requestId] = o_request;
      return(o_request);
    }
    
    //==================================================================
    // createRequestId
    //==================================================================
    private function createRequestId(s_clientId:String):String {
      return(s_clientId + new Date().time);
    }
    
    //==================================================================
    // deleteRequest
    //==================================================================
    private function deleteRequest(s_requestId:String):SvComponentThemeImageDataDispatcherRequest {
      var o_request:SvComponentThemeImageDataDispatcherRequest =
        mo_requestsById[s_requestId] as SvComponentThemeImageDataDispatcherRequest;
      delete mo_requestsById[s_requestId];
      return(o_request);
    }
    
    //==================================================================
    // getThemesImagesData
    //==================================================================
    private function getThemesImagesData(s_deviceProfileId:String):SvThemeImagesDataLoader {
      var o_data:SvThemeImagesDataLoader = mo_imagesRepo[s_deviceProfileId] as SvThemeImagesDataLoader;
      if (!o_data) {
        o_data = mo_imagesRepo[s_deviceProfileId] =
          new SvThemeImagesDataLoader(s_deviceProfileId, mo_engine, this);
      }
      
      return(o_data);
    }
  }
}