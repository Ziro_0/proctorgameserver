/*
  ////////////////////////////////////////////////////////////////////
  ====================================================================
  SvEngine

  Author:				Cartrell (Ziro)
    https://www.gameplaycoder.com
  Date:   			05-03-2018
  ActionScript:	3.0
  Description:	
  History:
    05-03-2018:	Started.
  ====================================================================
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
*/

package engine {
  import engine.common.data.ClientMessageTypes;
  import engine.common.data.GameIds;
  import engine.common.data.MessageProperties;
  import engine.common.data.PlayerGameProgress;
  import engine.common.data.PlayerStatuses;
  import engine.common.data.ServerMessageTypes;
  import engine.common.data.UserData;
  import engine.common.data.messageKeys.ActiveGameChangedKeys;
  import engine.common.data.messageKeys.ClientJoinedKeys;
  import engine.common.data.messageKeys.ClientLeftKeys;
  import engine.common.data.messageKeys.ClientNextKeys;
  import engine.common.data.messageKeys.GameStateChangedKeys;
  import engine.common.data.messageKeys.PlayerStateKeys;
  import engine.common.data.messageKeys.ServerStateKeys;
  import engine.common.ui.dialogs.DialogActions;
  import engine.common.ui.dialogs.IDialogManagerCallbacks;
  import engine.common.utils.AppDescriptorData;
  import engine.common.utils.messages.Message;
  import engine.data.SvDialogData;
  import engine.data.settings.SvSettings;
  import engine.data.settings.SvSettingsData;
  import engine.data.settings.SvThemeSettings;
  import engine.data.settings.SvThemesSettings;
  import engine.game.SvGame;
  import engine.gameManager.SvGameFactory;
  import engine.ui.dialogs.SvDialogActions;
  import engine.ui.dialogs.SvDialogIds;
  import engine.ui.dialogs.SvDialogManager;
  import engine.ui.dialogs.game.SvGameBaseDialog;
  import engine.ui.dialogs.games.SvGamesDialog;
  import engine.ui.dialogs.games.SvGamesDialogActions;
  import engine.ui.dialogs.message.SvMessageDialog;
  import engine.ui.dialogs.themes.SvThemesDialogCompleteData;
  import engine.ui.mainView.I_SvMainViewCallbacks;
  import engine.ui.mainView.SvMainView;
  import engine.utils.SvComponentThemeImageDataDispatcher;
  import engine.utils.gamesImport.I_SvGamesImportCallbacks;
  import engine.utils.gamesImport.SvGamesImport;
  import engine.utils.gamesImport.SvGamesImportResults;
  import flash.display.Sprite;
  import flash.events.ErrorEvent;
  import flash.filesystem.File;
  import flib.utils.FArrayUtils;
  import flib.utils.FDataBlock;
  import net.data.NetClientData;
  import net.data.NetConnectData;
  import net.server.I_NetServerCallbacks;
  import net.server.NetServer;
  import net.utils.Utils;

  public class SvEngine implements I_SvEngine, I_SvMainViewCallbacks, I_NetServerCallbacks,
  IDialogManagerCallbacks, I_SvGamesImportCallbacks {
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // static / const
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // members
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //data: String
    private var mas_waitingPlayerIds:Array;
    
    private var mo_mainView:SvMainView;
    private var mo_game:SvGame;
    private var mo_dialogManager:SvDialogManager;
    private var mo_settings:SvSettings;
    
    private var mo_importer:SvGamesImport;
    
    private var mo_netServer:NetServer;
    
    private var mo_state:FDataBlock;
    
    private var ms_dialogAffectsUserId:String;
    
    private var mb_isStartGameRequested:Boolean;
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // constructor
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    public function SvEngine():void {
      super();
      init();
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // public functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // activeGameId (get)
    //==================================================================
    public function get activeGameId():String {
      return(mo_state.getString(ServerStateKeys.ACTIVE_GAME_ID));
    }
    
    //==================================================================
    // activeGameId (set)
    //==================================================================
    public function set activeGameId(s_value:String):void {
      var b_isValidGameId:Boolean = GameIds.ALL.indexOf(s_value) > -1;
      if (b_isValidGameId) {
        setActiveGameId(s_value);
      } else {
        clearActiveGameId();
      }
    }
    
    //==================================================================
    // activeGameSettings (get)
    //==================================================================
    public function get activeGameSettings():Object {
      return(mo_state.getObject(ServerStateKeys.ACTIVE_GAME_SETTINGS));
    }
    
    //==================================================================
    // areAllWaitingPlayersReady
    //==================================================================
    public function areAllWaitingPlayersReady():Boolean {
      return(mo_game &&
        isWaitingForPlayers() &&
        getClientIds().length > 0 &&
        mas_waitingPlayerIds.length == 0);
    }
    
    //==================================================================
    // dialogManager (get)
    //==================================================================
    public function get dialogManager():SvDialogManager {
      return(mo_dialogManager);
    }
    
    //==================================================================
    // dialogManagerOnAction
    //==================================================================
    public function dialogManagerOnAction(s_dialogId:String, s_dialogAction:String, o_data:Object):void {
      switch (s_dialogId) {
        case SvDialogIds.GAMES:
          onDialogActionGames(s_dialogAction, o_data);
          break;
          
        case SvDialogIds.THEMES:
          onDialogActionThemes(s_dialogAction, o_data);
          break;
          
        case SvDialogIds.SETTINGS_FILE:
          onDialogActionSettings(s_dialogAction, o_data);
          break;
      }
    }
    
    //==================================================================
    // display (get)
    //==================================================================
    public function get display():Sprite {
      return(mo_mainView.display);
    }
    
    //==================================================================
    // game (get)
    //==================================================================
    public function get game():SvGame {
      return(mo_game);
    }
    
    //==================================================================
    // gamesImportOnComplete
    //==================================================================
    public function gamesImportOnComplete(o_import:SvGamesImport, u_action:uint,
    o_data:Object = null):void {
      mo_importer = null;
      
      if (u_action == SvGamesImportResults.COMPLETE) {
        handleGamesImportComplete(o_data);
      } else if (u_action == SvGamesImportResults.ERROR) {
        handleGamesImportError(o_data);
      }
    }
    
    //==================================================================
    // getActiveClientIds
    //==================================================================
    public function getActiveClientIds():Array {
      var as_clientIds:Array = [];
      var as_keys:Array = mo_state.getBlock(ServerStateKeys.PLAYERS).getKeys();
      for each (var s_clientId:String in as_keys) {
        var o_playerState:FDataBlock = getPlayerState(s_clientId);
        if (o_playerState.getNumber(PlayerStateKeys.STATUS) == PlayerStatuses.ACTIVE) {
          as_clientIds.push(s_clientId);
        }
      }
      
      return(as_clientIds);
    }
    
    //==================================================================
    // getClientIds
    //==================================================================
    public function getClientIds():Array  {
      var as_clientIds:Array = [];
      var o_players:FDataBlock = mo_state.getBlock(ServerStateKeys.PLAYERS);
      for (var s_clientId:String in o_players.data) {
        as_clientIds.push(s_clientId);
      }
      return(as_clientIds);
    }
    
    //==================================================================
    // getPlayerState
    //==================================================================
    public function getPlayerState(s_clientId:String):FDataBlock  {
      var o_players:FDataBlock = mo_state.getBlock(ServerStateKeys.PLAYERS);
      return(o_players ? o_players.getBlock(s_clientId) : null);
    }
    
    //==================================================================
    // isGameStarted (get)
    //==================================================================
    public function get isGameStarted():Boolean {
      return(mo_state.getBoolean(ServerStateKeys.IS_GAME_STARTED));
    }
    
    //==================================================================
    // isGameStarted (set)
    //==================================================================
    public function set isGameStarted(b_value:Boolean):void {
      if (!activeGameId) {
        //can't start/stop a game if one isn't active
        return;
      }
      
      mb_isStartGameRequested = b_value;
      
      if (mb_isStartGameRequested) {
        if (areAllWaitingPlayersReady()) {
          handleAllClientsAreReady();
        }
      } else {
        activeGameId = null;
        mo_state.setProperty(ServerStateKeys.IS_GAME_STARTED, false);
        notifyClientsOfGameStateChanged();
      }
    }
    
    //==================================================================
    // isValidClientId
    //==================================================================
    public function isValidClientId(s_clientId:String):Boolean {
      var o_players:FDataBlock = mo_state.getBlock(ServerStateKeys.PLAYERS);
      return(o_players && o_players.containsProperty(s_clientId));
    }
    
    //==================================================================
    // isWaitingForPlayers
    //==================================================================
    public function isWaitingForPlayers():Boolean {
      return(
        mo_state.getBoolean(ServerStateKeys.WAITING_FOR_PLAYERS) &&
        mas_waitingPlayerIds != null);
    }
    
    //==================================================================
    // mainView (get)
    //==================================================================
    public function get mainView():SvMainView {
      return(mo_mainView);
    }
    
    //==================================================================
    // mainViewOnGames
    //==================================================================
    public function mainViewOnGames():void {
      openGamesDialog();
    }
    
    //==================================================================
    // mainViewOnImport
    //==================================================================
    public function mainViewOnImport():void {
      if (!mo_importer) {
        mo_importer = new SvGamesImport();
        mo_importer.run(mo_settings, this);
      }
    }
    
    //==================================================================
    // mainViewOnKickUser
    //==================================================================
    public function mainViewOnKickUser(s_userId:String):void {
      if (s_userId) { //sanity check
        kickClient(s_userId);
      }
    }
    
    //==================================================================
    // mainViewOnSettingsFile
    //==================================================================
    public function mainViewOnSettingsFile():void {
      openSettingsFileDialog();
    }
    
    //==================================================================
    // mainViewOnStartServer
    //==================================================================
    public function mainViewOnStartServer():void {
      initSocketServer();
    }
    
    //==================================================================
    // mainViewOnStopServer
    //==================================================================
    public function mainViewOnStopServer():void {
      uninitSocketServer();
    }
    
    //==================================================================
    // mainViewOnThemes
    //==================================================================
    public function mainViewOnThemes():void {
      openThemesDialog();
    }
    
    //==================================================================
    // mainViewOnUpdateSettings
    //==================================================================
    public function mainViewOnUpdateSettings():void {
      var o_settingsData:SvSettingsData = mo_settings.data;
      o_settingsData.ipAddress = mo_mainView.ipAddress;
      o_settingsData.port = mo_mainView.port;
      writeSettings();
    }
    
    //==================================================================
    // netServer (get)
    //==================================================================
    public function get netServer():NetServer {
      return(mo_netServer);
    }
    
    //==================================================================
    // netServerOnClientConnect
    //==================================================================
    public function netServerOnClientConnect(o_netServer:NetServer, s_clientId:String):void {
      var o_clientData:NetClientData = o_netServer.getClientData(s_clientId);
      if (!o_clientData || !o_clientData.userData) {
        return;
      }
      
      addClient(s_clientId, o_clientData);
      
      var b_isObserver:Boolean = isGameStarted;
      clientJoinsGame(s_clientId, b_isObserver);
    }
    
    //==================================================================
    // netServerOnClientClose
    //==================================================================
    public function netServerOnClientClose(o_netServer:NetServer, s_clientId:String):void {
      removeClientFromServer(s_clientId);
    }
    
    //==================================================================
    // netServerOnClientMessage
    //==================================================================
    public function netServerOnClientMessage(o_netServer:NetServer, s_clientId:String,
    o_message:Object):void {
      //Handles messages sent from clients.
      
      if (!mo_netServer.isValidClientId(s_clientId)) {
        traceAndLog("SvEngine.netServerOnClientMessage. Invalid client id: " + s_clientId);
        return;
      }
      
      trace("SvEngine.netServerOnClientMessage. " + Utils.ObjectToString(o_message));
      
      if (o_message == null) {
        return; //sanity check
      }
      
      var o_block:FDataBlock = new FDataBlock(o_message);
      var s_type:String = o_block.getString(MessageProperties.TYPE);
      switch (s_type) {
        case ClientMessageTypes.JOIN:
          handleClientJoin(s_clientId, o_block);
          break;
          
        case ClientMessageTypes.NEXT:
          handleClientNext(s_clientId, o_block);
          break;
          
        case ClientMessageTypes.READY:
          handleClientReady(s_clientId, o_block);
          break;
          
        case ClientMessageTypes.WATCH:
          handleClientWatch(s_clientId, o_block);
          break;
          
        case ClientMessageTypes.REQUEST_COMPONENT_THEME_IMAGE_DATA:
          handleClientRequestComponentThemeImageData(s_clientId, o_block);
          break;
          
        default:
          traceAndLog("SvEngine.netServerOnClientMessage. Unknown message type: " + s_type);
          break;
      }
      
      updatePlayersList();
    }
    
    //==================================================================
    // netServerOnClientState
    //==================================================================
    public function netServerOnClientState(o_netServer:NetServer, s_clientId:String,
    o_clientData:NetClientData):void {
      var o_state:Object = o_clientData.state;
      traceAndLog("SvEngine.netServerOnClientState. " + Utils.ObjectToString(o_state));
    }
    
    //==================================================================
    // sendMessageToClient
    //==================================================================
    public function sendMessageToClient(s_clientId:String, o_message:Object):void {
      if (mo_netServer) {
        mo_netServer.sendMessageToClient(s_clientId, o_message);
      }
    }
    
    //==================================================================
    // sendMessageToClients
    //==================================================================
    public function sendMessageToClients(o_message:Object):void {
      if (mo_netServer) {
        mo_netServer.sendMessageToClients(o_message);
      }
    }
    
    //==================================================================
    // sendStateToClient
    //==================================================================
    public function sendStateToClient(s_clientId:String):void {
      if (mo_netServer) {
        mo_netServer.sendStateToClient(s_clientId, mo_state.data);
      }
    }
    
    //==================================================================
    // sendStateToClients
    //==================================================================
    public function sendStateToClients():void {
      if (mo_netServer) {
        mo_netServer.sendStateToClients(mo_state.data);
      }
    }
    
    //==================================================================
    // settingsData (get)
    //==================================================================
    public function get settingsData():SvSettingsData {
      return(mo_settings.data);
    }
    
    //==================================================================
    // state (get)
    //==================================================================
    public function get state():FDataBlock {
      return(mo_state);
    }
    
    //==================================================================
    // traceAndLog
    //==================================================================
    public function traceAndLog(s_message:String, o_bUseDateTime:Object = null):void {
      if (!(o_bUseDateTime is Boolean)) {
        o_bUseDateTime = true;
      }
      
      Utils.TraceAndLog(s_message, mo_mainView ? mo_mainView.logTextField : null, o_bUseDateTime);
    }
    
    //==================================================================
    // updatePlayersList
    //==================================================================
    public function updatePlayersList():void {
      mo_mainView.updatePlayersList();
    }
    
    //==================================================================
    // writeSettings
    //==================================================================
    public function writeSettings():Boolean {
      var b_ok:Boolean = mo_settings.write();
      if (!b_ok) {
        traceAndLog("SvEngine.writeSettings. Warning: Unable to write settings. Error: " +
          mo_settings.error.message);
      }
      return(b_ok);
    }
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // protected functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////
    //==================================================================
    // private functions
    //==================================================================
    ////////////////////////////////////////////////////////////////////
    
    //==================================================================
    // addClient
    //==================================================================
    private function addClient(s_clientId:String, o_clientData:NetClientData):void {
      var o_userData:UserData = o_clientData.userData.clone();
      o_userData.id = s_clientId;
      
      var o_playerState:FDataBlock = new FDataBlock();
      o_playerState.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.NONE);
      o_playerState.setProperty(PlayerStateKeys.USER_DATA, o_userData);
      o_playerState.setProperty(PlayerStateKeys.GAME_PROGRESS, new PlayerGameProgress());
      
      var o_players:FDataBlock = mo_state.getBlock(ServerStateKeys.PLAYERS);
      o_players.addProperty(s_clientId, o_playerState);
      
      mo_mainView.usersDataGridView.addUser(o_playerState);
    }
    
    //==================================================================
    // allObserverClientsBecomeActive
    //==================================================================
    private function allObserverClientsBecomeActive():void {
      var o_players:FDataBlock = mo_state.getBlock(ServerStateKeys.PLAYERS);
      var ao_playerStates:Array = o_players.getValues();
      for each (var o_playerState:FDataBlock in ao_playerStates) {
        if (o_playerState.getBoolean(PlayerStateKeys.STATUS) == PlayerStatuses.OBSERVER) {
          o_playerState.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.ACTIVE);
        }
      }
      
      updateUsersDataGrid();
    }
    
    //==================================================================
    // beginWaitingForPlayers
    //==================================================================
    private function beginWaitingForPlayers():void {
      mo_state.setProperty(ServerStateKeys.WAITING_FOR_PLAYERS, true);
      
      var o_players:FDataBlock = mo_state.getBlock(ServerStateKeys.PLAYERS);
      mas_waitingPlayerIds = o_players.getKeys();
    }
    
    //==================================================================
    // clearActiveGameId
    //==================================================================
    private function clearActiveGameId():void {
      mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_ID, null);
      //mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_TITLE, null);
      mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_SETTINGS, { });
      
      endWaitingForPlayers();
      uninitGame();
      
      mo_dialogManager.setDialogData(new SvDialogData(SvDialogData.TYPE_ACTIVE_GAME_CHANGED));
      
      allObserverClientsBecomeActive();
      
      //send message to all clients that the active game has changed
      var o_message:Object = Message.Create(ServerMessageTypes.ACTIVE_GAME_ID_CHANGED);
      o_message[ActiveGameChangedKeys.GAME_ID] = activeGameId;
      o_message[ActiveGameChangedKeys.GAME_SETTINGS] = activeGameSettings;
      //o_message[ActiveGameChangedKeys.TITLE] = getActiveGameTitle();
      sendMessageToClients(o_message);
    }
    
    //==================================================================
    // clientJoinsGame
    //==================================================================
    private function clientJoinsGame(s_clientId:String, b_isObserver:Boolean):void {
      var o_playerState:FDataBlock = getPlayerState(s_clientId);
      
      if (b_isObserver) {
        //joins as an obverser
        o_playerState.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.OBSERVER);
      } else {
        o_playerState.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.ACTIVE);
      }
      
      if (isWaitingForPlayers()) {
        mas_waitingPlayerIds.push(s_clientId);
      }
      
      //send state to new client
      sendStateToClient(s_clientId);
      
      var o_userData:UserData = o_playerState.getObject(PlayerStateKeys.USER_DATA) as UserData;
      
      //send message to client that joined, send full player state data
      var o_message:Object = Message.Create(ServerMessageTypes.CLIENT_JOINED);
      o_message[ClientJoinedKeys.CLIENT_ID] = s_clientId;
      o_message[ClientJoinedKeys.PLAYER_STATE] = o_playerState;
      o_message[ClientJoinedKeys.NAME] = o_userData.name;
      sendMessageToClient(s_clientId, o_message);
      
      //send message to all other clients that new client joined
      o_message = Message.Create(ServerMessageTypes.CLIENT_JOINED);
      o_message[ClientJoinedKeys.CLIENT_ID] = s_clientId;
      o_message[ClientJoinedKeys.OTHER_PLAYER_STATE] = createOtherPlayerState(o_playerState);
      o_message[ClientJoinedKeys.NAME] = o_userData.name;
      
      var vs_clientIds:Vector.<String> = mo_netServer.getClientDataIds();
      for each (var s_otherClientId:String in vs_clientIds) {
        if (s_otherClientId != s_clientId) {
          sendMessageToClient(s_otherClientId, o_message);
        }
      }
      
      if (mo_game) {
        mo_game.handleClientJoin(s_clientId);
      }
      
      mo_dialogManager.setDialogData(new SvDialogData(SvDialogData.TYPE_WAITING_CLIENTS_CHANGED));
    }
    
    //==================================================================
    // clientLeavesGame
    //==================================================================
    private function clientLeavesGame(s_clientId:String, b_isObserver:Boolean):void {
      //client leaves. if b_isObserver is true, client will become an observer. otherwise, client is
      // leaving game completely (disconnecting from server)
      
      var o_playerState:FDataBlock = getPlayerState(s_clientId);
      if (!o_playerState) {
        return; //client doesn't exist - already removed
      }
      
      //send message that client left to all _other_ clients (except said client)
      var o_message:Object = Message.Create(ServerMessageTypes.CLIENT_LEFT);
      o_message[ClientLeftKeys.CLIENT_ID] = s_clientId;
      o_message[ClientLeftKeys.IS_OBSERVER] = b_isObserver;
      
      var as_clientIds:Array = getClientIds();
      for each (var s_otherClientId:String in as_clientIds) {
        if (s_otherClientId != s_clientId) {
          sendMessageToClient(s_otherClientId, o_message);
        }
      }
      
      if (b_isObserver) {
        //client becomes observer
        o_playerState.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.OBSERVER);
      } else {
        //client leaving game completely
        o_playerState.setProperty(PlayerStateKeys.STATUS, PlayerStatuses.NONE);
      }
      
      removeClientFromWaitingList(s_clientId);
      mo_dialogManager.setDialogData(new SvDialogData(SvDialogData.TYPE_WAITING_CLIENTS_CHANGED));
      
      //allow game engine chance to update due to client leaving
      if (mo_game) {
        mo_game.handleClientLeft(s_clientId);
      }
      
      updatePlayersList();
    }
    
    //==================================================================
    // clientMovesToNextEntry
    //==================================================================
    private function clientMovesToNextEntry(s_clientId:String, o_messageBlock:FDataBlock):void {
      if (mo_game && o_messageBlock) {
        var o_response:Object = o_messageBlock.getObject(ClientNextKeys.RESPONSE);
        mo_game.handleClientNext(s_clientId, o_response);
      }
    }
    
    //==================================================================
    // copyPlayerStatePropertyFrom
    //==================================================================
    private function copyPlayerStatePropertyFrom(s_playerStateKey:String, o_targetState:FDataBlock,
    o_sourceState:FDataBlock):void {
      o_targetState.setProperty(s_playerStateKey, o_sourceState.getObject(s_playerStateKey));
    }
    
    //==================================================================
    // createOtherPlayerState
    //==================================================================
    private function createOtherPlayerState(o_playerState:FDataBlock):FDataBlock {
      var o_targetPlayerState:FDataBlock = new FDataBlock({ });
      copyPlayerStatePropertyFrom(PlayerStateKeys.STATUS, o_targetPlayerState, o_playerState);
      return(o_targetPlayerState);
    }
    
    //==================================================================
    // createThemeComponentsData
    //==================================================================
    private function createThemeComponentsData():Object {
      var o_themesSettings:SvThemesSettings = new SvThemesSettings(mo_settings.data.themes);
      var o_themeSettings:SvThemeSettings = o_themesSettings.getThemeSettings(mo_settings.data.activeThemeId);
      return(o_themeSettings ? o_themeSettings.components : null);
    }
    
    //==================================================================
    // updateUsersDataGrid
    //==================================================================
    private function updateUsersDataGrid():void {
      mo_mainView.usersDataGridView.update();
    }
    
    //==================================================================
    // endWaitingForPlayers
    //==================================================================
    private function endWaitingForPlayers():void {
      mo_state.setProperty(ServerStateKeys.WAITING_FOR_PLAYERS, false);
      mas_waitingPlayerIds = null;
    }
    
    /*
    //==================================================================
    // getActiveGameTitle
    //==================================================================
    private function getActiveGameTitle():String {
      var o_gameSettings:Object = mo_settings.data.games[activeGameId];
      var o_block:FDataBlock = new FDataBlock(o_gameSettings);
      return(o_block.getString("title"));
    }
    */
    
    //==================================================================
    // getGameDialog
    //==================================================================
    private function getGameDialog():SvGameBaseDialog {
      var o_gamesDialog:SvGamesDialog = getGamesDialog();
      return(o_gamesDialog ? o_gamesDialog.gameDialog : null);
    }
    
    //==================================================================
    // getGamesDialog
    //==================================================================
    private function getGamesDialog():SvGamesDialog {
      return(mo_dialogManager.dialog as SvGamesDialog);
    }
    
    //==================================================================
    // handleAllClientsAreReady
    //==================================================================
    private function handleAllClientsAreReady():void {
      endWaitingForPlayers();
      mo_state.setProperty(ServerStateKeys.IS_GAME_STARTED, true);
      notifyClientsOfGameStateChanged();
      startGame();
    }
    
    //==================================================================
    // handleClientJoin
    //==================================================================
    private function handleClientJoin(s_clientId:String, o_messageBlock:FDataBlock):void {
      clientJoinsGame(s_clientId, false);
    }
    
    //==================================================================
    // handleClientNext
    //==================================================================
    private function handleClientNext(s_clientId:String, o_messageBlock:FDataBlock):void {
      clientMovesToNextEntry(s_clientId, o_messageBlock);
    }
    
    //==================================================================
    // handleClientReady
    //==================================================================
    private function handleClientReady(s_clientId:String, o_messageBlock:FDataBlock):void {
      if (!isWaitingForPlayers()) {
        return;
      }
      
      if (!FArrayUtils.Remove(mas_waitingPlayerIds, s_clientId)) {
        traceAndLog("SvEngine.handleClientReady. Warning: Invalid client ID: " + s_clientId);
        return;
      }
      
      mo_dialogManager.setDialogData(new SvDialogData(SvDialogData.TYPE_WAITING_CLIENTS_CHANGED));
    }
    
    //==================================================================
    // handleClientRequestComponentThemeImageData
    //==================================================================
    private function handleClientRequestComponentThemeImageData(s_clientId:String,
    o_messageBlock:FDataBlock):void {
      var o_dispatcher:SvComponentThemeImageDataDispatcher =
        new SvComponentThemeImageDataDispatcher(this);
      o_dispatcher.load(s_clientId, o_messageBlock);
    }
    
    //==================================================================
    // handleClientWatch
    //==================================================================
    private function handleClientWatch(s_clientId:String, o_messageBlock:FDataBlock):void {
      if (mo_game) {
        mo_game.handleClientWatch(s_clientId);
      }
      clientLeavesGame(s_clientId, true);
    }
    
    //==================================================================
    // handleGameCallbackImagesLoadError
    //==================================================================
    private function handleGameCallbackImagesLoadError(o_data:Object):void {
      traceAndLog("SvEngine.handleGameCallbackImagesLoadError. " + o_data);
    }
    
    //==================================================================
    // handleGamesImportComplete
    //==================================================================
    private function handleGamesImportComplete(o_data:Object):void {
      const MSG_SUCCESSFUL:String = "Successfully imported games settings!";
      const MSG_HEADER:String = "Games Import";
      openMessageDialog(MSG_SUCCESSFUL, MSG_HEADER, SvDialogIds.MESSAGE_IMPORT);
    }
    
    //==================================================================
    // handleGamesImportError
    //==================================================================
    private function handleGamesImportError(o_data:Object):void {
      var s_message:String;
      
      if (o_data is String) {
        s_message = o_data as String;
      } else {
        var o_block:FDataBlock = new FDataBlock(o_data);
        var o_errorEvent:ErrorEvent = o_block.getObject("errorEvent") as ErrorEvent;
        s_message = o_block.getString("message",
          o_errorEvent ? o_errorEvent.text : "Unknown games import error.");
      }
      
      traceAndLog("SvEngine.handleGamesImportError. Error: " + s_message);
      const MSG_HEADER:String = "Games Import";
      openMessageDialog(s_message, MSG_HEADER, SvDialogIds.MESSAGE_IMPORT);
    }
    
    //==================================================================
    // init
    //==================================================================
    private function init():void {
      initState();
      initMainView();
      initSettings();
      mo_dialogManager = new SvDialogManager(mo_mainView.display, this);
      updateStateThemeData();
    }
    
    //==================================================================
    // initDefaultSettings
    //==================================================================
    private function initDefaultSettings():void {
      var o_settingsData:SvSettingsData = mo_settings.data;
      o_settingsData.ipAddress = "";
      o_settingsData.port = NetConnectData.SERVER_PORT;
      mo_settings.validateData();
    }
    
    //==================================================================
    // initGame
    //==================================================================
    private function initGame():void {
      if (mo_game) {
        traceAndLog(
          "SvEngine.initGame. Warning: Uninit previous game first before starting new game.");
        return;
      }
      
      var s_gameId:String = activeGameId;
      mo_game = SvGameFactory.Create(this, s_gameId);
      if (mo_game) {
        mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_SETTINGS, mo_game.settings.block.data);
      } else {
        mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_SETTINGS, { });
        traceAndLog("SvEngine.initGame. Warning: Unable to create game with id: " + s_gameId);
      }
    }
    
    //==================================================================
    // initMainView
    //==================================================================
    private function initMainView():void {
      mo_mainView = new SvMainView(mo_state, this);
      const VERSION_TEXT:String = "VERSION: ";
      mo_mainView.version = VERSION_TEXT + AppDescriptorData.Version;
    }
    
    //==================================================================
    // initSettings
    //==================================================================
    private function initSettings():void {
      mo_settings = new SvSettings();
      var b_applyDefaultSettings:Boolean;
      
      if (mo_settings.read()) {
        b_applyDefaultSettings = mo_settings.fileDidntExist;
      } else {
        if (!mo_settings.fileDidntExist) {
          traceAndLog("SvEngine.initSettings. Warning: Unable to read existing settings.");
        }
        b_applyDefaultSettings = true;
      }
      
      if (b_applyDefaultSettings) {
        traceAndLog("SvEngine.initSettings. Applying default settings.");
        initDefaultSettings();
        writeSettings();
      }
      
      var o_settingsData:SvSettingsData = mo_settings.data;
      mo_mainView.ipAddress = o_settingsData.ipAddress;
      mo_mainView.port = o_settingsData.port;
    }
    
    //==================================================================
    // initSocketServer
    //==================================================================
    private function initSocketServer():void {
      mo_mainView.startButton.enabled = false;
      mo_mainView.themesButton.enabled = false;
      
      mo_netServer = new NetServer(this);
      mo_netServer.logTextField = mo_mainView.logTextField;
      
      var s_ipAddress:String = mo_mainView.ipAddress;
      var u_port:uint = mo_mainView.port;
      
      if (mo_netServer.connect(u_port, s_ipAddress)) {
        mo_mainView.stopButton.enabled = true;
        mo_mainView.inputTextsEnabled = false;
      } else {
        traceAndLog("SvEngine.initSocketServer. Error: Unable to connect to socket server at " +
          s_ipAddress + ":" + u_port);
        mo_mainView.startButton.enabled = true;
        mo_mainView.themesButton.enabled = true;
        mo_mainView.inputTextsEnabled = true;
      }
    }
    
    //==================================================================
    // initState
    //==================================================================
    private function initState():void {
      mo_state = new FDataBlock();
      mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_ID, null);
      //mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_TITLE, null);
      mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_SETTINGS, { });
      mo_state.setProperty(ServerStateKeys.IS_GAME_STARTED, false);
      mo_state.setProperty(ServerStateKeys.PLAYERS, new FDataBlock( { } ));
      mo_state.setProperty(ServerStateKeys.THEME_ID, null);
      mo_state.setProperty(ServerStateKeys.THEME_IDS_BY_COMP_NAME, {});
    }
    
    //==================================================================
    // isClientInGame
    //==================================================================
    private function isClientInGame(s_clientId:String):Boolean {
      var o_playerState:FDataBlock = getPlayerState(s_clientId);
      return(o_playerState && o_playerState.getNumber(PlayerStateKeys.STATUS) == PlayerStatuses.ACTIVE);
    }
    
    //==================================================================
    // isGameActivated (get)
    //==================================================================
    private function get isGameActivated():Boolean {
      var s_activeGameId:String = mo_state.getString(ServerStateKeys.ACTIVE_GAME_ID);
      return(s_activeGameId && s_activeGameId.length);
    }
    
    //==================================================================
    // kickClient
    //==================================================================
    private function kickClient(s_clientId:String):void {
      removeClientFromServer(s_clientId);
      mo_netServer.disconnectClient(s_clientId);
    }
    
    //==================================================================
    // kickClients
    //==================================================================
    private function kickClients(as_clientIds:Array):void {
      for each (var s_clientId:String in as_clientIds) {
        kickClient(s_clientId);
      }
    }
    
    //==================================================================
    // notifyClientOfGameStateChanged
    //==================================================================
    private function notifyClientOfGameStateChanged(s_clientId:String):void {
      var o_message:Object = Message.Create(ServerMessageTypes.GAME_STATE_CHANGED);
      o_message[GameStateChangedKeys.IS_STARTED] = isGameStarted;
      //o_message[GameStateChangedKeys.GAME_SETTINGS] = mo_game.settings.block.data;
      sendMessageToClient(s_clientId, o_message);
    }
    
    //==================================================================
    // notifyClientsOfGameStateChanged
    //==================================================================
    private function notifyClientsOfGameStateChanged():void {
      var o_message:Object = Message.Create(ServerMessageTypes.GAME_STATE_CHANGED);
      o_message[GameStateChangedKeys.IS_STARTED] = isGameStarted;
      //o_message[GameStateChangedKeys.GAME_SETTINGS] = mo_game.settings.block.data;
      sendMessageToClients(o_message);
    }
    
    //==================================================================
    // onDialogActionGames
    //==================================================================
    private function onDialogActionGames(s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction == SvGamesDialogActions.ACTIVATE_GAME) {
        activeGameId = o_data as String;
      }
    }
    
    //==================================================================
    // onDialogActionSettings
    //==================================================================
    private function onDialogActionSettings(s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction != SvDialogActions.RESET_SETTINGS) {
        return;
      }
      
      var o_file:File = SvSettings.CreateFile();
      var b_isDeleteOk:Boolean;
      
      try {
        o_file.deleteFile();
        b_isDeleteOk = true;
        traceAndLog("SvEngine.onDialogActionSettings. Reset settings ok.");
      } catch (o_error:Error) {
        traceAndLog("SvEngine.onDialogActionSettings. Couldn't delete settings file. Error: " + 
          o_error);
      }
      
      if (b_isDeleteOk) {
        initSettings();
      }
      
      mo_dialogManager.setDialogData(b_isDeleteOk);
    }
    
    //==================================================================
    // onDialogActionThemes
    //==================================================================
    private function onDialogActionThemes(s_dialogAction:String, o_data:Object):void {
      if (s_dialogAction != DialogActions.COMPLETE) {
        return;
      }
      
      var o_completeData:SvThemesDialogCompleteData = o_data as SvThemesDialogCompleteData;
      if (!o_completeData) {
        traceAndLog("SvEngine.onDialogActionThemes. Warning: Unable to apply new themes settings.");
        return;
      }
      
      var b_shouldWriteSettings:Boolean;
      
      if (o_completeData.activeTheme) {
        mo_settings.data.activeThemeId = o_completeData.activeTheme;
        b_shouldWriteSettings = true;
      }
      
      var o_themesSettings:SvThemesSettings = o_completeData.themesSettings;
      if (o_themesSettings) {
        mo_settings.data.themes = o_themesSettings.getData();
        b_shouldWriteSettings = true;
      }
      
      if (b_shouldWriteSettings) {
        updateStateThemeData();
        writeSettings();
      }
    }
    
    //==================================================================
    // openGamesDialog
    //==================================================================
    private function openGamesDialog():void {
      mo_dialogManager.openGames(this, GameIds.ALL);
    }
    
    //==================================================================
    // openMessageDialog
    //==================================================================
    private function openMessageDialog(s_message:String, s_header:String = null,
    s_dialogId:String = null):void {
      mo_dialogManager.openMessage(s_message, s_header, s_dialogId);
    }
    
    //==================================================================
    // openSelectedGameDialog
    //==================================================================
    private function openSelectedGameDialog():void {
      var o_gamesDialog:SvGamesDialog = getGamesDialog();
      if (o_gamesDialog) {
        o_gamesDialog.openSelectedMiniGameDialog();
      }
    }
    
    //==================================================================
    // openSettingsFileDialog
    //==================================================================
    private function openSettingsFileDialog():void {
      mo_dialogManager.openSettingsFile(this, SvSettings.CreateFile());
    }
    
    //==================================================================
    // openThemesDialog
    //==================================================================
    private function openThemesDialog():void {
      mo_dialogManager.openThemes(this, mo_settings.data.activeThemeId, mo_settings.data.themes);
    }
    
    //==================================================================
    // removeClientFromServer
    //==================================================================
    private function removeClientFromServer(s_clientId:String):void {
      clientLeavesGame(s_clientId, false);
      mo_mainView.usersDataGridView.removeUser(s_clientId);
      
      var o_players:FDataBlock = mo_state.getBlock(ServerStateKeys.PLAYERS);
      o_players.deleteProperty(s_clientId);
    }
    
    //==================================================================
    // removeClientFromWaitingList
    //==================================================================
    private function removeClientFromWaitingList(s_clientId:String):void {
      FArrayUtils.Remove(mas_waitingPlayerIds, s_clientId);
    }
    
    //==================================================================
    // resetPlayerState
    //==================================================================
    private function resetPlayerState(s_clientId:String):void {
      var o_playerState:FDataBlock = getPlayerState(s_clientId);
      var o_progress:PlayerGameProgress =
        o_playerState ?
        o_playerState.getObject(PlayerStateKeys.GAME_PROGRESS) as PlayerGameProgress :
        null;
      
      if (o_progress) {
        o_progress.reset();
      }
    }
    
    //==================================================================
    // resetPlayerStates
    //==================================================================
    private function resetPlayerStates():void {
      var as_clientIds:Array = getActiveClientIds();
      for each (var s_clientId:String in as_clientIds) {
        resetPlayerState(s_clientId);
      }
    }
    
    //==================================================================
    // resolveDialogAffectsUserId
    //==================================================================
    private function resolveDialogAffectsUserId():String {
      var s_id:String = ms_dialogAffectsUserId;
      ms_dialogAffectsUserId = null;
      return(s_id);
    }
    
    //==================================================================
    // setActiveGameId
    //==================================================================
    private function setActiveGameId(s_gameId:String):void {
      mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_ID, s_gameId);
      //mo_state.setProperty(ServerStateKeys.ACTIVE_GAME_TITLE, getActiveGameTitle());
      
      mo_dialogManager.setDialogData(new SvDialogData(SvDialogData.TYPE_ACTIVE_GAME_CHANGED));
      
      allObserverClientsBecomeActive();
      
      beginWaitingForPlayers();
      initGame();
      
      //send message to all clients that the active game has changed
      var o_message:Object = Message.Create(ServerMessageTypes.ACTIVE_GAME_ID_CHANGED);
      o_message[ActiveGameChangedKeys.GAME_ID] = activeGameId;
      o_message[ActiveGameChangedKeys.GAME_SETTINGS] = activeGameSettings;
      //o_message[ActiveGameChangedKeys.TITLE] = getActiveGameTitle();
      sendMessageToClients(o_message);
    }
    
    //==================================================================
    // startGame
    //==================================================================
    private function startGame():void {
      if (!mo_game) {
        return;
      }
      
      mb_isStartGameRequested = false;
      openSelectedGameDialog();
      mo_game.start();
    }
    
    //==================================================================
    // uninitGame
    //==================================================================
    private function uninitGame():void {
      if (!mo_game) {
        return;
      }
      
      resetPlayerStates();
      
      mo_game.uninit();
      mo_game = null;
    }
    
    //==================================================================
    // uninitSocketServer
    //==================================================================
    private function uninitSocketServer():void {
      if (!mo_netServer) {
        return;
      }
      
      mo_netServer.disconnect();
      mo_netServer = null;
      
      mo_mainView.startButton.enabled = true;
      mo_mainView.stopButton.enabled = false;
      mo_mainView.themesButton.enabled = true;
      mo_mainView.inputTextsEnabled = true;
    }
    
    //==================================================================
    // updateStateThemeData
    //==================================================================
    private function updateStateThemeData():void {
      mo_state.setProperty(ServerStateKeys.THEME_ID, mo_settings.data.activeThemeId);
      mo_state.setProperty(ServerStateKeys.THEME_IDS_BY_COMP_NAME, createThemeComponentsData());
    }
  }
}